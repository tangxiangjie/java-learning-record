/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/11 下午4:55
 * Modified date: 2024/6/11 下午4:55
 */

package com.sdust.springboot.service;

import com.sdust.springboot.pojo.entity.User;
import com.sdust.springboot.pojo.query.UserQuery;
import com.sdust.springboot.utils.PageResult;

import java.util.List;

public interface IUserService {
    List<User> selectAll();

    PageResult list(UserQuery userQuery);

    void deleteById(Integer id);

    void deleteSelected(Integer[] ids);

    void add(User user);

    User selectById(Integer id);

    void updateForm(User user);

    User login(String name, String password);
}
