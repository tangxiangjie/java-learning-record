<%--
  ~ 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
  ~ Copyright @TangXJ
  ~ Created by TangXJ
  ~ Created&Used date: 2024/4/7 下午2:31
  ~ Modified date: 2024/4/7 下午2:31
  --%>

<%--
  Created by IntelliJ IDEA.
  User: TangXJ
  Date: 2024/4/7
  Time: 14:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
    <form action="/teacher?method=add" method="post"><%--默认就是get ?method=就是get方法传参  会被覆盖  所以form表单一般不用--%>
        姓名:<input type="text" name="name"><br>
        年龄:<input type="text" name="age"><br>
        性别:<input type="text" name="gender"><br>
        <input type="submit" value="添加">
    </form>
</body>
</html>
