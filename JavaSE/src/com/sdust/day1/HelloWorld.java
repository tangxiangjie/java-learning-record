package com.sdust.day1;

import org.junit.Test;

//Shift+Enter
public class HelloWorld {
    //main
    //Code review
    //args: arguments 参数
    public static void main(String[] args) {
        //sout
        //Ctrl+Alt+l
        System.out.println("Hello World!");
        System.out.println("Hello World!");
        System.out.println("Hello World!");

        //表达一个人的年龄
        //类型 变量
        //=: 赋值
        //“见名知意”
        int age = 23;
        int $age2 = 23;
        //int 9$age2 = 23;

        //变量 variable var
        int age1 = 23;
        age1 = 34;

        //final“修饰”的变量就是常量: 值不能改变
        final int age2 = 24;
        //Cannot assign a value to final variable 'age2'
        //age2 = 34;

        //test();
    }

/*    public static void test() {
        //soutm打印类名+方法名 标识是否执行
        //不是main，不会默认执行，在main中调用这个方法
        System.out.println("HelloWorld.test");
    }*/

    //单元测试 JUnit--开源的单元测试框架
    //Alt+Enter(快速修复一些错误,此处是导入依赖包,第三行)
    //ctrl r 替换 无法排除注释中的   shift F6 修改类名 变量名 Ctrl shift / 大段注释
    //Ctrl f/r 当前文件搜/替        Ctrl shift f/r 全部文件 搜/替(实际上更多选择) Ctrl shift f 跟搜狗输入法也冲突(简繁切换)
    @Test
    public void test1() {//方法
        //soutm打印类名+方法名 标识是否执行
        //不是main，不会默认执行，在main中调用这个方法
        System.out.println("HelloWorld.test");

        int num = 3;
        double e = 3.14;
        float f = 3.14f;
        char ch1 = 'a';
        char ch2 = '中';
        boolean b1 = true;
        boolean b2 = false;
        int Num = 3;
    }

    @Test
    public void test2() {//方法
        System.out.println("HelloWorld.test2");
        int num = 3;
    }


}
