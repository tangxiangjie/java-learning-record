/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/3/26 下午4:42 ~ 2024/3/28 下午9:55
 * Modified date: 2024/3/28 下午9:55
 */

package com.sdust.day12;

import org.junit.Test;

import java.io.*;

public class IODemo {

    public static void main(String[] args) {
        String goodsName = "诺基亚原装5800耳机";
        String name = "HTC" + goodsName.substring(3);
        System.out.println(name);//HTC原装5800耳机
    }

    @Test
    public void test1() {
        FileReader fileReader = null;//声明放在外边  局部变量必须初始化
        //FileReader fileReader;//声明放在外边 ×××××
        try {
            fileReader = new FileReader("io.txt");//打开水龙头
            //Reads a single character.
            int ch1 = fileReader.read();
            System.out.println((char)ch1);//a
            int ch2 = fileReader.read();
            System.out.println((char)ch2);//b
            int ch3 = fileReader.read();
            System.out.println((char)ch3);//c
            int ch4 = fileReader.read();
            System.out.println((char)ch4);//￿
            System.out.println(ch4);//-1
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {//在finally里边必须关闭IO流
            try {
                fileReader.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Test
    public void test2() {
        try {
            FileReader fileReader = new FileReader("io.txt");
            int ch = -1;
            while ((ch = fileReader.read()) != -1) {
                System.out.println((char)ch);
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    //----------Day12---------------
    @Test
    public void test3() {
        try {
            FileReader fileReader = new FileReader("io.txt");
            char[] buffer = new char[10];
            int length = -1;
            //public int read(char[] cbuf)
            //一次将10个字符读入到buffer数组里面
            //返回：读取的字符数，如果已经达到流的末尾，返回-1
            while ((length = fileReader.read(buffer)) != -1) {
                System.out.println(length);
                System.out.println(buffer);
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void test4() {
        //父类
        FileReader fileReader = null;
        FileWriter fileWriter = null;
        try {
            fileReader = new FileReader("io.txt");
            fileWriter = new FileWriter("io_back.txt");
            char[] buffer = new char[10];
            int length = -1;
            //public int read(char[] cbuf)
            //一次将10个字符读入到buffer数组里面
            //返回：读取的字符数，如果已经达到流的末尾，返回-1
            while ((length = fileReader.read(buffer)) != -1) {
                System.out.println(length);
                System.out.println(buffer);
                //读出多少写多少，最后一次读出来的数据很有可能不够buffer数组的长度
                fileWriter.write(buffer, 0, length);
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            //先打开的后关闭
            if (fileWriter != null) {
                try {
                    fileWriter.close();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            if (fileReader != null) {
                try {
                    fileReader.close();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

    @Test
    public void test5() {
        //FileInoutSteam 父类是InputSteam
        FileInputStream fileInputStream = null;//经验 先生命成null 局部变量必须初始化
        FileOutputStream fileOutputStream = null;
        try {
            fileInputStream = new FileInputStream("baidu.png");
            fileOutputStream = new FileOutputStream("baidu_back.png");
            byte[] buffer = new byte[1024];
            int length = -1;
            while ((length = fileInputStream.read(buffer)) != -1){//赋值语句有结果 结果就是length值
                System.out.println(length);
                //fileOutputStream.write(buffer, 0, length);//这个不会多写
                fileOutputStream.write(buffer);//会多写几个字节
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }
}
