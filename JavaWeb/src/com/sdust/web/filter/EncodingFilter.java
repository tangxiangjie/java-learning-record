/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/4/10 上午9:06 ~ 2024/4/18 上午9:27
 * Modified date: 2024/4/18 上午9:27
 */

package com.sdust.web.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@WebFilter(filterName = "encoding", urlPatterns = "/*")
public class EncodingFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        Filter.super.init(filterConfig);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        System.out.println("EncodingFilter.doFilter");
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        String method = request.getMethod();
        if ("post".equalsIgnoreCase(method)) {
            request.setCharacterEncoding("UTF-8");
        }
        filterChain.doFilter(servletRequest, servletResponse);//链条 往下个过滤器传递
    }

    @Override
    public void destroy() {
        Filter.super.destroy();
    }
}
