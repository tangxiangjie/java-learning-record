/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/4/3 上午11:41 ~ 2024/4/7 下午1:34
 * Modified date: 2024/4/7 下午1:34
 */
/*
 * 已经不需要的 整合到TeacherServlet里面了
 * */
package com.sdust.web.controller;

import com.sdust.web.util.JDBCUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@WebServlet("/deleteTeacher")
public class DeleteTeacher extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("DeleteTeacher.service");
        String id = req.getParameter("id");
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = JDBCUtil.getConnection();
            String sql = "delete from teacher where id=?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1,Integer.parseInt(id));
            System.out.println(statement);
            int count = statement.executeUpdate();
            System.out.println("count:" + count);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            JDBCUtil.close(connection, statement, null);
        }
        //resp啥也没有就是白色
        //重定向（让servle执行完之后 自己发送请求）302 服务器告诉浏览器去请求这个地址
        resp.sendRedirect("/teacher");
        //浏览器看到resp是302  自己取到resp的地址  自己再发送一个请求
        //没重定向：一次请求  1次返回（200 白色，因为你也没跟JSP配合展示，所以啥也没有）
        //有重定向：两次请求  两次返回（第一次302 返回重定向命令及地址，第二次200 返回重定向请求的地方）
        //重定向和转发
        //地址栏上的地址  是request决定的
    }
}
