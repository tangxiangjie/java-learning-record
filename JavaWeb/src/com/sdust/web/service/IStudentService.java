/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/4/7 下午1:39 ~ 2024/5/25 下午4:04
 * Modified date: 2024/5/25 下午4:04
 */

package com.sdust.web.service;

import com.sdust.web.pojo.entity.Student;
import com.sdust.web.pojo.vo.StudentBanjiVO;
import com.sdust.web.util.PageInfo;

import java.util.List;

public interface IStudentService {
    public abstract List<StudentBanjiVO> selectAll();
    void deleteById(Integer id);
    void add(Student student);//没有id
    Student selectById(Integer id);
    void update(Student student);//有id

    PageInfo<StudentBanjiVO> selectByPage(Integer pageNo, Integer pageSize);

    int selectCount();
}
