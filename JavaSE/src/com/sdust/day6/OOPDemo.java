/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/3/20 下午10:05 ~ 2024/3/20 下午11:00
 * Modified date: 2024/3/20 下午11:00
 */

package com.sdust.day6;

import org.junit.Test;

import java.util.Scanner;

public class OOPDemo {

    @Test
    public void test1() {
        Scanner scanner = new Scanner(System.in);
        int num = 3;
        //student1：变量、实例、对象
        Student student1 = new Student();
        //对象.属性
        student1.id = 1;
        student1.name = "张三";
        student1.age = 23;
        student1.gender = "男";
        System.out.println(student1.id);
        System.out.println(student1.name);
        System.out.println(student1.age);
        System.out.println(student1.gender);

        Student student2 = new Student();
        student2.id = 2;
        student2.name = "李四";
        student2.age = 24;
        student2.gender = "女";
        System.out.println(student2.id);
        System.out.println(student2.name);
        System.out.println(student2.age);
        System.out.println(student2.gender);
    }

    @Test
    public void test89() {
        Student student1 = new Student();
        student1.id = 1;
        student1.name = "lisi11";
        student1.age = 26;
        student1.gender = "男";
        String info1 = student1.getInfo();
        System.out.println(info1);

        Student student2 = new Student();
        student2.id = 2;
        student2.name = "李四";
        student2.age = 24;
        student2.gender = "女";
        String info2 = student2.getInfo();
        System.out.println(info2);
        System.out.println(student2.getInfo());
    }


    @Test
    public void test80() {
        Student student1 = new Student();
        student1.id = 1;
        student1.name = "lisi11";
        student1.age = 26;
        student1.gender = "男";
        System.out.println(student1.toString());
        //当打印对象的时候，默认就调用了：对象.toString()
        System.out.println(student1);

        Student student2 = new Student();
        student2.id = 2;
        student2.name = "李四";
        student2.age = 24;
        student2.gender = "女";
        System.out.println(student2.toString());
    }

    @Test
    public void test99() {
        Student student = new Student();
        student.id = 1;
        student.age = 167;
        System.out.println(student);
    }

    @Test
    public void test2() {
//        Teacher teacher1 = new Teacher();
//        teacher1.id = 1;
//        teacher1.name = "张老师";
//        teacher1.age = 230;
//        teacher1.salary = 10000;

//        Teacher teacher2 = new Teacher();
//        teacher2.id = 2;
//        teacher2.name = "高老师";
//        teacher2.age = 34;
//        teacher2.salary = 10000.0;

        Teacher teacher = new Teacher();
        teacher.setId(1);
        teacher.setName("张老师");
        teacher.setAge(234);
        teacher.setSalary(1000);
        System.out.println(teacher);
    }

    @Test
    public void test5() {
        int[] array1 = new int[3];
        boolean[] array2 = new boolean[3];
        Student[] students = new Student[3];
        for (int i = 0; i < students.length; i++) {
            System.out.println(students[i]);
        }
        //增强的for循环 students.for
        for (Student student : students) {
            System.out.println(student);
        }

        int[] array = {34, 435, 7};
        for (int i : array) {
            System.out.println(i);
        }
    }

    //空指针异常测试
    @Test
    public void test7() {
        //在方法里面定义的叫局部变量
        Student student = null;
        //Variable 'student' might not have been initialized
        //student.id = 23;
        System.out.println(student);//null
        //null.属性   null.方法
        //java.lang.NullPointerException: Cannot read field "id" because "student" is null
        System.out.println(student.id);
    }

    @Test
    public void test56() {
        Student student = new Student();
        System.out.println(student.id);//0
        System.out.println(student.name);//null
        int num = 0;
        System.out.println(num);
    }


}
