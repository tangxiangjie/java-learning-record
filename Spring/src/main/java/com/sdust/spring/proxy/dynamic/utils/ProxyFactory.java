/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/7 上午1:22
 * Modified date: 2024/6/7 上午1:22
 */

package com.sdust.spring.proxy.dynamic.utils;

import com.sdust.spring.proxy.staticc.DongShiZhang;
import com.sdust.spring.proxy.staticc.IQianzi;
import com.sdust.spring.service.IBanjiService;
import org.junit.Test;
import org.springframework.stereotype.Service;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * 相当于把动态代理处理器和使用时代理的创建进行了一个整合封装
 * 使用时需要传进目标对象，就可以返回其代理对象
 */
public class ProxyFactory {
    /**
     * 代理工厂的方法
     * 传递过来任何一个目标对象，创建对应的代理对象返回
     * @param target 目标对象
     * @return 代理对象
     */
    public static Object getProxyInstance(Object target) {
        return Proxy.newProxyInstance(
                target.getClass().getClassLoader(), // 目标对象使用的类加载器
                target.getClass().getInterfaces(), // 目标对象实现的接口
                new InvocationHandler() { // 匿名内部类  事件处理器 动态代理处理器
                    // Object proxy 代理对象
                    // Method method 调用的方法
                    // Object[] args 调用的方法的参数
                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        System.out.println(target.getClass().getName() + "." + method.getName());
                        System.out.println("Proxy: Logging before" + method.getName());
                        Object returnValue = method.invoke(target, args);
                        System.out.println("Proxy: Logging after" + method.getName());
                        return returnValue;
                    }
                });
    }
}