/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/3/20 上午9:19 ~ 2024/3/21 下午5:18
 * Modified date: 2024/3/21 下午5:18
 */

package com.sdust.day6.homework;

public class Number {
    private int num1;
    private int num2;

    public Number() {
    }

    public Number(int num1, int num2) {
        this.num1 = num1;
        this.num2 = num2;
    }

/*    //单一职责原则
    public void addOld() {
        int res = (num1 + num2);
        System.out.println(res);//打印不是add该干的事
    }*/

    public int add() {
        return (num1 + num2);
    }

    public int sub() {
        return num1 - num2;
    }

    public int multiple() {
        return num1 * num2;
    }

    public double divide() {
        return (double)num1 / (double)num2;
    }

    public void setNum1(int num1) {
        this.num1 = num1;
    }

    public void setNum2(int num2) {
        this.num2 = num2;
    }

    public int getNum1() {
        return num1;
    }

    public int getNum2() {
        return num2;
    }
}
