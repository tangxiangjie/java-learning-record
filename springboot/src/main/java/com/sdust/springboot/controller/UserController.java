/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/11 下午4:29
 * Modified date: 2024/6/11 下午4:29
 */

package com.sdust.springboot.controller;

import cn.hutool.extra.servlet.JakartaServletUtil;
import com.sdust.springboot.annotation.MyLog;
import com.sdust.springboot.pojo.entity.LoginLog;
import com.sdust.springboot.pojo.entity.User;
import com.sdust.springboot.pojo.query.UserQuery;
import com.sdust.springboot.service.ILoginLogService;
import com.sdust.springboot.service.IUserService;
import com.sdust.springboot.utils.PageResult;
import com.sdust.springboot.utils.Result;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@Controller
@RestController     //RestController=Controller+ResponseBody   不加ResponseBody就是转发了
@RequestMapping("/user")
public class UserController {
    // @Autowired
    // private UserMapper userMapper;

    @Autowired
    private IUserService userService;
    @Autowired
    private ILoginLogService loginLogService;

    // @MyLog
    // @MyLog(module = "用户模块")
    //@MyLog(module = "用户模块", desc="返回用户列表")
    @RequestMapping("/list")
    //@ResponseBody
    public PageResult list(UserQuery userQuery) {
        PageResult pageInfo = userService.list(userQuery);
        return pageInfo;
    }

    @MyLog(module = "用户模块", desc="删除指定记录")
    @RequestMapping("/deleteById")
    public Result deleteById(Integer id) {
        userService.deleteById(id);
        return Result.ok("删除成功");
    }

    @MyLog(module = "用户模块", desc="删除所选记录")
    @RequestMapping("/deleteSelected")
    public Result deleteSelected(Integer[] ids) {
        /*String str = null;
        str.substring(1);*/
        userService.deleteSelected(ids);
        return Result.ok("删除成功");
    }


    @MyLog(module = "用户模块", desc="添加单条记录")
    @RequestMapping("/add")
    public Result add(User user) {
        userService.add(user);
        return Result.ok("添加成功");
    }

    @RequestMapping("/selectById")
    public Result selectById(Integer id) {
        User user = userService.selectById(id);
        return Result.ok(user);
    }

    @MyLog(module = "用户模块", desc="更新指定记录")
    @RequestMapping("/updateForm")
    public Result updateForm(User user) {
        userService.updateForm(user);
        return Result.ok("编辑成功");
    }

    @MyLog(module = "用户模块", desc="用户登录")
    @RequestMapping("/login")
    public Result login(@RequestParam(value = "username") String name,
                        String password, HttpSession session, HttpServletRequest request) {
        User user = userService.login(name, password);
        if (user == null) {
            LoginLog loginLog = new LoginLog();
            loginLog.setUserName(name);
            loginLog.setStatus(0);
            loginLog.setMsg("用户名或密码错误");
            loginLog.setAccessTime(new Date());
            loginLog.setIp(JakartaServletUtil.getClientIP(request));
            loginLogService.add(loginLog);
            return Result.error("用户名或密码错误");
        }

        LoginLog loginLog = new LoginLog();
        loginLog.setUserName(name);
        loginLog.setUserId(user.getId());
        loginLog.setStatus(1);
        loginLog.setMsg("登录成功");
        loginLog.setAccessTime(new Date());
        loginLog.setIp(JakartaServletUtil.getClientIP(request));
        loginLogService.add(loginLog);

        session.setAttribute("user", user);
        return Result.ok("登录成功");
    }

    @MyLog(module = "用户模块", desc="用户登出")
    @RequestMapping("/logout")
    public Result logout(HttpSession session) {
        session.removeAttribute("user");
        return Result.ok("已退出");
    }

    @RequestMapping("/selectAll")
    //@ResponseBody
    public List<User> selectAll() {
        System.out.println("UserController.selectAll");
        /*List<User> list = new ArrayList<>();
        User user1 = new User();
        user1.setId(1);
        user1.setName("zhangsan");
        User user2 = new User();
        user2.setId(2);
        user2.setName("list");
        User user3 = new User();
        user3.setId(3);
        user3.setName("wangwu");
        list.add(user1);
        list.add(user2);
        list.add(user3);*/

        List<User> list = userService.selectAll();
        return list;
    }
}
