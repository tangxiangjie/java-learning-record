/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/3/20 下午5:16 ~ 2024/3/21 下午10:13
 * Modified date: 2024/3/21 下午10:13
 */

package com.sdust.day8.day7diffpackinherittest;
import com.sdust.day7.Person;
import org.junit.Test;

public class WorkerTest {

    @Test
    public void test1() {
        Worker worker = new Worker(1, "zhangsan", 23);
        //worker.name = "zhangsan";
        Worker worker1 = new Worker();
        //worker.id = 1;//不能直接访问赋值
        //要用set方法
        worker1.setId(11);
        worker1.age = 11;
        System.out.println(worker1.age);
        worker1.test2();
    }

}
