/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/5/30 下午2:43
 * Modified date: 2024/5/30 下午2:43
 */

package com.sdust.day39;

import org.junit.Test;

import java.util.List;

public class AnnotationDemo {

    // 抑制编译器警告 List不加泛型会出警告
    @SuppressWarnings({"unused","unchecked"})
    private void save() {
        List list = null;//Raw use of parameterized class 'List'
    }

    // 标记方法以及过时
    @Deprecated
    private void save1() {
    }
}
