/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/3/26 下午2:24 ~ 2024/3/27 上午12:05
 * Modified date: 2024/3/27 上午12:05
 */

package com.sdust.day11;

import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class ExceptionDemo {

    @Test
    public void test1() {
        //运行时异常：运行的时候抛出的异常，是程序员自己写的代码有问题
        //java.lang.ArithmeticException: / by zero
        //int num1 = 1 / 0;

        //编译时异常：未雨绸缪
        //FileInputStream fileInputStream = new FileInputStream("aa.txt");//这就叫编译时异常
        //Unhandled exception: java.io.FileNotFoundException
        try {
            // 需要检查的代码（可能会抛出异常，也可能不会抛出异常）
            FileInputStream fileInputStream = new FileInputStream("aa.txt");//这就叫编译时异常
            System.out.println("ExceptionDemo.test1");
        } catch (FileNotFoundException e) {
            //捕获异常后要处理异常
            //throw new RuntimeException(e);//抛出了 后边不执行了？？？？？？？？
            e.printStackTrace();
        } finally {
            //一定会被执行的代码（不管异常抛不抛出都会执行）
            System.out.println("finally");
        }

        System.out.println("ExceptionDemo.test1");

    }

    @Test
    public void test2() throws FileNotFoundException {
        FileInputStream fileInputStream = new FileInputStream("aa.txt");
    }


    @Test
    public void test3() throws ClassNotFoundException {
        Class.forName("com.mysql.jdbc.Driver");//加载驱动，驱动名字
    }

    @Test
    public void test4() {
        //Unhandled exception: java.lang.ClassNotFoundException
        // try-catch自己处理这个异常
        try {
            Class.forName("com.mysql.jdbc.Driver");//加载驱动，驱动名字
        } catch (ClassNotFoundException e) {
            //throw new RuntimeException(e);
            e.printStackTrace();
            System.out.println("ExceptionDemo.test4");
        }
    }

    @Test
    public void test5() {
        try {
            show();//alt+enter ① 再抛出 ② try catch
        } catch (ClassNotFoundException e) {
            //throw new RuntimeException(e);
            e.printStackTrace();
            System.out.println("ExceptionDemo.test5");
        }
    }

    //抛出编译时异常，谁调用我谁去处理
    @Test
    public void show() throws ClassNotFoundException {//抛出 给别人解决
        Class.forName("com.mysql.jdbc.Driver");
    }

    @Test
    public void test6() {
        //FileNotFoundException extends IOException
        //一般都要精确捕获异常，除非实在找不出 用Exception
        try {
            FileInputStream fileInputStream = new FileInputStream("a1.txt");
            fileInputStream.read();
        } catch (FileNotFoundException e) {//不能调换顺序
            //throw new RuntimeException(e);
            e.printStackTrace();//还会继续
            System.out.println("FileNotFoundException");
        } catch (IOException e) {//防止文件损坏 加个异常处理
            //throw new RuntimeException(e);
            e.printStackTrace();//还会继续
            System.out.println("IOException");
        } finally {
            System.out.println("finally");
        }
    }//类似声明成父类 new 子类：IOException是FileNotFoundException的父类

    @Test
    public void test7() {
        try {
            quQian(900);//编译型异常
        } catch (MeiQianException e) {
            e.printStackTrace();
        }
    }

    public void quQian(double money) throws MeiQianException {//本身就是为了抛出，不能try catch
        if (money >= 1000) {
            throw new MeiQianException("钱不够");
        }
        System.out.println("钱够了");
    }
}
