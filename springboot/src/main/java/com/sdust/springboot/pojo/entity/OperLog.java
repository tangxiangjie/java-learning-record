/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/13 下午2:45
 * Modified date: 2024/6/13 下午2:45
 */

package com.sdust.springboot.pojo.entity;

import lombok.Data;

import java.util.Date;

@Data
public class OperLog {
    private Integer id;
    private String module;
    private String description;
    private String logType;
    private Integer userId;
    private String userName;
    private String requestMethod;
    private String requestUri;
    private String requestParams;
    private String responseParams;
    private String requestIp;
    private String serverAddress;
    private Integer exception;//is_exception
    private String exceptionMsg;
    private Date startTime;
    private Date endTime;
    private Long  executeTime;
    private String userAgent;
    private String deviceName;
    private String browserName;
    private Integer deleted;//is_deleted
    private Date createTime;
    private Date updateTime;

}