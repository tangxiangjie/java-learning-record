<%--
  ~ 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
  ~ Copyright @TangXJ
  ~ Created by TangXJ
  ~ Created&Used date: 2024/4/17 下午3:27 ~ 2024/4/18 上午9:23
  ~ Modified date: 2024/4/18 上午9:23
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>登录</title>
    <link href="static/layui/css/layui.css" rel="stylesheet">
</head>
<body>
<style>
    .demo-login-container{width: 320px; margin: 100px auto 0;}
    /*margin 改这里 登录靠下一点  100px auto 0(取对边，上下100 左右居中) 4个参数上右下左*/
    .demo-login-other .layui-icon{position: relative; display: inline-block; margin: 0 2px; top: 2px; font-size: 26px;}
</style>
<form class="layui-form">
    <div class="demo-login-container">
        <div class="layui-form-item">
            <div class="layui-input-wrap">
                <div class="layui-input-prefix">
                    <i class="layui-icon layui-icon-username"></i>
                </div>
                <input type="text" name="username" value="" lay-verify="required" placeholder="用户名" lay-reqtext="请填写用户名" autocomplete="off" class="layui-input" lay-affix="clear">
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-input-wrap">
                <div class="layui-input-prefix">
                    <i class="layui-icon layui-icon-password"></i>
                </div>
                <input type="password" name="password" value="" lay-verify="required" placeholder="密   码" lay-reqtext="请填写密码" autocomplete="off" class="layui-input" lay-affix="eye">
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-row">
                <div class="layui-col-xs7">
                    <div class="layui-input-wrap">
                        <div class="layui-input-prefix">
                            <i class="layui-icon layui-icon-vercode"></i>
                        </div>
                        <input type="text" name="captcha" value="" lay-verify="required" placeholder="验证码" lay-reqtext="请填写验证码" autocomplete="off" class="layui-input" lay-affix="clear">
                    </div>
                </div>
                <div class="layui-col-xs5">
                    <div style="margin-left: 10px;">
                        <%--<img src="https://www.oschina.net/action/user/captcha" onclick="this.src='https://www.oschina.net/action/user/captcha?t='+ new Date().getTime();">--%>
                        <img id="verifyCodeId" src="/verifyCode" onclick="refresh()"><%--onclick="this.src='/veryfyCode?t='+ new Date().getTime();"--%>
                    </div>
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <input type="checkbox" name="remember" lay-skin="primary" title="记住密码">
            <a href="#forget" style="float: right; margin-top: 7px;">忘记密码？</a>
        </div>
        <div class="layui-form-item">
            <button class="layui-btn layui-btn-fluid" lay-submit lay-filter="demo-login">登录</button>
        </div>
        <div class="layui-form-item demo-login-other">
            <label>社交账号登录</label>
            <span style="padding: 0 21px 0 6px;">
        <a href="javascript:;"><i class="layui-icon layui-icon-login-qq" style="color: #3492ed;"></i></a>
        <a href="javascript:;"><i class="layui-icon layui-icon-login-wechat" style="color: #4daf29;"></i></a>
        <a href="javascript:;"><i class="layui-icon layui-icon-login-weibo" style="color: #cf1900;"></i></a>
      </span>
            或 <a href="user_add.jsp">注册帐号</a>
        </div>
    </div>
</form>

<script src="static/jquery-2.1.4.js" type="text/javascript" charset="utf-8"></script>
<script src="static/layui/layui.js"></script>
<script src="static/mylayer.js" type="text/javascript" charset="utf-8"></script>

<script>
    function refresh() {
        $('#verifyCodeId').attr('src', '/verifyCode?' + Math.random());
    }

    layui.use(function(){
        var form = layui.form;
        var layer = layui.layer;
        // 提交事件
        form.on('submit(demo-login)', function(data){
            var field = data.field; // 获取表单字段值
            // 显示填写结果，仅作演示用
            /*layer.alert(JSON.stringify(field), {
                title: '当前填写的字段值'
            });*/
            // 此处可执行 Ajax 等操作
            $.post(
                '/user?method=login',//仍然需要filter放行
                field,//$('#formId').serialize(),效果是一样的，给form加id就行了
                function (result) {
                    console.log(result);
                    if (result.code == 0) {
                        mylayer.okUrl(result.msg, '/');
                        layer.msg(
                            result.msg,
                            {icon: 1, time: 1000},
                            function() {// msg消失之后触发的函数
                                location.href = '/';
                            }
                        );
                    } else {
                        mylayer.errorMsg(result.msg);
                    }
                },
                'json'
            );
            return false; // 阻止默认 form 跳转
        });
    });
</script>

</body>
</html>