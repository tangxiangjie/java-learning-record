/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/3/20 上午9:43 ~ 2024/3/20 上午11:00
 * Modified date: 2024/3/20 上午11:00
 */

package com.sdust.day7.practice;

import org.junit.Test;

import java.util.Scanner;

public class StudentManager {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("input the number of students");
        int count = scanner.nextInt();
        Student[] students = new Student[count];
        for (int i = 0; i < students.length; i++) {
            System.out.println("请输入id: ");
            int id = scanner.nextInt();
            System.out.println("请输入姓名");
            String name = scanner.next();
            System.out.println("请输入年龄");
            int age = scanner.nextInt();
            System.out.println("请输入性别");
            String gender = scanner.next();
            //
            //students[i] = new Student(id, name, age, gender);
            Student student = new Student(id, name, age, gender);
            students[i] = student;
        }

        for (Student student : students) {
            System.out.println(student);
        }

        while (true) {//如果搜索不到对应信息，打印一个：没有该信息的学生
            System.out.println("按照id搜索输入1");
            System.out.println("按照name搜索输入2");
            System.out.println("按照age搜索输入3");
            System.out.println("按照gender搜索输入4");
            System.out.println("退出输入0");
            int order = scanner.nextInt();
            if (order == 0) {
                System.out.println("退出");
                break;
            }
            switch (order) {
                case 1:
                    System.out.println("请输入你要搜索学生的id: ");
                    int idSearch = scanner.nextInt();
                    searchById(students, idSearch);
                    break;
                case 2:
                    System.out.println("请输入你要搜索学生的名字: ");
                    String nameSearch = scanner.next();
                    searchByName(students, nameSearch);
                    break;
                case 3:
                    System.out.println("请输入你要搜索学生的年龄: ");
                    int ageSearch = scanner.nextInt();
                    searchByAge(students, ageSearch);
                    break;
                case 4:
                    System.out.println("请输入你要搜索学生的性别: ");
                    String genderSearch = scanner.next();
                    searchByGender(students, genderSearch);
                    break;
                default:
                    System.out.println("非法输入，程序自动退出!");
                    break;
            }
        }
    }

    @Test
    public void test1() {
        String str1 = "zhangsan";
        String str2 = "lisi";
        if (str1.equals(str2)) {
            System.out.println("==");
        } else {
            System.out.println("!=");
        }
    }

    public static void searchById(Student[] students, int idSearch) {
        Student[] studentsRes = new Student[10];
        int count = 0;
        for (Student student : students) {
            if (idSearch == (student.getId())){
                studentsRes[count++] = student;
            }
        }
        if (count > 0) {
            System.out.println("搜索到" + count + "个结果");
            for (int i = 0; i < count; i++) {
                System.out.println(studentsRes[i]);
            }
        } else {
            System.out.println("没有搜索到该学生的信息!");
        }
    }

    public static void searchByName(Student[] students, String nameSearch) {
        Student[] studentsRes = new Student[10];
        int count = 0;
        for (Student student : students) {
            if (nameSearch.equals(student.getName())){
                studentsRes[count++] = student;
            }
        }
        if (count > 0) {
            System.out.println("搜索到" + count + "个结果");
            for (int i = 0; i < count; i++) {
                System.out.println(studentsRes[i]);
            }
        } else {
            System.out.println("没有搜索到该学生的信息!");
        }
    }

    public static void searchByAge(Student[] students, int ageSearch) {
        Student[] studentsRes = new Student[10];
        int count = 0;
        for (Student student : students) {
            if (ageSearch == student.getAge()){
                studentsRes[count++] = student;
            }
        }
        if (count > 0) {
            System.out.println("搜索到" + count + "个结果");
            for (int i = 0; i < count; i++) {
                System.out.println(studentsRes[i]);
            }
        } else {
            System.out.println("没有搜索到该学生的信息!");
        }
    }

    public static void searchByGender(Student[] students, String genderSearch) {
        Student[] studentsRes = new Student[10];
        int count = 0;
        for (Student student : students) {
            if (genderSearch.equals(student.getGender())){
                studentsRes[count++] = student;
            }
        }
        if (count > 0) {
            System.out.println("搜索到" + count + "个结果");
            for (int i = 0; i < count; i++) {
                System.out.println(studentsRes[i]);
            }
        } else {
            System.out.println("没有搜索到该学生的信息!");
        }
    }
}
