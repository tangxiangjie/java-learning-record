/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/11 上午11:38
 * Modified date: 2024/6/11 上午11:38
 */

package com.sdust.spring.decorator.impl;

import com.sdust.spring.decorator.Shape;

public class RedShapeDecorator extends ShapeDecorator{
    public RedShapeDecorator(Shape decoratedShape) {//构造函数
        super(decoratedShape);
    }

    @Override
    public void draw() {
        decoratedShape.draw();
        setRedBorder(decoratedShape);
    }

    private void setRedBorder(Shape decoratedShape) {
        System.out.println("Border Color: Red");
    }
}