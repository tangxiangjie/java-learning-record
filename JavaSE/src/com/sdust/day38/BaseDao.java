/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/5/30 上午10:37
 * Modified date: 2024/5/30 上午10:37
 */

package com.sdust.day38;

import com.sdust.day37.Student;
import com.sdust.day37.Teacher;
import org.junit.Test;

public class BaseDao<T> {//泛型类
    public <K>K save(K k){//泛型方法  举个例子而已
        return k;
    }

    public void add(T t) {//泛型方法

    }

    public void update(T t) {//泛型方法

    }

}
