/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/6 下午11:37 ~ 2024/6/7 上午1:10
 * Modified date: 2024/6/6 下午11:42
 */

package com.sdust.spring.proxy.staticc;

//个人觉得学到这个地步 具体的业务例子可能更好一点  当然可能比较难找
//董事长日理万机，客户不能直接和董事长打交道，董事长身边的人有秘书和司机
//让董事长和秘书都实现IQianzi这个接口，让他们对外都具有签字这个行为
//客户只要把文件交给秘书就可以，当然客户也知道秘书只是前期审核 后期收尾处理
//真正签字的是董事长，但还是要交给秘书，因为只有秘书实现了对外签字这个行为，司机肯定没有

public class MiShu implements IQianzi{
    private DongShiZhang dongShiZhang;//引用一个董事长对象  为什么是引用

    public MiShu(DongShiZhang dongShiZhang) {
        this.dongShiZhang = dongShiZhang;
    }

    @Override
    public void qianzi() {
        System.out.println("MiShu.qianzi  前期审核");
        dongShiZhang.qianzi();
        System.out.println("MiShu.qianzi  后期收尾");
    }
}
