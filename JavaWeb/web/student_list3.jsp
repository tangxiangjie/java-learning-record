<%@ page import="com.sdust.web.util.PageInfo" %>
<%@ page import="com.sdust.web.pojo.entity.Student" %>
<%@ page import="com.sdust.web.pojo.vo.StudentBanjiVO" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%--
  ~ 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
  ~ Copyright @TangXJ
  ~ Created by TangXJ
  ~ Created&Used date: 2024/4/7 下午5:22 ~ 2024/4/18 上午10:21
  ~ Modified date: 2024/4/18 上午10:21
  --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="static/bootstrap-3.4.1-dist/css/bootstrap.css">
</head>
<body>

    <a class="btn btn-primary" href="http://localhost:8080/student?method=toAddStudent">添加</a>
    <table class="table table-striped table-bordered table-hover table-condensed">
        <tr>
            <td>ID</td>
            <td>名字</td>
            <td>年龄</td>
            <td>性别</td>
            <td hidden="hidden">班级ID</td>
            <td>班级</td>
            <td>编辑</td>
            <td>删除</td>
        </tr>

        <c:forEach items="${pageInfo.list}" var="student">
            <tr>
                <td>${student.id}</td>
                <td>${student.name}</td>
                <td>${student.age}</td>
                <td>${student.gender}</td>
                <td hidden="hidden">${student.banjiId}</td>
                <td>${student.banjiName}</td>
                <td><a href="/student?method=updateQuery&id=${student.id}">编辑</a></td>
                <td><a href="javascript:void(0)" onclick="deleteById(${student.id})">删除</a></td>
                <%--<td><button onclick="deleteById(<%=student.getId()%>)">删除</button></td>--%>
            </tr>
        </c:forEach>
    </table>

    <nav aria-label="Page navigation">
        <ul class="pagination">
            <li>
                <a href="javascript:void(0)" aria-label="Previous" onclick="previous(${pageInfo.pageNo})">
                    <span aria-hidden="true">&laquo;</span>
                </a>
            </li>

            <c:forEach begin="1" var="i" step="1" end="${pageInfo.totalPage}">

                <li><a href="/student?method=selectByPage&pageNo=${i}">${i}</a></li>

            </c:forEach>

            <li>
                <a href="javascript:void(0)" aria-label="Next" onclick="next(${pageInfo.pageNo}, ${pageInfo.totalPage})">
                    <span aria-hidden="true">&raquo;</span>
                </a>
            </li>
        </ul>
    </nav>

    <script>
        function deleteById(id) {
            var isDelete = confirm('您确认要删除吗？')
            if (isDelete) {
                location.href = '/student?method=deleteById&id=' + id;
            }
        }
        /*function update(id) {
            var isUpdate = confirm('您确认要更新吗？')
            if (isUpdate) {
                location.href = '/student?method=updateQuery&id=' + id;
            }
        }*/
        function previous(pageNo) {
            if (pageNo > 1) {
                location.href = '/student?method=selectByPage&pageNo=' + (pageNo - 1);
            } else {
                alert('已经是第一页了！');
            }
        }
        function next(pageNo, totalPage) {
            if (pageNo < totalPage) {
                location.href = '/student?method=selectByPage&pageNo=' + (pageNo + 1);
            } else {
                alert('已经是最后一页了！');
            }
        }
    </script>
</body>
</html>
