/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/4/9 下午2:31 ~ 2024/4/9 下午2:33
 * Modified date: 2024/4/9 下午2:33
 */

package com.sdust.web.controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/setCookie")
public class SetCookieSetvlet extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("SetCookieSetvlet.service");
        Cookie cookie1 = new Cookie("goods", "IPhone");
        Cookie cookie2 = new Cookie("name", "IPhone");
        resp.addCookie(cookie1);
        resp.addCookie(cookie2);
    }
}

