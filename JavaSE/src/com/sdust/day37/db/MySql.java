/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/5/30 上午12:14
 * Modified date: 2024/5/30 上午12:14
 */

package com.sdust.day37.db;

public class MySql implements IDB{
    @Override
    public void getConnection() {
        System.out.println("MySql.getConnection");
    }
}
