package com.sdust.day2;

import org.junit.Test;

public class BasicDataType {
    //小驼峰 变量命名 方法命名
    @Test
    public void test1() {
        //byte short(char) int long
        //1、从小到大自动类型转换
        //2、从大到小需要强制类型转换，可能会丢失精度(装水的杯子大小不一样)
        char ch = 'a';
        System.out.println(ch);//a
        System.out.println(ch + 1);//97 + 1 = 98 从小到大自动类型转换
        System.out.println((char)(ch + 1));//从大到小需要强制类型转换，可能会丢失精度(装水的杯子大小不一样)
        //ascii码 大小写 'a' 97  'A'65

        double d = 3.14;
        int num = (int)d;
        System.out.println(num);//3

    }

    @Test
    public void test2() {
        //双目运算符
        int num1 = 2;
        int num2 = 3;
        int res = num1 + num2;//变量只用一次,不需要定义这个变量
        System.out.println(res);
        System.out.println(num1 - num2);
        System.out.println(num1 * num2);
        System.out.println(num1 / num2);

        System.out.println(8 / 4);
        System.out.println(8 % 4);
        System.out.println(8 / 5);
        System.out.println(8 % 5);

    }

    //单目运算符
    //num1++;
    @Test
    public void test4() {
        int num = 1;
        num = num + 1;
        System.out.println(num);
        //++(自增1)  --(自减1)
        int i = 1;
        i++;
        System.out.println(i++);//2
        System.out.println(i);//3
        System.out.println(++i);//4
    }

    //比较运算符
    @Test
    public void test5() {
        System.out.println(3 >= 5);
        System.out.println(5 < 10);
        System.out.println(5 == 10);
        System.out.println(5 <= 10);
        int num1 = 3;
        int num2 = 5;
        System.out.println(num1 != num2);
        boolean bool1 = num1 != num2;
        System.out.println(bool1);
        boolean bool2 = 3 >= 5;
        System.out.println(bool2);
    }

    //逻辑运算符
    @Test
    public void test6() {
        int score1 = 57;
        int score2 = 456;
        //比较运算符的优先级 高于 逻辑运算符
        //加了括号之后就不依赖默认优先级：()>比较运算符>逻辑运算符
        boolean res1 = (score1 >= 60) && (score2 >= 400);
        System.out.println(res1);//false
        boolean res2 = (score1 >= 60) || (score2 >= 400);
        System.out.println(res2);//true

        //短路
        int num1 = 3;
        int num2 = 5;
        //&&短路
        System.out.println(num1 < 0 && num1++ < num2);//false
        System.out.println(num1);//3
        System.out.println(num2);//5

        //||短路
        System.out.println(num1 > 0 || num1++ < num2);//false
        System.out.println(num1);//3
        System.out.println(num2);//5

        //&不短路
        System.out.println(num1 < 0 & num1++ < num2);//false
        System.out.println(num1);//4
        System.out.println(num2);//5

    }
}
