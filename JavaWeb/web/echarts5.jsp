<%--
  ~ 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
  ~ Copyright @TangXJ
  ~ Created by TangXJ
  ~ Created&Used date: 2024/5/28 下午2:23 ~ 2024/5/28 下午6:48
  ~ Modified date: 2024/5/28 下午6:47
  --%>

<%--
  Created by IntelliJ IDEA.
  User: TangXJ
  Date: 2024/5/28
  Time: 14:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data Dashboard</title>
    <link rel="stylesheet" href="/static/style.css">
    <style>
        .number-container {
            display: flex;
            flex-direction: column;
            align-items: center;
            font-family: Arial, sans-serif;
        }
        .number-display {
            font-size: 48px;
            font-weight: bold;
            color: orange;
            margin: 10px 0;
        }
        .number-title {
            font-size: 24px;
            color: #ffffff;
        }
    </style>
</head>
<body>
<div class="dashboard">
    <header>
        <h1>实时数据大屏</h1>
        <br>
        <p id="current-time"></p>
    </header>
    <section class="top-section">
        <div class="card">
            <div class="number-container">
                <div id="regNum">
                    <div class="number-title">今日挂号人次</div>
                    <div id="countUp" class="number-display" style="color: #68ffec;">3,210</div>
                </div>
                <div id="admitNum">
                    <div class="number-title">今日住院人次</div>
                    <div class="number-display" style="color: #fd7966;">123</div>
                </div>
                <div id="dischargeNum">
                    <div class="number-title">今日出院人次</div>
                    <div class="number-display" style="color: #2aff00;">123</div>
                </div>
            </div>
        </div>

        <div class="card">
            <div id="main3" style="width: 580px;height:400px;"></div>
        </div>

        <div class="card">
            <div class="number-container">
                <div id="name">
                    <div class="number-title">本院科室数量</div>
                    <div id="" class="number-display" style="color: #ffdd00;">3,210</div>
                </div>
                <div id="age">
                    <div class="number-title">本院医生人数</div>
                    <div class="number-display" style="color: #ffdd00;">123</div>
                </div>
                <div id="age2">
                    <div class="number-title">本院病房数量</div>
                    <div class="number-display" style="color: #ffdd00;">123</div>
                </div>
            </div>
        </div>

    </section>
    <section class="middle-section">
        <div class="chart">
            <div id="main1" style="width: 580px;height:400px;"></div>
        </div>
        <div class="chart">
            <div class="scrolling-info">
                <div class="number-title">叫号信息</div><br>
                <div id="scroll-container">
                    <div id="scroll-content">
                        <!-- 这里添加挂号叫号信息 -->
                        <p class="number-title">叫号信息1</p>
                        <p class="number-title">叫号信息1</p>
                        <p class="number-title">叫号信息1</p>
                        <p class="number-title">叫号信息1</p>
                        <p class="number-title">叫号信息1</p>
                        <p class="number-title">叫号信息1</p>
                        <p class="number-title">叫号信息1</p>
                        <p class="number-title">叫号信息1</p>
                        <p class="number-title">叫号信息1</p>
                        <p class="number-title">叫号信息1</p>
                        <p class="number-title">叫号信息1</p>
                        <p class="number-title">叫号信息1</p>
                        <p class="number-title">叫号信息1</p>
                        <p class="number-title">叫号信息1</p>
                        <p class="number-title">叫号信息1</p>
                        <p class="number-title">叫号信息1</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="chart">
            <div id="main2" style="width: 580px;height:400px;"></div>
        </div>
    </section>
<%--    <section class="bottom-section">--%>
<%--        <div class="chart">柱状图</div>--%>
<%--    </section>--%>
<%--    <footer>--%>
<%--        <p>累计访问人次：123456</p>--%>
<%--    </footer>--%>
</div>
<script src="/static/echarts.min.js" type="text/javascript" charset="utf-8"></script>
<script src="/static/jquery-2.1.4.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
    // 基于准备好的dom，初始化echarts实例

    var myChart1 = echarts.init(document.getElementById('main1'), 'dark');
    var myChart2 = echarts.init(document.getElementById('main2'), 'dark');
    var myChart3 = echarts.init(document.getElementById('main3'), 'dark');

    $.post(
        '/banji?method=selectBanjiCount',
        function (jsonObj) {
            console.log(jsonObj);
            var xArray = new Array();
            var yArray = new Array();
            for (var banjiCountVO of jsonObj) {
                xArray.push(banjiCountVO.name);
                yArray.push(banjiCountVO.value);
            }
            // 指定图表的配置项和数据
            var option1 = {
                title: {
                    text: '各科室业务总量'
                },
                tooltip: {
                    trigger: 'item'
                },
                legend: {
                    top: '5%',
                    left: 'center'
                },
                series: [
                    {
                        name: 'Access From',
                        type: 'pie',
                        radius: ['40%', '70%'],
                        avoidLabelOverlap: false,
                        label: {
                            show: false,
                            position: 'center'
                        },
                        emphasis: {
                            label: {
                                show: true,
                                fontSize: 40,
                                fontWeight: 'bold'
                            }
                        },
                        labelLine: {
                            show: false
                        },
                        data: [
                            { value: 1048, name: 'Search Engine' },
                            { value: 735, name: 'Direct' },
                            { value: 580, name: 'Email' },
                            { value: 484, name: 'Union Ads' },
                            { value: 300, name: 'Video Ads' }
                        ]
                    }
                ]
            };
            // 使用刚指定的配置项和数据显示图表。
            myChart1.setOption(option1);

            //-----------饼状图----------------
            option2 = {
                title: {
                    text: '各科室医生人数',
                    subtext: 'Sql Data',
                    left: 'center'
                },
                tooltip: {
                    trigger: 'item'
                },
                legend: {//(地图或书中图表的)图例，说明
                    orient: 'vertical',
                    left: 'left'
                },
                series: [
                    {
                        name: 'Count In',
                        type: 'pie',
                        radius: '50%',
                        data: jsonObj,
                        emphasis: {
                            itemStyle: {
                                shadowBlur: 10,
                                shadowOffsetX: 0,
                                shadowColor: 'rgba(0, 0, 0, 0.5)'
                            }
                        }
                    }
                ]
            };
            myChart2.setOption(option2);

            //-------------趋势图-------------------
            var option3 = {
                color: ['#80FFA5', '#00DDFF', '#37A2FF', '#FF0087', '#FFBF00'],
                title: {
                    text: '挂号人数趋势图'
                },
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'cross',
                        label: {
                            backgroundColor: '#6a7985',
                            precision: 0,
                        }
                    }
                },
                legend: {
                    data: ['人数', 'Line 2', 'Line 3', 'Line 4', 'Line 5']
                },
                toolbox: {
                    feature: {
                        // saveAsImage: {}
                    }
                },
                grid: {
                    left: '3%',
                    right: '4%',
                    bottom: '3%',
                    containLabel: true
                },
                xAxis: [
                    {
                        type: 'category',
                        boundaryGap: false,
                        data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
                    }
                ],
                yAxis: [
                    {
                        type: 'value'
                    }
                ],
                series: [

                    {
                        name: '人数',
                        type: 'line',
                        stack: 'Total',
                        smooth: true,
                        lineStyle: {
                            width: 0
                        },
                        showSymbol: false,
                        areaStyle: {
                            opacity: 0.8,
                            color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                                {
                                    offset: 0,
                                    color: 'rgb(0, 221, 255)'
                                },
                                {
                                    offset: 1,
                                    color: 'rgb(77, 119, 255)'
                                }
                            ])
                        },
                        emphasis: {
                            focus: 'series'
                        },
                        data: [120, 282, 111, 234, 220, 340, 3]
                    }
                ]
            };
            myChart3.setOption(option3);

        },
        'json'
    );
</script>
<script>
    function updateTime() {
        const now = new Date();
        const formattedTime = now.toLocaleString('zh-CN', {
            year: 'numeric',
            month: '2-digit',
            day: '2-digit',
            hour: '2-digit',
            minute: '2-digit',
            second: '2-digit'
        });
        document.getElementById('current-time').innerText = formattedTime;
    }

    updateTime();
    setInterval(updateTime, 1000); // 每秒更新一次时间

    // JavaScript to duplicate content for smooth scrolling
    // window.onload = function() {
    //     const scrollContainer = document.getElementById('scroll-container');
    //     const scrollContent = document.getElementById('scroll-content');
    //     const clone = scrollContent.cloneNode(true);
    //     scrollContainer.appendChild(clone);
    // }
</script>
</body>
</html>