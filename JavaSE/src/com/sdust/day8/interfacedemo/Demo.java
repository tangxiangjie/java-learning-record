/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/3/21 上午10:49 ~ 2024/3/21 下午7:14
 * Modified date: 2024/3/21 下午7:12
 */

package com.sdust.day8.interfacedemo;

import org.junit.Test;

import java.awt.desktop.SystemEventListener;

public class Demo {

    //argument: 参数
    public static void main(String[] args) {
        GeZi geZi = new GeZi();
        geZi.egg();
        geZi.fly();
        geZi.gugujiao();

        //多态: 声明成父类类型，new子类对象
        AbstractBird bird = new GeZi();
        bird.egg();
        //bird是站在AbstractBird这个父类的角度去看new出来的DaYan
        //bird.fly();
        //cannot resolve method 'gugujiao' in 'AbstractBird'
        //bird.gugujiao();

        //站在能不能飞 IFLy的角度去看GeZi，只能看到fly()这个方法
        IFly fly = new GeZi();
        fly.fly();
        //Cannot resolve method 'egg' in 'IFly'
        //fly.egg();
        //fly.gugujiao();
        fly = new MiFeng();
        fly.fly();
    }

    @Test
    public void testStatic() {
        //静态方法
        DaYan.guangbo1();
        DaYan.guangbo2();
        //静态属性
        System.out.println(DaYan.isBird);
        //由于静态方法不需要new对象就可以使用，所以只有对象创建后才能用的东西，它都不能写。
        //先有公共设施 再有房主     公共设施不能访问房主的一些东西
    }

}
