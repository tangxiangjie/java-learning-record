/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/4/7 下午2:14 ~ 2024/4/9 上午11:56
 * Modified date: 2024/4/9 上午11:56
 */

package com.sdust.web.service;

import com.sdust.web.pojo.entity.Teacher;
import com.sdust.web.util.PageInfo;

import java.util.List;

public interface ITeacherService {
    public abstract List<Teacher> selectAll();
    void deleteById(Integer id);
    void add(Teacher teacher);
    Teacher selectById(Integer id);
    void update(Teacher teacher);
    PageInfo<Teacher> selectByPage(Integer pageNo, Integer pageSize);
}
