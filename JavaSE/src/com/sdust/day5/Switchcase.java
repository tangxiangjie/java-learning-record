package com.sdust.day5;

import org.junit.Test;

import java.awt.desktop.SystemEventListener;
import java.util.Scanner;

public class Switchcase {

    @Test
    public void test1() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("score:");
        int score = scanner.nextInt();
        switch (score / 10) {
            case 10:
            case 9:
                System.out.println("you");
                break;
            case 8:
                System.out.println("liang");
                break;
            case 7:
                System.out.println("zhong");
                break;
            case 6:
                System.out.println("bujige");
                break;
            default:
                System.out.println("bujige");
                break;
        }
    }
}
