/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/3/21 下午2:05 ~ 2024/3/21 下午2:11
 * Modified date: 2024/3/21 下午2:11
 */

package com.sdust.day8.utildemo;

import java.util.Arrays;

public class ArraysUtilTest {
    public static void main(String[] args) {
        int[]array = {85, 47, 44};
        int sum = ArraysUtil.sum(array);
        int max = ArraysUtil.max(array);
        ArraysUtil.bubbleSort(array);
        System.out.println(sum);
        System.out.println(max);
        System.out.println(Arrays.toString(array));
    }
}
