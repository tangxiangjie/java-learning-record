/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/12 下午10:30
 * Modified date: 2024/6/12 下午10:30
 */

package com.sdust.springboot.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

//@Component
public class String2DateConverter implements Converter<String, Date> {
    private static List<SimpleDateFormat> list = new ArrayList<>();

    //静态代码块中的代码只会执行一次
    static {
        list.add(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
        list.add(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss"));
        list.add(new SimpleDateFormat("yyyy-MM-dd"));
        list.add(new SimpleDateFormat("yyyy/MM/dd"));
    }

    @Override
    public Date convert(String source) {
        //遍历所有的格式，如果符合就直接parse之后返回，不符合就抛出异常
        for (SimpleDateFormat simpleDateFormat : list) {
            try {
                return simpleDateFormat.parse(source);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return null;
    }
}
