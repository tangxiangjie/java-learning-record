<%@ page import="java.util.List" %>
<%@ page import="com.sdust.mvc.pojo.entity.Student" %><%--
  ~ 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
  ~ Copyright @TangXJ
  ~ Created by TangXJ
  ~ Created&Used date: 2024/6/2 下午4:39
  ~ Modified date: 2024/6/2 下午4:39
  --%>

<%--
  Created by IntelliJ IDEA.
  User: TangXJ
  Date: 2024/6/2
  Time: 16:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page isELIgnored="false"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h1>Student_list.jsp</h1>
${students}
<%
    List<Student> students = (List<Student>) request.getAttribute("students");
%>
<table border="1">
    <tr>
        <td>ID</td>
        <td>姓名</td>
        <td>年龄</td>
        <td>性别</td>
    </tr>
    <%
        for (Student student : students) {
    %>
    <tr>
        <td><%=student.getId()%></td>
        <td><%=student.getName()%></td>
        <td><%=student.getAge()%></td>
        <td><%=student.getGender()%></td>
    </tr>
    <%
        }
    %>
</table>
</body>
</html>
