/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/3/20 下午2:34 ~ 2024/3/20 下午3:20
 * Modified date: 2024/3/20 下午2:56
 */

package com.sdust.day7.cal;

public class Sub extends Cal{

    //有默认的无参构造

    @Override
    public int getRes() {
        return num1 - num2;
    }
}
