/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/5 上午12:54
 * Modified date: 2024/6/5 上午12:54
 */

package com.sdust.spring.dao.impl;

import com.sdust.spring.dao.IStudentDao;

public class StudentDaoImpl implements IStudentDao {
    @Override
    public void selectAll() {
        System.out.println("StudentDaoImpl.selectAll");
    }
}
