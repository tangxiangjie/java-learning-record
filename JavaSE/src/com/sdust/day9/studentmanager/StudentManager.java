/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/3/20 上午9:43 ~ 2024/3/23 下午9:42
 * Modified date: 2024/3/23 下午9:42
 */

package com.sdust.day9.studentmanager;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Scanner;

public class StudentManager {

    public static void main(String[] args) {
        ArrayList<Student> studentList = setStudentList();
//        for (Student student : studentList) {
//            System.out.println(student);
//        }
        searchStudent(studentList);
    }

    private static ArrayList<Student> setStudentList() {
        ArrayList<Student> studentList = new ArrayList<>();
        while (true) {
            System.out.println("----开始录入学生信息 (输入-1结束信息录入)----");
            int id = readId();
            if (id <= 0) {
                System.out.println("----信息录入结束----");
                break;
            }
            String name = readName();
            int age = readAge();
            String gender = readGender();

            Student student = new Student(id, name, age, gender);
            studentList.add(student);
        }
        return studentList;
    }

    private static void searchStudent(ArrayList<Student> studentList) {
        while (true) {//如果搜索不到对应信息，打印一个：没有该信息的学生
            int order = readOrder();
            if (order == 0) {
                System.out.println("退出");
                break;
            }
            switch (order) {
                case 1:
                    int idSearch = readId();
                    printResult(searchById(studentList, idSearch));
                    break;
                case 2:
                    String nameSearch = readName();
                    printResult(searchByName(studentList, nameSearch));
                    break;
                case 3:
                    int ageSearch = readAge();
                    printResult(searchByAge(studentList, ageSearch));
                    break;
                case 4:
                    String genderSearch = readGender();
                    printResult(searchByGender(studentList, genderSearch));
                    break;
            }
        }
    }

    private static int readOrder() {
        Scanner scanner = new Scanner(System.in);
        int order = 0;

        while (true) {
            System.out.println("按照id搜索输入1");
            System.out.println("按照name搜索输入2");
            System.out.println("按照age搜索输入3");
            System.out.println("按照gender搜索输入4");
            System.out.println("退出输入0");
            if (scanner.hasNextInt()) {
                order = scanner.nextInt();
                if (order >= 0 && order <= 4) {
                    break;
                } else {
                    System.out.println("必须是0-4之间的数字，请重新输入: ");
                }
            } else {
                System.out.println("必须是数字，请重新输入: ");
                scanner.next();
            }
        }
        return order;
    }

    public static int readId() {
        Scanner scanner = new Scanner(System.in);
        int id = 0;
        while (true) {
            System.out.println("请输入id: ");
            if (scanner.hasNextInt()) {
                id = scanner.nextInt();
                break;
            } else {
                System.out.println("输入的id格式不正确，请重新输入: ");
                scanner.next();
            }
        }
        return id;
    }

    public static String readName() {
        Scanner scanner = new Scanner(System.in);
        String name = "";
        while (true) {
            System.out.println("请输入姓名: ");
            name = scanner.nextLine().trim();
            if (name.isEmpty()) {
                System.out.println("姓名不能为空，请重新输入: ");
            } else {
                break;
            }
        }
        return name;
    }

    public static int readAge() {
        Scanner scanner = new Scanner(System.in);
        int age = 0; // 初始化 age 变量，用于存储用户输入的年龄
        // 循环直到用户输入了合法的年龄值
        while (true) {
            System.out.println("请输入年龄: ");
            // 检查输入是否为整数
            if (scanner.hasNextInt()) {
                age = scanner.nextInt(); // 如果是整数，读取用户输入的年龄值
                break; // 跳出循环，继续执行后续代码
            } else {
                // 如果输入不是整数，则清空输入缓冲区，并提示用户重新输入
                System.out.println("输入的年龄格式不正确，请重新输入: ");
                scanner.next(); // 清空输入缓冲区 **********
            }
        }
        return age;
    }

    public static String readGender() {
        Scanner scanner = new Scanner(System.in);
        String gender = "";
        while (true) {
            System.out.println("请输入性别(男/女):");
            gender = scanner.nextLine().trim(); // 读取用户输入的性别并去除前后空格
            if ("男".equals(gender) || ("女").equals(gender)) {
                break;
            } else {
                System.out.println("非法输入，请重新输入: ");
            }
        }
        return gender;
    }

    public static ArrayList<Student> searchById(ArrayList<Student> studentList, int idSearch) {
        ArrayList<Student> foundList = new ArrayList<>();
        for (Student student : studentList) {
            if (student.getId() == idSearch) {
                foundList.add(student);
            }
        }
        return foundList;
    }

    public static ArrayList<Student> searchByName(ArrayList<Student> studentList, String nameSearch) {
        ArrayList<Student> foundList = new ArrayList<>();
        for (Student student : studentList) {
            if (nameSearch.equals(student.getName())) {
                foundList.add(student);
            }
        }
        return foundList;
    }

    public static ArrayList<Student> searchByAge(ArrayList<Student> studentList, int ageSearch) {
        ArrayList<Student> foundList = new ArrayList<>();
        for (Student student : studentList) {
            if (student.getAge() == ageSearch) {
                foundList.add(student);
            }
        }
        return foundList;
    }

    public static ArrayList<Student> searchByGender(ArrayList<Student> studentList, String genderSearch) {
        ArrayList<Student> foundList = new ArrayList<>();
        for (Student student : studentList) {
            if (genderSearch.equals(student.getGender())) {
                foundList.add(student);
            }
        }
        return foundList;
    }

    public static void printResult(ArrayList<Student> foundList) {
        System.out.println("------------------------------");
        if (!foundList.isEmpty()) {
            System.out.println("搜索到" + foundList.size() + "个结果");
            for (Student student : foundList) {
                System.out.println(student);
            }
        } else {
            System.out.println("没有搜索到符合条件的学生!");
        }
        System.out.println("------------------------------");
    }
}
