/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/7 上午1:14
 * Modified date: 2024/6/7 上午1:14
 */

package com.sdust.spring.proxy.staticc.demo2;

public class BankAccountProxy implements BankAccount {
    private RealBankAccount realBankAccount;

    public BankAccountProxy(RealBankAccount realBankAccount) {
        this.realBankAccount = realBankAccount;
    }

    @Override
    public void withdraw(double amount) {
        System.out.println("Proxy: Logging before withdrawing.");
        realBankAccount.withdraw(amount);
        System.out.println("Proxy: Logging after withdrawing.");
    }
}
