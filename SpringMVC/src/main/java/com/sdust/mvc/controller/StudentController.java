/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/5/31 上午9:56
 * Modified date: 2024/5/31 上午9:56
 */

package com.sdust.mvc.controller;

import com.sdust.mvc.pojo.entity.Student;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/student")
public class StudentController {

    ///student/add
    @RequestMapping("/add1")
    public void add1(String name, Integer age, String gender) { //String age
        //返回值是void会报404错误   好像是因为请求了/student/student/add
        System.out.println("StudentController.add");
        System.out.println("name" + name);
        System.out.println("age" + age);
        System.out.println("gender" + gender);
    }

    @RequestMapping("/add2")
    public ModelAndView add2(Student student) { //String age
        System.out.println("StudentController.add");
        System.out.println(student);

        // Model:数据  View:界面
        ModelAndView modelAndView = new ModelAndView();
        // request.setAttribute("student", student); // model
        modelAndView.addObject("student", student);
        // request.getRequestDispatcher("/student_info.jsp").forward(request, response); // view
        modelAndView.setViewName("/student_info.jsp");
        return modelAndView;//  /student/add(没有.jsp了，所以方法名要和地址一致)
    }

    // 查询一般用Get, 查询用Post
    // 题外话: Get可能会把原来拼接的一些参数替换掉
    // @RequestMapping(value = "/add", method = {RequestMethod.POST, RequestMethod.GET})
    @RequestMapping(value = "/add3", method = RequestMethod.POST)//405: Request method 'GET' not supported
    public String add3(Student student, Model model) { //String age
        System.out.println("StudentController.add");
        System.out.println(student);

        // request.setAttribute("student", student); // model
        // request.getRequestDispatcher("/student_info.jsp").forward(request, response); // view
        model.addAttribute("student", student);
        return "/student_info.jsp";
    }

    @RequestMapping(value = "/add")
    public void add(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ServletException, IOException {
        System.out.println("StudentController.add");
        String name = request.getParameter("name");
        String age = request.getParameter("age");
        String gender = request.getParameter("gender");
        Student student = new Student(null, name, Integer.parseInt(age), gender);
        System.out.println(student);
        request.setAttribute("student", student);
        request.getRequestDispatcher("/student_info.jsp").forward(request, response);

    }

    // /student/deleteAll?ids=1&ids=2&ids=4
    @RequestMapping("/deleteAll")
    public void deleteAll(Integer[] ids) {
        System.out.println(Arrays.toString(ids));
    }

    @RequestMapping("/deleteById")
    public String deleteById(Integer id) {
        System.out.println("StudentController.deleteById");
        System.out.println("Id: " + id);
        return "redirect:/student/selectAll";
    }

    @RequestMapping("/selectAll")
    public ModelAndView selectAll() {
        System.out.println("StudentController.selectAll");
        Student student1 = new Student(1, "张三", 23, "男");
        Student student2 = new Student(2, "李四", 22, "男");
        Student student3 = new Student(3, "王五", 21, "女");
        Student student4 = new Student(4, "赵六", 24, "男");
        Student student5 = new Student(5, "田七", 19, "女");

        List<Student> students = new ArrayList<>();
        students.add(student1);
        students.add(student2);
        students.add(student3);
        students.add(student4);
        students.add(student5);

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("students", students);

        //modelAndView.setViewName("/student_list.jsp");
        modelAndView.setViewName("student_list");
        return modelAndView;
    }

    @RequestMapping("selectById")
    @ResponseBody
    public Student selectById(String id) {
        Student student = new Student(114514, "田所浩二", 54, "男");
        //发Ajax请求  需要返回JSON格式 ------->   这一句很关键
        return student;//默认的return 是转发的页面的意思
                        //加个@ResponseBody 响应体   集合的形式 同理
    }

    @RequestMapping("/select")
    @ResponseBody
    public List<Student> select() {
        Student student1 = new Student(1, "张三", 23, "男");
        Student student2 = new Student(2, "李四", 22, "男");
        Student student3 = new Student(3, "王五", 21, "女");

        List<Student> students = new ArrayList<>();
        students.add(student1);
        students.add(student2);
        students.add(student3);
        //要返回Result的话，再封装一层 带着data、code、msg等  一起返回就行了，嵌套的JSON
        return students;
    }

    //注解可以加在 类 方法 属性上边  还可以加在参数上边--->可以快速实现 如果传来的参数没有值的处理。设置默认值的操作
    /**
    * 1. value(输入参数名和传入方法的参数名做一个映射,保证即使不一致也能拿到)
    * 2. defaultvalue
    * 3. required
    * defaultvalue和required同时使用会使required失效(默认值为true)
    * 由于required默认值是ture,所以如果没有使用defaultvalue的话，相应的参数没有传 也会报错
    * */
    @RequestMapping("/selectByPage")
    public ModelAndView  selectByPage(@RequestParam(value = "page", defaultValue = "1") Integer pageNo,
                                      @RequestParam(defaultValue = "5") Integer pageSize,
                                      @RequestParam(required = true, defaultValue = "10") Integer totalPage) {
        System.out.println("pageNo: " + pageNo);
        System.out.println("pageSize: " + pageSize);
        System.out.println("totalPage: " + totalPage);

        Student student1 = new Student(1, "张三", 23, "男");
        Student student2 = new Student(2, "李四", 22, "男");

        List<Student> students = new ArrayList<>();
        students.add(student1);
        students.add(student2);

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("students", students);
        //modelAndView.setViewName("/student_list.jsp");
        modelAndView.setViewName("student_list");

        return modelAndView;
    }
}
