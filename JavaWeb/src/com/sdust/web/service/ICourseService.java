/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/4/9 上午10:19 ~ 2024/4/9 上午10:20
 * Modified date: 2024/4/9 上午10:20
 */

package com.sdust.web.service;

import com.sdust.web.pojo.entity.Course;
import com.sdust.web.util.PageInfo;

import java.util.List;

public interface ICourseService {
    public abstract List<Course> selectAll();
    void deleteById(Integer id);
    void add(Course course);//没有id
    Course selectById(Integer id);
    void update(Course course);//有id

    PageInfo<Course> selectByPage(Integer pageNo, Integer pageSize);
}
