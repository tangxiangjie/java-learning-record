/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/4/15 下午2:41
 * Modified date: 2024/4/15 下午2:41
 */

package com.sdust.web.util;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JSONUtil {

    public static void toJSON(HttpServletResponse resp, Object object) {
        try {
            resp.setContentType("text/html;charset=utf-8");
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.writeValue(resp.getWriter(), object);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
