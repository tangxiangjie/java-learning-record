/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/3/21 上午11:30 ~ 2024/3/21 下午2:24
 * Modified date: 2024/3/21 下午2:24
 */

package com.sdust.day8.staticmethoddemo;

public class Student {

    //属性、成员变量——有对象才能调用
    public int id;
    private String name;
    //静态变量
    public static String country = "China";

    //成员方法、实例方法、非静态方法——有对象才能调用
    public void show() {
        System.out.println("Student.show");
    }

    //静态方法——没对象也能用。自然里边并不能用成员方法
    public static void staticShow() {
        System.out.println("Student.staticShow");
        //show();//Non-static method 'show()' cannot be referenced from a static context
        //this.name = "China";//当前类new出的某个对象的属性
        //super.xx  父类new出的某个对象的属性
        System.out.println(country);
    }

}
