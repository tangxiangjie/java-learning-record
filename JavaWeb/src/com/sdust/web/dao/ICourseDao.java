/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/4/9 上午10:22 ~ 2024/4/9 上午10:23
 * Modified date: 2024/4/9 上午10:23
 */

package com.sdust.web.dao;

import com.sdust.web.pojo.entity.Course;

import java.util.List;

public interface ICourseDao {
    public abstract List<Course> selectAll();
    void deleteById(Integer id);
    void add(Course course);//没有id
    Course selectById(Integer id);
    void update(Course course);//有id

    List<Course> selectByPage(int offset, Integer pageSize);

    int selectTotalCount();
}
