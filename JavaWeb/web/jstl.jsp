<%@ page import="java.util.ArrayList" %>
<%@ page import="com.sdust.web.pojo.entity.Student" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Map" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%--
  ~ 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
  ~ Copyright @TangXJ
  ~ Created by TangXJ
  ~ Created&Used date: 2024/4/18 上午9:52 ~ 2024/4/18 上午10:07
  ~ Modified date: 2024/4/18 上午10:07
  --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
    <%--1、向域对象放数据--%>
    <%
        pageContext.setAttribute("age", 23);
        session.setAttribute("gender", "男");
    %>
    <c:set var="age" value="24" scope="request"></c:set>
    ${age} <br/>
    <hr/>
    <%--2、条件判断--%>
    <c:if test="${gender=='男'}">
        男
    </c:if>
    <c:if test="${gender=='女'}">
        男
    </c:if>
    <hr>
    <%--3、多条件判断--%>
    <h1>3、多条件判断</h1>
    <c:set var="score" value="89"></c:set>
    <c:choose>
        <c:when test="${score >= 90 && score <= 100}">
            优秀
        </c:when>
        <c:when test="${score >= 80 && score < 90}">
            良好
        </c:when>
        <c:when test="${score >= 70 && score < 80}">
            一般
        </c:when>
        <c:when test="${score >= 60 && score < 70}">
            及格
        </c:when>
        <c:otherwise>
            不及格
        </c:otherwise>
    </c:choose>
    <hr>
    <%--4、集合遍历
        List<Student>
        Map<String, String>
        Map<String, Student>
    --%>
    <%--打印1-10--%>
    <h1>4、集合遍历 打印1-10</h1>
    <c:forEach begin="1" end="10" var="i" step="1">
        ${i}_
    </c:forEach>
    <hr>
    <%--遍历集合--%>
    <h1>4、集合遍历 遍历集合</h1>
    <%
        List<Student> list = new ArrayList<>();
        Student student1 = new Student(1, "zhangsan1", 23, "男" ,1);
        Student student2 = new Student(2, "zhangsan2", 23, "男" ,1);
        Student student3 = new Student(3, "zhangsan3", 23, "男" ,2);
        list.add(student1);
        list.add(student2);
        list.add(student3);
        application.setAttribute("list", list);
    %>
    <c:forEach items="${list}" var="student">
        ${student.id}--${student.name}--${student.age}<br>
    </c:forEach>
    <hr>
    <%--Map<String, String>--%>
    <%
        Map<String, String> map = new HashMap<>();
        map.put("cn", "中国");
        map.put("us", "美国");
        request.setAttribute("map", map);
    %>
    <c:forEach items="${map}" var="entry">
        key:${entry.key}__value:${entry.value}<br>
    </c:forEach>
    <hr/>
    <%--Map<String, Student>--%>
    <%
        Map<String, Student> studentMap = new HashMap<>();
        studentMap.put("student1", student1);
        studentMap.put("student2", student2);
        studentMap.put("student3", student3);
        request.setAttribute("studentMap", studentMap);
    %>
    <c:forEach items="${studentMap}" var="entry">
        key:${entry.key}__value:${entry.value}__value.attribute(name):${entry.value.name}<br>
    </c:forEach>
</body>
</html>
