<%--
  ~ 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
  ~ Copyright @TangXJ
  ~ Created by TangXJ
  ~ Created&Used date: 2024/5/25 下午3:40 ~ 2024/5/25 下午3:41
  ~ Modified date: 2024/5/25 下午3:41
  --%>

<%--
  Created by IntelliJ IDEA.
  User: TangXJ
  Date: 2024/5/25
  Time: 15:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>数字美化展示</title>
    <style>
        .number-container {
            display: flex;
            flex-direction: column;
            align-items: center;
            font-family: Arial, sans-serif;
        }
        .number-display {
            font-size: 48px;
            font-weight: bold;
            color: orange;
            margin: 10px 0;
        }
        .number-title {
            font-size: 24px;
            color: #333;
        }
    </style>
</head>
<body>
<div class="number-container">
    <div>
        <div class="number-title">今日门诊人次</div>
        <div class="number-display" style="color: #c23531;">3,210</div>
    </div>
    <div>
        <div class="number-title">今日急诊人次</div>
        <div class="number-display" style="color: #2f4554;">1,230</div>
    </div>
    <div>
        <div class="number-title">今日住院人次</div>
        <div class="number-display" style="color: #61a0a8;">123</div>
    </div>
    <div>
        <div class="number-title">今日出院人次</div>
        <div class="number-display" style="color: #d48265;">123</div>
    </div>
</div>
</body>
</html>
