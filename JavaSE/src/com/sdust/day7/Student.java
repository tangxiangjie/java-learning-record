/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/3/20 上午11:14 ~ 2024/3/21 下午10:21
 * Modified date: 2024/3/21 下午10:21
 */

package com.sdust.day7;

import org.junit.Test;

//实体类：和数据库表一一对应
public class Student extends Person{
    //    private int id;
//    private String name;
//    private int age;
    private String className;

    //重载——跟方法名没关系
    public Student() {
        super();//new Person();
    }

    public Student(int id, String name, int age, String className) {
        super(id, name, age);//new Person(id,name,age)
        this.className = className;
    }


    //    public void show() {
//        System.out.println("Student.show");
//    }

    public void study() {
        System.out.println(className + "班的"+name+"正在学习");
        super.age = 12;
    }

/*    public void study2() {
        System.out.println(super.className + "班的"+name+"正在学习");
    }*/

/*    public int study2() {
        System.out.println(super.className + "班的"+name+"正在学习");
        return 0;
    }*/

    @Override//重写 覆盖   相当于标识一下   父类(不一定是直接的父类)也有这个方法
    public String toString() {
        return "Student{" +
                "className='" + className + '\'' +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    public void test2() {
        Person person = new Person();
        person.age = 12;
        person.id = 100;
        System.out.println(person);

        Student student = new Student();
        student.age = 13;
        System.out.println(student);
    }
}
