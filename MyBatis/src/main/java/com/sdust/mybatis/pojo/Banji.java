/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/3 上午12:22
 * Modified date: 2024/6/3 上午12:22
 */

package com.sdust.mybatis.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Banji {
    private Integer id;
    private String name;
    //一个班级拥有多个学生，表达一对多关系
    List<Student> studentList;
}
