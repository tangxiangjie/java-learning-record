/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/3/20 上午9:30 ~ 2024/3/21 下午5:18
 * Modified date: 2024/3/21 下午5:18
 */

package com.sdust.day6.homework;

public class Rectangle {
    private int width;
    private int height;

    //没有重名的，可以不加this
    public Rectangle() {
        width = 1;
        height = 1;
    }

    public Rectangle(int width) {
        this(1,1);//this():调用当前类的无参构造方法(有参就加参数)
        //new
        this.width = width;
        this.height = width;
    }

    public Rectangle(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int zhouChang() {
        return (width + height) * 2;
    }

    public int mianJi() {
        return width * height;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
