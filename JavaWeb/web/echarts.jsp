<%--
  ~ 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
  ~ Copyright @TangXJ
  ~ Created by TangXJ
  ~ Created&Used date: 2024/4/12 下午2:06 ~ 2024/5/29 下午12:06
  ~ Modified date: 2024/5/29 下午12:06
  --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
    <!-- 为 ECharts 准备一个定义了宽高的 DOM -->
    <div id="main1" style="width: 600px;height:400px;"></div>
    <div id="main2" style="width: 600px;height:400px;"></div>

    <script src="/static/echarts.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="/static/jquery-2.1.4.js" type="text/javascript" charset="utf-8"></script>

    <script type="text/javascript">
        // 基于准备好的dom，初始化echarts实例
        var myChart1 = echarts.init(document.getElementById('main1'));
        var myChart2 = echarts.init(document.getElementById('main2'));
        //var myChart3 = echarts.init(document.getElementById('main3'));
        $.post(
            '/banji?method=selectBanjiCount',
            function (jsonObj) {
                console.log(jsonObj);
                var xArray = new Array();
                var yArray = new Array();
                for (var banjiCountVO of jsonObj) {
                    xArray.push(banjiCountVO.name);
                    yArray.push(banjiCountVO.value);
                }
                // 指定图表的配置项和数据
                var option1 = {
                    title: {
                        text: '各班级人数柱状图'
                    },
                    tooltip: {},
                    legend: {//(地图或书中图表的)图例，说明
                        data: ['人数']//没找到在哪
                    },
                    xAxis: {
                        data: xArray
                    },
                    yAxis: {},
                    series: [
                        {
                            name: '人数',
                            type: 'bar',
                            data: yArray
                        }
                    ]
                };
                // 使用刚指定的配置项和数据显示图表。
                myChart1.setOption(option1);

                //-----------饼状图----------------
                option2 = {
                    title: {
                        text: '各班级人数饼图',
                        subtext: 'Sql Data',
                        left: 'center'
                    },
                    tooltip: {
                        trigger: 'item'
                    },
                    legend: {//(地图或书中图表的)图例，说明
                        orient: 'vertical',
                        left: 'left'
                    },
                    series: [
                        {
                            name: 'Count In',
                            type: 'pie',
                            radius: '50%',
                            data: jsonObj,
                            emphasis: {
                                itemStyle: {
                                    shadowBlur: 10,
                                    shadowOffsetX: 0,
                                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                                }
                            }
                        }
                    ]
                };
                myChart2.setOption(option2);
            },
            'json'
        );

    </script>
</body>
</html>
