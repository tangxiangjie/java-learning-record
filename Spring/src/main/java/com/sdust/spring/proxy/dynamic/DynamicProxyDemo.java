/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/7 上午1:15
 * Modified date: 2024/6/7 上午1:15
 */

package com.sdust.spring.proxy.dynamic;

import com.sdust.spring.proxy.dynamic.utils.ProxyFactory;
import com.sdust.spring.service.IStudentService;
import com.sdust.spring.service.impl.StudentServiceImpl;
import org.junit.Test;

import java.lang.reflect.Proxy;

public class DynamicProxyDemo  {

    @Test
    public void testDynamicProxy() {
        RealBankAccount realAccount = new RealBankAccount(1000.0);
        IStudentService studentService = new StudentServiceImpl();

        BankAccount proxy1 = (BankAccount) Proxy.newProxyInstance(
                realAccount.getClass().getClassLoader(),
                realAccount.getClass().getInterfaces(),
                new DynamicProxyHandler(realAccount)
        );

        IStudentService proxy2 = (IStudentService) Proxy.newProxyInstance(
                studentService.getClass().getClassLoader(),
                studentService.getClass().getInterfaces(),
                new DynamicProxyHandler(studentService)
        );

        // 为了测试没办法，上边的代码实际上不会出现在客户端当中
        proxy1.withdraw(100.0);
        // System.out.println("--------------");
        // realAccount.withdraw(100.0);
        proxy2.selectAll();
    }

    @Test
    public void testDynamicProxyFactory() {
        RealBankAccount realAccount = new RealBankAccount(1000.0); //  1
        IStudentService studentService = new StudentServiceImpl();            //  2

        /*BankAccount proxy = (BankAccount) Proxy.newProxyInstance(
                realAccount.getClass().getClassLoader(),
                realAccount.getClass().getInterfaces(),
                new DynamicProxyHandler(realAccount)
        );*/
        BankAccount proxy1 = (BankAccount) ProxyFactory.getProxyInstance(realAccount);
        IStudentService proxy2 = (IStudentService) ProxyFactory.getProxyInstance(studentService);

        // 为了测试没办法，上边的代码实际上不会出现在客户端当中
        proxy1.withdraw(100.0);
        proxy2.selectAll();
    }
}