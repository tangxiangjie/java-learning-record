package com.sdust.day2;

import org.junit.Test;

public class LoopControlStatement {

    /*循环的三要素：
    1、循环条件的初始化
    2、循环条件的控制
    3、循环条件的改变
    */
    @Test
    public void test1() {
        // while、for
        int i = 1;
        while(i <= 5) {
            System.out.println("Hello World");
            i++;
        }
    }

    @Test
    public void test2() {
        for(int i = 1; i <= 5; i++){
            System.out.println("Hello World");
        }
    }

    //累加思想(1+2+3+...+100)
    @Test
    public void test3() {
        int sum = 0;
        for(int i = 1; i <= 100; i++){
            //System.out.println(i);
            sum += i;//sum = sum + i;
        }
        System.out.println("sum = " + sum);
    }
}
