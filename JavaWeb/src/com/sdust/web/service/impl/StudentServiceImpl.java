/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/4/7 下午1:40 ~ 2024/5/25 下午4:04
 * Modified date: 2024/5/25 下午4:04
 */

package com.sdust.web.service.impl;

import com.sdust.web.dao.IStudentDao;
import com.sdust.web.dao.impl.StudentDaoImpl;
import com.sdust.web.pojo.entity.Student;
import com.sdust.web.pojo.vo.StudentBanjiVO;
import com.sdust.web.service.IStudentService;
import com.sdust.web.util.PageInfo;

import java.util.List;

public class StudentServiceImpl implements IStudentService {
    private IStudentDao studentDao = new StudentDaoImpl();

    @Override
    public List<StudentBanjiVO> selectAll() {
        //return studentDao.selectAll();
        List<StudentBanjiVO> studentList = studentDao.selectAll();
        for (StudentBanjiVO student : studentList) {
            student.setName("sdust-" + student.getName());
        }
        return studentList;
    }

    @Override
    public void deleteById(Integer id) {
        studentDao.deleteById(id);
    }

    @Override
    public void add(Student student) {
        studentDao.add(student);
    }

    @Override
    public Student selectById(Integer id) {
        return studentDao.selectById(id);
    }

    @Override
    public void update(Student student) {
        studentDao.update(student);
    }

    @Override
    public PageInfo<StudentBanjiVO> selectByPage(Integer pageNo, Integer pageSize) {
        //Service业务层为了完成PageInfo的封装，需要调用两次Dao操作
        //1.查找当前页数据
        int offset = (pageNo - 1) * pageSize;
        List<StudentBanjiVO> list = studentDao.selectByPage(offset, pageSize);
        //2.总的页数  查总的数量 Math.ceil(总数量/pageSize)
        int totalCount = studentDao.selectTotalCount();
        int totalPage = (int)Math.ceil((double)totalCount / pageSize);

        PageInfo<StudentBanjiVO> pageInfo = new PageInfo<StudentBanjiVO>(list, totalPage, pageNo, pageSize);
        return pageInfo;
    }

    @Override
    public int selectCount() {
        return studentDao.selectTotalCount();
    }
}
