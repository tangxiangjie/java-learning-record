/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/3/21 上午10:43 ~ 2024/3/21 上午11:18
 * Modified date: 2024/3/21 上午11:18
 */

package com.sdust.day8.interfacedemo;

public class DaYan extends AbstractBird implements IFly{
    static boolean isBird = true;

    @Override
    public void egg() {
        System.out.println("DaYan.egg");
    }

    @Override
    public void fly() {
        System.out.println("DaYan.fly");
    }

    public static void guangbo1() {
        System.out.println("DaYan.guangbo1");
    }

    public static void guangbo2() {
        guangbo1();
        System.out.println("DaYan.guangbo2");
    }
}
