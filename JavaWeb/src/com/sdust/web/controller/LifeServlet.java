/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/4/2 下午2:56 ~ 2024/4/7 下午1:34
 * Modified date: 2024/4/7 下午1:34
 */

package com.sdust.web.controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/life")
public class LifeServlet extends HttpServlet {

    public LifeServlet() {
        System.out.println("LifeServlet.LifeServlet");
    }

    @Override
    public void init() throws ServletException {
        super.init();
        System.out.println("LifeServlet.init");
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("LifeServlet.service");
    }

    @Override
    public void destroy() {
        super.destroy();
        System.out.println("LifeServlet.destroy");
    }
}
