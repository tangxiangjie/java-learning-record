/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/3/20 下午1:35 ~ 2024/3/21 下午10:15
 * Modified date: 2024/3/21 下午10:15
 */

package com.sdust.day8.day7diffpackinherittest;

import com.sdust.day7.Person;

public class Worker extends Person {
    public Worker() {

    }

    public Worker(int id, String name, int age) {
        super(id, name, age);
        //Person person = new Person();
        //person.name = "11";
        Worker worker = new Worker();
        worker.name = "11";
    }

    void test2() {
        id = 12;
        super.show();
        System.out.println(super.name);
    }


}
