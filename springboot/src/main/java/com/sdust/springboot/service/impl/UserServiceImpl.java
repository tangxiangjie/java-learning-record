/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/11 下午4:55
 * Modified date: 2024/6/11 下午4:55
 */

package com.sdust.springboot.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sdust.springboot.mapper.UserMapper;
import com.sdust.springboot.pojo.entity.User;
import com.sdust.springboot.pojo.query.UserQuery;
import com.sdust.springboot.service.IUserService;
import com.sdust.springboot.utils.PageResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements IUserService {
    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
    @Autowired
    private UserMapper userMapper;

    @Override
    public List<User> selectAll() {
        return userMapper.selectAll();
    }

    /*@Override
    public PageInfo list(UserQuery userQuery) {
        int offset = (userQuery.getPage() - 1) * userQuery.getLimit();
        List<User> list = userMapper.list(offset, userQuery.getLimit(), userQuery);
        int totalCount = userMapper.selectTotalCount(userQuery);
        return PageInfo.ok(totalCount, list);
    }*/

    @Override
    public PageResult list(UserQuery userQuery) {
        //记录开始时间
        /*long begin = System.currentTimeMillis();*/

        //首先使用PageHelper开启分页
        PageHelper.startPage(userQuery.getPage(), userQuery.getLimit());
        //开启分页之后 Limit offset, pageSize 这个代码PageHelper帮我们做了
        List<User> list = userMapper.list(userQuery);
        //查找总数量PageHelper自动帮我们完成
        //int totalCount = userMapper.selectTotalCount(userQuery);
        PageInfo pageInfo = new PageInfo(list);
        int totalCount = (int) pageInfo.getTotal();

        //记录结束时间
        /*try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }*/
        /*long end = System.currentTimeMillis();
        long timeCost = end - begin;
        if (timeCost >= 3000) {
            logger.error("========执行结束，耗时{}毫秒========", timeCost);
        } else if (timeCost >= 2000) {
            logger.warn("========执行结束，耗时{}毫秒========", timeCost);
        } else {
            logger.info("========执行结束，耗时{}毫秒========", timeCost);
        }*/
        return PageResult.ok(totalCount, list);
    }

    @Override
    public void deleteById(Integer id) {
        userMapper.deleteById(id);
    }

    @Override
    public void deleteSelected(Integer[] ids) {
        userMapper.deleteSelected(ids);
    }

    @Override
    public void add(User user) {
        userMapper.add(user);
    }

    @Override
    public User selectById(Integer id) {
        return userMapper.selectById(id);
    }

    @Override
    public void updateForm(User user) {
        userMapper.updateForm(user);
    }

    @Override
    public User login(String name, String password) {
        return userMapper.login(name, password);
    }
}
