<%--
  ~ 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
  ~ Copyright @TangXJ
  ~ Created by TangXJ
  ~ Created&Used date: 2024/4/9 下午3:15 ~ 2024/4/17 下午3:58
  ~ Modified date: 2024/4/17 下午3:58
  --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" type="text/css" href="static/layui/css/layui.css"/>
</head>
<body>
    <form id="formId" method="post">
        用户名<input type="text" name="username"><br>
        密码<input type="password" name="password"><br>
        验证码<input type="text" name="captcha"><img id="verifyCodeId" src="/verifyCode" onclick="refresh()"> <br><%--被拦截--%>
        <input type="button" onclick="submitForm()" value="登录">
    </form>

    <script src="static/jquery-2.1.4.js" type="text/javascript" charset="utf-8"></script>
    <script src="static/layui/layui.js" type="text/javascript" charset="utf-8"></script>
    <script src="static/mylayer.js" type="text/javascript" charset="utf-8"></script>
    <script>
        function refresh() {
            $('#verifyCodeId').attr('src', '/verifyCode?' + Math.random());
        }
        function submitForm() {
            //{"name":"zhangsan","password":"1234"}
            $.post(
                '/user?method=login',
                $('#formId').serialize(),//函数 用小括号传参  犯病
                //{"name":"zhangsan","password":"1234"}
                function (result) {
                    console.log(result);
                    if (result.code == 0) {
                        mylayer.okUrl(result.msg, '/');
                        //location.href='/';
                    } else {
                        mylayer.errorMsg(result.msg);
                    }
                },
                'json'
            );
        }
    </script>
</body>
</html>
