/*
* update:day 6 2024.3.19
* */
package com.sdust.day4;

import org.junit.Test;

import java.util.Scanner;

public class Homework2 {

    /*1、int[] scores={0,0,1,2,3,5,4,5,2,8,7,6,9,5,4,8,3,1,0,2,4,8,7,9,5,2,1,2,3,9};
    求出上面数组中0-9分别出现的次数*/
    //①10个if-else（有很多浪费时间的判断语句）
    //②switch-case
    //③双重for循环：效率较低，外层数字0-9，内层比较 i统计i出现次数 j遍历数组的循环变量
    //④哈希
    @Test
    public void solveProblem1() {
        int[] scores = {0,0,1,2,3,5,4,5,2,8,7,6,9,5,4,8,3,1,0,2,4,8,7,9,5,2,1,2,3,9};
        int[] count = new int[10];
        for (int i = 0; i < scores.length; i++) {
            count[scores[i]]++;
        }
        for (int i = 0; i < count.length; i++) {
            System.out.println(i + "出现的次数为: " + count[i]);
        }
    }

    /*2、int[] scores={0,0,1,2,3,5,4,5,2,8,7,6,9,5,4,8,3,1,0,2,4,8,7,9,5,2,1,2,3,9};
    要求求出其中的奇数个数和偶数个数。*/
    @Test
    public void solveProblem2() {
        int[] scores = {0,0,1,2,3,5,4,5,2,8,7,6,9,5,4,8,3,1,0,2,4,8,7,9,5,2,1,2,3,9};
        int cnt_odd = 0;
        int cnt_even = 0;
        for (int i = 0; i < scores.length; i++) {
            if (scores[i] %2 == 1){
                cnt_odd++;
            } else {
                cnt_even++;
            }
        }
//        System.out.println(scores.length);
        System.out.println("奇数出现的次数为: " + cnt_odd);
        System.out.println("偶数出现的次数为: " + cnt_even);
    }

    //判断回文串
    @Test
    public void solveProblem3() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("请输入你要判断的字符串: ");
        String str = scanner.next();
        System.out.println(str);
        //char[] arr = str.toCharArray();
        char[] arr = str.toCharArray();//toCharArray().var kuai
        //boolean flag = true;
        boolean isHuiWen = true;
        for (int i = 0; i < arr.length / 2; i++) {
            if (arr[i] != arr[arr.length - i - 1]) {
                isHuiWen = false;
                break;
            }
        }
        if (isHuiWen) {
            System.out.println(str + "是回文串");
        } else {
            System.out.println(str + "不是回文串");
        }
    }

    //统计字符数 A 65 a 97
    @Test
    public void solveProblem5() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("请输入你要统计的字符串: ");
        String str = scanner.nextLine();
        System.out.println(str);
        char arr[] = str.toCharArray();
        int cnt_num = 0;
        int cnt_let = 0;
        int cnt_oth = 0;
        int cnt_space = 0;
        for (int i = 0; i < arr.length; i++) {
            if ((arr[i] >= 'a' && arr[i] <= 'z') || (arr[i] >= 'A' && arr[i] <= 'Z')) {
                cnt_let++;
            } else if (arr[i] >= '0' && arr[i] <= '9') {
                cnt_num++;
            } else if (arr[i] == ' ') {
                cnt_space++;
            } else {
                cnt_oth++;
            }
        }
        System.out.println("英文字母个数为: " + cnt_let);
        System.out.println("空格个数为: " + cnt_space);
        System.out.println("数字个数为: " + cnt_num);
        System.out.println("其他字符个数为: " + cnt_oth);
    }
}
