/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/4/12 上午9:51 ~ 2024/4/17 下午2:59
 * Modified date: 2024/4/17 下午2:59
 */

package com.sdust.web.test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sdust.web.pojo.entity.Person;
import com.sdust.web.util.MD5Util;
import org.junit.Test;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class JSONTest {

    @Test
    public void testMD5() {
        System.out.println(MD5Util.MD5Encode("abd"));
        System.out.println(MD5Util.MD5Encode("American"));
        System.out.println(MD5Util.MD5Encode("唐相杰"));
        System.out.println(MD5Util.MD5Encode("梁龙啸"));
        System.out.println(MD5Util.MD5Encode("谭竟优"));
        System.out.println(MD5Util.MD5Encode("Xiangjie*409"));
        System.out.println(MD5Util.MD5Encode("xiangjie409"));
        System.out.println(MD5Util.MD5Encode("xiangjie*409"));
        System.out.println(MD5Util.MD5Encode("zy1024"));
        //4911E516E5AA21D327512E0C8B197616
        //C6C6F067A3EAD9CE67A1C67A790FA26B
        //A82D8ACF117FCEE5AC9586531C8105D3
        //76841B8A5A95A3C723515EE341EC0EA7
        //5DAC2CF3010697D34467D69F1DD58666
        //8494B511BA80EC802E055F750C593253
        //BA33DCF8CF3EA087813A1D43B34DFF7B
        //3E2202CF2D3BA0B9E717147C0760E5B6
        //08999C6EBBE769921CF0B2265D359D60
    }

    //Java 对象转为JSON
    @Test
    public void test1() throws JsonProcessingException {
        //1. 创建Person对象
        Person person = new Person();
        person.setId(12);
        person.setName("张三");
        person.setAge(12);
        person.setGender("男");
        person.setBirthday(new Date());
        //2.创建Jackson核心对象
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(person);
        System.out.println(json);
    }

    @Test
    public void test2() throws JsonProcessingException {
        //1. 创建Person对象
        Map<String,Object> map = new HashMap<String,Object>();
        map.put("name","张三");
        map.put("age",23);
        map.put("gender","男");
        //2. 创建Jackson的核心对象  ObjectMapper
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(map);
        System.out.println(json);
    }

    @Test
    //演示 JSON字符串转为Java对象
    public void  test3() throws IOException {
        //1. 初始化 创建一个JSON字符串
        String json = "{\"id\":12,\"name\":\"张三\",\"gender\":\"男\",\"birthday\":1712889197268}";
        //2. 创建Jackson的核心对象  ObjectMapper
        ObjectMapper mapper = new ObjectMapper();
        //3. json字符串转换为java对象
        Person person = mapper.readValue(json, Person.class);
        System.out.println(person);
    }
}
