/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/5/22 上午10:01 ~ 2024/5/22 上午10:54
 * Modified date: 2024/5/22 上午10:54
 */

package com.sdust.day34;

import org.junit.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexDemo {

    @Test
    public void test1() {
        String[] array = {"2019 5013 08", "2019-5013-08", "2019501308"};
        String regex = "(\\d{4})[\\s-]?(\\d{4})[\\s-]?(\\d{2})";
        Pattern pattern = Pattern.compile(regex);
        for (String str : array) {
            Matcher matcher = pattern.matcher(str);
            if (matcher.find()) {
                System.out.println(matcher.group(0)); //匹配的整体
                System.out.println(matcher.group(1) + "-" + matcher.group(2) + "-" + matcher.group(3));
                //System.out.println("***************");
            }
        }
    }

    @Test
    public void test2() {
        // 默认就找一次  加上g表示全局模式，java没搞明白怎么整  if --->  while即可
        String[] array = {"2019 5013 08", "2019-5013-08", "2019501308","0532 1234567   0532 1234567"};
        String regex = "(\\d{4})[\\s-]?(\\d{4})[\\s-]?(\\d{2})";
        Pattern pattern = Pattern.compile(regex);
        for (String str : array) {
            Matcher matcher = pattern.matcher(str);
            while (matcher.find()) {
                //System.out.println(matcher.group(0)); //匹配的整体
                System.out.println(matcher.group(1) + "-" + matcher.group(2) + "-" + matcher.group(3));
                //System.out.println("***************");
            }
        }
    }

    @Test
    public void testMatch() {
        //public boolean matches(String regex)
        //告知此字符串是否匹配给定的正则表达式。
        //当且仅当此字符串匹配给定的正则表达式时，返回 true
        //dhfidhf@gamil.com  343546545@qq.com.cn
        String regex = "[a-zA-Z0-9_-]+@[a-zA-Z0-9]+(\\.[a-zA-Z0-9]+)+";
        //String mail = "y384f_-df@dffd.cn.gov";
        String mail = "769244616@qq.com";
        if (mail.matches(regex)) {
            System.out.println("是邮箱");
        } else {
            System.out.println("不是邮箱");
        }
    }

    @Test
    public void testSplit1() {
        String regex = "";
        String str = "Hello Android Java PHP";
        String[] array = str.split(" ");
        for (String string : array) {
            System.out.println(string);
        }
    }

    @Test
    public void testSplit2() {
        //public String[] split(String regex)
        //根据给定正则表达式的匹配拆分此字符串。
        //字符串数组，它是根据给定正则表达式的匹配拆分此字符串确定的
        String regex = "\\d+";
        String str = "dfghu2344dfhi5644ffdhfi44EHHE35fhdf1235df";
        String[] array = str.split(regex);
        for (String string : array) {
            System.out.println(string);
        }
    }

    @Test
    public void testReplace() {
        //public String replaceAll(String regex,String replacement)
        //使用给定的 replacement 替换此字符串所有匹配给定的正则表达式的子字符串。
        // 数字变成：#NUMBER#
        String regex = "\\d+";
        String string = "ehrifdi3456dfhdfhoDSf223dffhfd34hi";
        String result = string.replaceAll(regex, "#NUMBER#");
        System.out.println(result);
    }
}
