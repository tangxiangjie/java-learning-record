/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/11 下午4:45
 * Modified date: 2024/6/11 下午4:45
 */

package com.sdust.springboot.mapper;

import com.sdust.springboot.pojo.entity.User;
import com.sdust.springboot.pojo.query.UserQuery;

import java.util.List;

public interface UserMapper {
    List<User> selectAll();

    //mybatis 3.4.1之前不允许传多个参数，需要加@Param 封装成一个Map（key value）给传进去
    // List<User> list(@Param("offset") int offset, @Param("limit") Integer limit, @Param("userQuery") UserQuery userQuery);

    // List<User> list(int offset, Integer limit, UserQuery userQuery);

    List<User> list(UserQuery userQuery);

    int selectTotalCount(UserQuery userQuery);

    void deleteById(Integer id);

    void deleteSelected(Integer[] ids);

    void add(User user);

    User selectById(Integer id);

    void updateForm(User user);

    User login(String name, String password);
}
