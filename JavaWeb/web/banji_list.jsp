<%@ page import="com.sdust.web.pojo.vo.StudentBanjiVO" %>
<%@ page import="java.util.List" %>
<%@ page import="com.sdust.web.pojo.entity.Banji" %><%--
  ~ 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
  ~ Copyright @TangXJ
  ~ Created by TangXJ
  ~ Created&Used date: 2024/4/8 下午4:46 ~ 2024/4/9 上午9:51
  ~ Modified date: 2024/4/9 上午9:51
  --%>

<%--
  Created by IntelliJ IDEA.
  User: TangXJ
  Date: 2024/4/8
  Time: 16:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="static/bootstrap-3.4.1-dist/css/bootstrap.css">
</head>
<body>

    <%
        List<Banji> banjiList = (List<Banji>) request.getAttribute("banjiList");
    %>
    <table class="table table-striped table-bordered table-hover table-condensed">
        <tr>
            <td>ID</td>
            <td>班级</td>
            <td>编辑</td>
            <td>删除</td>
        </tr>
        <%
            for (Banji banji : banjiList) {
        %>
        <tr>
            <td><%=banji.getId()%></td>
            <td><%=banji.getName()%></td>
            <td><a href="/banji?method=toUpdate&id=<%=banji.getId()%>">编辑</a></td>
            <td><a href="javascript:void(0)" onclick="deleteById(<%=banji.getId()%>)">删除</a></td>
        </tr>
        <%
            }
        %>
    </table>
    <script>
        function deleteById(id) {
            var isDelete = confirm('你确定要删除这个班级吗？');
            if (isDelete) {
                console.log('执行删除');
            }
        }
    </script>
</body>
</html>
