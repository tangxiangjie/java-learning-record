/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/13 下午2:05
 * Modified date: 2024/6/13 下午2:05
 */

package com.sdust.springboot.aspect;

import com.sdust.springboot.service.impl.UserServiceImpl;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

//日志打印切面类(切面=通知+切入点)
@Aspect
@Component //new 对象放到容器中  spring要使用  (控制反转？)
/*
  <!-- 定义切面bean -->
  <bean id="loggingAspect" class="com.sdust.spring.aop.LoggingAspect" />
*/
public class ServiceLogAspect {
    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    //重用切入点表达式
    @Pointcut("execution(* com.sdust.springboot.service.impl.*.*(..))")
    public void userServicePointcut() {}


    //@Around("execution(* com.sdust.springboot.service.impl.*.*(..))")
    @Around("userServicePointcut()")
    public Object timeLogRecord(ProceedingJoinPoint joinPoint) throws Throwable {
        logger.info("=============开始执行 {}.{}=============",
                joinPoint.getTarget().getClass(), joinPoint.getSignature().getName());

        //记录开始时间
        long begin = System.currentTimeMillis();

        //执行切入点代码
        Object result = joinPoint.proceed();

        /*try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }*/
        long end = System.currentTimeMillis();
        long timeCost = end - begin;
        if (timeCost >= 3000) {
            logger.error("========执行结束，耗时{}毫秒========", timeCost);
        } else if (timeCost >= 2000) {
            logger.warn("========执行结束，耗时{}毫秒========", timeCost);
        } else {
            logger.info("========执行结束，耗时{}毫秒========", timeCost);
        }

        return result;
    }

}
