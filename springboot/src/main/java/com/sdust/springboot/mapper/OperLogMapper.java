/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/14 上午10:51
 * Modified date: 2024/6/14 上午10:51
 */

package com.sdust.springboot.mapper;

import com.sdust.springboot.pojo.entity.OperLog;

public interface OperLogMapper {
    void add(OperLog operLog);
}
