<%@ page import="com.sdust.web.pojo.vo.StudentBanjiVO" %>
<%@ page import="com.sdust.web.util.PageInfo" %>
<%@ page import="com.sdust.web.pojo.entity.Banji" %><%--
  ~ 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
  ~ Copyright @TangXJ
  ~ Created by TangXJ
  ~ Created&Used date: 2024/4/9 上午9:51 ~ 2024/4/9 上午10:08
  ~ Modified date: 2024/4/9 上午10:08
  --%>

<%--
  Created by IntelliJ IDEA.
  User: TangXJ
  Date: 2024/4/9
  Time: 9:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="static/bootstrap-3.4.1-dist/css/bootstrap.css">
</head>
<body>
    <a class="btn btn-primary" href="banji_add.jsp">添加</a>

    <table class="table table-striped table-bordered table-hover table-condensed">
        <tr>
            <td>ID</td>
            <td>名称</td>
            <td colspan="2">操作</td>
        </tr>
        <%
            PageInfo<Banji> pageInfo = (PageInfo<Banji>) request.getAttribute("banjiPageInfo");
            for (Banji banji : pageInfo.getList()) {
        %>
        <tr>
            <td><%=banji.getId()%></td>
            <td><%=banji.getName()%></td>
            <td><a href="/banji?method=toUpdate&id=<%=banji.getId()%>">编辑</a></td>
            <td><a href="#" onclick="">删除</a></td>
        </tr>
        <%
            }
        %>
    </table>

    <nav aria-label="Page navigation">
        <ul class="pagination">
            <li>
                <a href="javascript:void(0)" aria-label="Previous" onclick="previous(<%=pageInfo.getPageNo()%>)">
                    <span aria-hidden="true">&laquo;</span>
                </a>
            </li>
            <%
                for (int i = 1; i <= pageInfo.getTotalPage(); i++) {
            %>
            <li><a href="/student?method=selectByPage&pageNo=<%=i%>"><%=i%></a></li>
            <%
                }
            %>
            <li>
                <a href="javascript:void(0)" aria-label="Next" onclick="next(<%=pageInfo.getPageNo()%>, <%=pageInfo.getTotalPage()%>)">
                    <span aria-hidden="true">&raquo;</span>
                </a>
            </li>
        </ul>
    </nav>

    <script>
        function previous(pageNo) {
            if (pageNo > 1) {
                location.href = '/banji?method=selectByPage&pageNo=' + (pageNo - 1);
            } else {
                alert('已经是第一页了！');
            }
        }
        function next(pageNo, totalPage) {
            if (pageNo < totalPage) {
                location.href = '/banji?method=selectByPage&pageNo=' + (pageNo + 1);
            } else {
                alert('已经是最后一页了！');
            }
        }
    </script>
</body>
</html>
