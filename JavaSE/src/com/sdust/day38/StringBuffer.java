/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/5/30 下午4:12
 * Modified date: 2024/5/30 下午4:12
 */

package com.sdust.day38;

import org.junit.Test;

import java.util.Arrays;

public class StringBuffer {

    @Test
    public void test1() {
        String[] ids = {"1", "2", "3"};
        StringBuilder sql = new StringBuilder("delete from banji where id in (");
        //StringBuilder线程不安全 效率高 StringBuffer 线程安全 效率低
        for (int i = 0; i < ids.length; i++) {
            //sqll += (i == ids.length - 1) ? "?)" : "?,";
            sql.append((i == ids.length - 1) ? "?)" : "?,");
        }
        System.out.println(sql);
    }

    @Test
    public void test2() {
        String[] ids = {"1", "2", "3"};
        String[] questionMarks = new String[ids.length];
        Arrays.fill(questionMarks, "?");

        String sql = "delete from banji where id in (";
        String str = String.join(",", questionMarks);
        sql = sql + str + ")";
        System.out.println(str);
        System.out.println(sql);
    }

    public String buildDeleteSql(String[] ids) {
        String[] questionMarks = new String[ids.length];
        Arrays.fill(questionMarks, "?");
        return "delete from banji where id in (" + String.join(",", questionMarks) + ")";
    }

    @Test
    public void testOptimized() {
        String[] ids = {"1", "2", "3"};
        String sql = buildDeleteSql(ids);
        System.out.println(sql);
    }

    @Test
    public void test3() {
        String[] ids = {"1", "2", "3"};
        String[] questionMarks = new String[ids.length];
        Arrays.fill(questionMarks, "?");

        StringBuilder sql = new StringBuilder("delete from banji where id in (");
        sql.append(String.join(",", questionMarks)).append(")");

        System.out.println(sql.toString());
    }
}
