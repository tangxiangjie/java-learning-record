/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/7 下午2:19
 * Modified date: 2024/6/7 下午2:19
 */

package com.sdust.ssm.mapper;

import com.sdust.ssm.pojo.Student;

import java.util.List;

public interface StudentMapper {
    List<Student> selectAll();

    void deleteById(Integer id);

    Student selectById(Integer id);
}
