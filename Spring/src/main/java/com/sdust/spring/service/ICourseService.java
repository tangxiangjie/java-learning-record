/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/5 下午3:20
 * Modified date: 2024/6/5 下午3:20
 */

package com.sdust.spring.service;

public interface ICourseService {
    void selectAll();
}
