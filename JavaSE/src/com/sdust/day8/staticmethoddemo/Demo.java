/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/3/21 上午11:32 ~ 2024/3/21 下午2:24
 * Modified date: 2024/3/21 下午2:24
 */

package com.sdust.day8.staticmethoddemo;

import org.junit.Test;

import java.util.Arrays;

public class Demo {

    @Test
    public void test1() {
        Student student = new Student();
        student.show();
        student.id = 10;

        //
        //不推荐对象调用类的静态属性
        //Static member 'com.sdust.day8.teacherdemo.Student.country' accessed via instance reference
        System.out.println(student.country);//业主形式使用公共设施
        System.out.println(Student.country);//小区拥有公共设施

        //不推荐对象调用类的静态方法
        student.staticShow();//业主形式使用公共设施
        Student.staticShow();//小区拥有公共设施
    }

    @Test
    public void test2() {
        int max1 = Math.max(3, 2);
        System.out.println(max1);
        double max2 = Math.max(3.2, 5.8);
        System.out.println(max2);

        //工具类
        //Math数学操作工具类
        //'Math()' has private access in 'java.lang.Math'
        //Math math = new Math();
        //java: Math() 在 java.lang.Math 中是 private 访问控制

        //Arrays数组操作工具类 ArrayUtil
        int[]array = {34, 47, 44};
        System.out.println(array);//[I@2a18f23c
        for (int i : array) {
            System.out.println(i);
        }
        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i]);
        }
        System.out.println(Arrays.toString(array));//常用
        Arrays.sort(array);
        System.out.println(Arrays.toString(array));
        int res = Arrays.binarySearch(array, 422);
        System.out.println(res);

        //
    }
}
