/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/7 上午1:15
 * Modified date: 2024/6/7 上午1:15
 */

package com.sdust.spring.proxy.staticc.demo2;

public class StaticProxyDemo {
    public static void main(String[] args) {
        RealBankAccount realAccount = new RealBankAccount(1000.0);
        BankAccount proxy = new BankAccountProxy(realAccount);

        proxy.withdraw(10000.0);
    }
}