/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/6 下午11:36 ~ 2024/6/7 上午1:10
 * Modified date: 2024/6/6 下午11:37
 */

package com.sdust.spring.proxy.staticc;

public interface IQianzi {
    void qianzi();
}
