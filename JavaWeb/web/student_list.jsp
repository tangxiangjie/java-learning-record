<%@ page import="com.sdust.web.pojo.entity.Student" %>
<%@ page import="java.util.List" %>
<%@ page import="com.sdust.web.pojo.vo.StudentBanjiVO" %><%--
  ~ 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
  ~ Copyright @TangXJ
  ~ Created by TangXJ
  ~ Created&Used date: 2024/4/2 上午11:18 ~ 2024/4/9 下午10:42
  ~ Modified date: 2024/4/9 下午10:42
  --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="static/bootstrap-3.4.1-dist/css/bootstrap.css">
</head>
<body>
        <%--${list}--%>
        <%
            //JSP脚本，这里面可以任意写java代码
            //request、response:JSP内置对象
            List<StudentBanjiVO> studentList = (List<StudentBanjiVO>) request.getAttribute("studentList");
            System.out.println(studentList);
        %>
        <a class="btn btn-primary" href="http://localhost:8080/student?method=toAddStudent">添加</a>
        <table class="table table-striped table-bordered table-hover table-condensed">
            <tr>
                <td>ID</td>
                <td>名字</td>
                <td>年龄</td>
                <td>性别</td>
                <td hidden="hidden">班级ID</td>
                <td>班级</td>
                <td>编辑</td>
                <td>删除</td>
            </tr>
            <%
                for (StudentBanjiVO student : studentList) {
            %>
                    <tr>
                        <td><%=student.getId()%></td>
                        <td><%=student.getName()%></td>
                        <td><%=student.getAge()%></td>
                        <td><%=student.getGender()%></td>
                        <td hidden="hidden"><%=student.getBanjiId()%></td>
                        <td><%=student.getBanjiName()%></td>
                        <%--<td><a href="javascript:void(0)" onclick="update(<%=student.getId()%>)">带确认编辑</a></td>--%>
                        <td><a href="/student?method=updateQuery&id=<%=student.getId()%>">编辑</a></td>
                        <%--<td><a href="/deleteStudent?id=<%=student.getId()%>">不带确认删除deletestudent</a></td>--%>
                        <%--<td><a href="/student?method=deleteById&id=<%=student.getId()%>">不带确认删除studentservlet</a></td>--%>
                        <td><a href="javascript:void(0)" onclick="deleteById(<%=student.getId()%>)">删除</a></td>
                    </tr>
            <%
                }
            %>
        </table>
        <script>
            function deleteById(id) {
                var isDelete = confirm('您确认要删除吗？')
                if (isDelete) {
                    location.href = '/student?method=deleteById&id=' + id;
                }
            }
            /*function update(id) {
                var isUpdate = confirm('您确认要更新吗？')
                if (isUpdate) {
                    location.href = '/student?method=updateQuery&id=' + id;
                }
            }*/
        </script>
</body>
</html>
