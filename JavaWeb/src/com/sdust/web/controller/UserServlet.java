/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/4/7 下午3:56 ~ 2024/4/24 上午9:18
 * Modified date: 2024/4/24 上午9:18
 */

package com.sdust.web.controller;

import com.sdust.web.pojo.entity.User;
import com.sdust.web.service.IUserService;
import com.sdust.web.service.impl.UserServiceImpl;
import com.sdust.web.util.JSONUtil;
import com.sdust.web.util.MD5Util;
import com.sdust.web.util.PageInfo;
import com.sdust.web.util.Result;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet("/user")
public class UserServlet extends HttpServlet {
    private IUserService userService = new UserServiceImpl();

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        // /user?method=selectByPage
        String method = req.getParameter("method");
        switch (method) {
            case "selectByPage":
                selectByPage(req, resp);
                break;
            case "login":
                login(req, resp);
                break;
            case "checkUser":
                checkUser(req, resp);
                break;
            case "register"://后端管理其实不需要注册
                register(req, resp);
                break;
            case "logout":
                logout(req, resp);
                break;
        }
    }

    private void checkUser(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        System.out.println("UserServlet.checkUser");
        String name = req.getParameter("username");
        Map<String, Object> map = new HashMap<>();
        if(name == null || "".equals(name)) {
            JSONUtil.toJSON(resp, Result.error("用户名不能为空"));
            return;
        }

        String username = MD5Util.MD5Encode(name);
        boolean isUsed = userService.checkUser(username);

        if (isUsed) {
            JSONUtil.toJSON(resp, Result.error("用户名已存在，请更换"));
        } else {
            JSONUtil.toJSON(resp, Result.ok("用户名可用"));
        }
    }

    private void register(HttpServletRequest req, HttpServletResponse resp) {
        System.out.println("UserServlet.register");
        String username = MD5Util.MD5Encode(req.getParameter("username"));
        String password = MD5Util.MD5Encode(req.getParameter("password"));
        User user = new User(null, username, password);

        userService.register(user);

        JSONUtil.toJSON(resp, Result.ok("注册成功，即将跳转到登录界面"));
    }

    private void logout(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        System.out.println("UserServlet.logout");
        HttpSession session = req.getSession();
        session.removeAttribute("user");
        resp.sendRedirect("/login_layui.jsp");
    }

    private void login(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        System.out.println("UserServlet.login");
        String name = MD5Util.MD5Encode(req.getParameter("username"));
        String password = MD5Util.MD5Encode(req.getParameter("password"));
        String code = req.getParameter("captcha");
        //先判断验证码是不是正确
        //1 验证码错误，返回一个验证码错误的提示
        //2 验证码正确，再验证用户名密码是否正确
        HttpSession session1 = req.getSession();
        Object codeInSession = session1.getAttribute("codeInSession");
        if (!codeInSession.equals(code)) {
            JSONUtil.toJSON(resp, Result.error("验证码错误"));
            return;
        }

        User user = userService.login(name, password);
        if (user == null) {
            JSONUtil.toJSON(resp, Result.error("用户名或者密码错误"));
        } else {
            HttpSession session = req.getSession();
            session.setAttribute("user", user);
            JSONUtil.toJSON(resp, Result.ok("登录成功"));
        }

        /*if (user == null) {
            resp.sendRedirect("/fail.jsp");
        } else {
            HttpSession session = req.getSession();
            session.setAttribute("user", user);//凭证
            resp.sendRedirect("/");
        }*/
    }

    private void selectByPage(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("UserServlet.selectByPage");
        // /user?method=selectByPage&pageNo=1&pageSize=5
        String pageNo = req.getParameter("pageNo");
        String pageSize = req.getParameter("pageSize");
        if (pageNo == null || pageNo.equals("")) {
            pageNo = "1";
        }
        if (pageSize == null || pageSize.equals("")) {
            pageSize = "5";
        }

        PageInfo pageInfo = userService.selectByPage(Integer.parseInt(pageNo), Integer.parseInt(pageSize));
        System.out.println(pageInfo);

        req.setAttribute("pageInfo", pageInfo);
        req.getRequestDispatcher("user_list2.jsp").forward(req, resp);
    }
}
