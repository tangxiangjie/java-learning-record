/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/3/27 下午3:23 ~ 2024/4/1 下午11:01
 * Modified date: 2024/4/1 下午11:01
 */

package com.sdust.jdbcuni;

import com.sdust.day12.JDBCUtil;
import com.sdust.day12.Student;
import org.junit.Test;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class JDBCDemo {

    @Test
    public void test1() {
        String update = "update student2 set age=age+1";
        JDBCUtil2.updateSQL(update);
    }

}
