<%--
  ~ 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
  ~ Copyright @TangXJ
  ~ Created by TangXJ
  ~ Created&Used date: 2024/4/15 下午2:17 ~ 2024/4/17 下午8:09
  ~ Modified date: 2024/4/17 下午8:09
  --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <script src="static/jquery-2.1.4.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" type="text/css" href="static/layui/css/layui.css"/>
    <script src="static/layui/layui.js" type="text/javascript" charset="utf-8"></script>
    <script src="static/mylayer.js" type="text/javascript" charset="utf-8"></script>
</head>
<body>
    <%--顶部搜索栏 开始--%>
    <br>
    <form class="layui-form layui-row layui-col-space16">
        <div class="layui-col-md2 layui-col-md-offset2">
            <div class="layui-input-wrap">
                <input type="text" name="name" value="" placeholder="班级名称" class="layui-input" lay-affix="clear">
            </div>
        </div>
        <div class="layui-col-md2">
            <div class="layui-input-wrap">
                <input type="text" name="address" placeholder="班级位置" class="layui-input" lay-affix="clear">
            </div>
        </div>
        <div class="layui-col-md2">
            <div class="layui-input-wrap">
                <input type="text" name="date" placeholder="入学时间" class="layui-input" lay-affix="clear">
            </div>
        </div>
        <div class="layui-col-md2">
            <button class="layui-btn" lay-submit lay-filter="submitSearch">搜索</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </form><br>
    <%--顶部搜索栏 结束--%>

    <%--班级信息显示 开始--%>
    <table class="layui-hide" id="test" lay-filter="test"></table>
    <%--班级信息显示 结束--%>

    <%--toolbarDemo 下边会用到 顶部工具栏 开始  是显示信息的一部分 layui就规定这么写--%>
    <script type="text/html" id="toolbarDemo">
        <div class="layui-btn-container">
            <button class="layui-btn layui-btn-sm" lay-event="add">添加</button>
            <button class="layui-btn layui-btn-sm layui-btn-danger" lay-event="deleteSelected">批量删除</button>
        </div>
    </script>
    <%--toolbarDemo 下边会用到 顶部工具栏 结束--%>

    <%--单元格工具栏 开始 是显示信息的一部分 layui就规定这么写--%>
    <script type="text/html" id="barDemo">
        <div class="layui-clear-space">
            <a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="delete">删除</a>
            <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
        </div>
    </script>
    <%--单元格工具栏 结束--%>

    <%-----------------------主体部分到此结束，下面都是按下“按钮”才会显示出来的--------------------------%>

    <%--添加form表单  开始--%>
    <div id="addForm" style="display: none;">
        <form class="layui-form" action="">
            <div class="layui-form-item">
                <label class="layui-form-label">班级名</label>
                <div class="layui-input-block">
                    <input type="text" name="name" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">地址</label>
                <div class="layui-input-block">
                    <input type="text" name="address" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">时间</label>
                <div class="layui-input-block">
                    <input type="text" name="date" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button type="submit" class="layui-btn" lay-submit lay-filter="submitAdd">立即提交</button>
                    <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                </div>
            </div>
        </form>
    </div>
    <%--添加form表单  结束--%>

    <%--修改form表单  开始--%>
    <div id="updateForm" style="display: none;">
        <form class="layui-form" lay-filter="updateFormFilter" action="">
            <div class="layui-form-item" hidden="hidden">
                <label class="layui-form-label">ID</label>
                <div class="layui-input-block">
                    <input type="text" name="id" lay-verify="required" autocomplete="off" class="layui-input" readonly>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">班级名</label>
                <div class="layui-input-block">
                    <input type="text" name="name" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">地址</label>
                <div class="layui-input-block">
                    <input type="text" name="address" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">时间</label>
                <div class="layui-input-block">
                    <input type="text" name="date" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button type="submit" class="layui-btn" lay-submit lay-filter="submitUpdate">修改</button>
                    <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                </div>
            </div>
        </form>
    </div>
    <%--修改form表单  结束--%>

    <script>
        //layui
        layui.use(['form','table'], function(){
            var table = layui.table;
            var form = layui.form;
            var laydate = layui.laydate;
            // 创建渲染实例
            // 创建表格实例
            table.render({
                elem: '#test',
                id: 'tableId',
                url:'/banji?method=selectByPage2',
                toolbar: '#toolbarDemo',
                cols: [[
                    {type: 'checkbox', fixed: 'left'},
                    {field:'id', title: 'ID', sort: true},
                    {field:'name', title: '班级名'},
                    {field:'address', title: '地址'},
                    {field:'date', title: '入学时间', sort: true},
                    {fixed: 'right', title:'操作', width: 134, minWidth: 125, toolbar: '#barDemo'}
                ]],
                page: true
            });

            // 搜索提交
            form.on('submit(submitSearch)', function(data){
                var field = data.field; // 获得表单字段JSON格式
                // 执行搜索重载
                table.reload('tableId', {
                    page: {
                        curr: 1 // 重新从第 1 页开始
                    },
                    where: field // 搜索的字段
                    //http://localhost:8080/banji?method=selectByPage2&page=1&limit=10&name=Java&address=asad&date=2403
                });
                return false; // 阻止默认 form 跳转
            });

            // 工具栏事件(顶部)
            table.on('toolbar(test)', function(obj){
                var id = obj.config.id;
                var checkStatus = table.checkStatus(id);
                var othis = lay(this);
                switch(obj.event){
                    case 'add':
                        var index = layer.open({
                            type: 1,
                            title: '添加数据',
                            area: ['500px', '300px'],//宽高
                            // content: '<div style="padding: 11px;">任意 HTML 内容</div>'
                            content: $('#addForm').html()//放一段页面
                        });

                        // 提交事件
                        form.on('submit(submitAdd)', function(data){
                            var field = data.field; // 获取表单字段值
                            console.log(data);
                            // 此处可执行 Ajax 等操作
                            $.post(
                                '/banji?method=add',
                                field,
                                function (result) {
                                    console.log(result);
                                    if (result.code == 0) {
                                        mylayer.okMsg(result.msg);
                                        //添加成功之后刷新表格，展示最新数据
                                        layer.close(index);
                                        table.reload('tableId');
                                    } else {
                                        mylayer.errorMsg(result.msg);
                                    }
                                },
                                'json'
                            );

                            return false; // 阻止默认 form 跳转
                        });
                        break;
                    case 'deleteSelected':
                        var data = checkStatus.data;
                        //[{"id":2,"name":"Java1812","address":"ddf","time":"232"},{"id":3,"name":"Java1903","address":"34","time":"323"}]
                        var ids = new Array();
                        $(data).each(function () {
                            ids.push(this.id);
                        })
                        //[2,4]
                        layer.confirm(
                            '您确认要删除么?',
                            {icon : 3},
                            function(index){
                                $.post(
                                    '/banji?method=deleteSelected',
                                    {'ids': ids},
                                    function (result) {
                                        console.log(result);
                                        if (result.code == 0) {
                                            mylayer.okMsg(result.msg);
                                            //删除成功之后刷新表格，展示最新数据
                                            table.reload('tableId');
                                        } else {
                                            mylayer.errorMsg(result.msg);
                                        }
                                    },
                                    'json'
                                );
                            }
                        );
                        break;
                    case 'LAYTABLE_TIPS':
                        layer.alert('自定义工具栏图标按钮');
                        break;
                };
            });

            // 触发单元格工具事件(右侧)
            table.on('tool(test)', function(obj) { // 双击 toolDouble
                var data = obj.data; // 获得当前行数据
                console.log(obj);
                //{"id": 1,"name": "Java1807","address": "dfdf","time": "3434"}
                console.log(obj.data);
                if (obj.event === 'delete') {
                    layer.confirm(
                        '您确认要删除么?',
                        {icon: 3},
                        function (index) {
                            $.post(
                                '/banji?method=deleteById',
                                {'id': data.id},
                                function (result) {//callback 回调函数
                                    console.log(result);
                                    if (result.code == 0) {
                                        mylayer.okMsg(result.msg);
                                        //删除成功之后刷新表格，展示最新数据
                                        table.reload('tableId');
                                    } else {
                                        mylayer.errorMsg(result.msg);
                                    }
                                },
                                'json'
                            );
                        }
                    )
                } else if(obj.event === 'edit') {
                    var index = layer.open({
                        title: '编辑 - id:' + data.id,
                        type: 1,
                        area: ['500px', '300px'],
                        content: $('#updateForm').html(),
                        success: function () {
                            //form.val有两个参数，第一个是form表单的lay-filter名字，
                            //第二个参数是json对象：{"id": 1,"name": "Java1807","address": "dfdf","time": "3434"}
                            //将第二个参数的值按照“名字”映射到form表单中
                            //form.val('updateFormFilter', data);//这里回显的是页面数据 不是数据库数据
                            $.post(
                                '/banji?method=selectById',
                                {'id': data.id},
                                function (result) {
                                    if (result.code == 0) {
                                        console.log(result.data);
                                        form.val('updateFormFilter', result.data);
                                    }
                                },
                                'json'
                            );
                        }
                    });

                    // 绑定提交事件
                    form.on('submit(submitUpdate)', function(data){
                        var field = data.field; // 获取表单字段值
                        console.log(data);
                        // 此处可执行 Ajax 等操作
                        $.post(
                            '/banji?method=updateForm',
                            field,  //{name: 'zhyangsan', address: 'fdfd', time: 'fd'}
                            function(result) {
                                console.log(result);
                                if (result.code == 0) {
                                    mylayer.okMsg(result.msg);
                                    layer.close(index);
                                    table.reload('tableId');
                                } else {
                                    mylayer.errorMsg(result.msg);
                                }
                            },
                            'json'
                        );
                        return false; // 阻止默认 form 跳转
                    });
                }
            });

            // 日期
            laydate.render({
                elem: '.demo-table-search-date'
            });
        });
    </script>
</body>
</html>
