/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/11 上午11:18
 * Modified date: 2024/6/11 上午11:18
 */

package com.sdust.spring.decorator.impl;

import com.sdust.spring.decorator.Shape;

//Shape的抽象实现类: 装饰器, 在子类中具体扩展功能,对Shape来说 无需知道装饰器的存在
//装饰器是抽象类，无法实例化，也即在draw前后添加额外功能行不通,再在抽象类的子类(继承)中实现"装饰"功能
//抽象类实现接口
//这个抽象实际上不是必要的，因为也可以在draw前后添加额外功能，但是如果"装饰"功能比较多，还是抽象  再子类
public abstract class ShapeDecorator implements Shape {
    protected Shape decoratedShape;

    public ShapeDecorator(Shape decoratedShape) {//有参构造方法
        this.decoratedShape = decoratedShape;
    }

    @Override
    public void draw() {
        decoratedShape.draw();
    }
}
