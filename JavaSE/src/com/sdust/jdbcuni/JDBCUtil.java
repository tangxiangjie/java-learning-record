/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/4/1 下午10:43
 * Modified date: 2024/4/1 下午10:43
 */

package com.sdust.jdbcuni;

import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.Properties;

public class JDBCUtil {
    private static String driver;
    private static String user;
    private static String password;
    private static String url;
    // 私有构造方法
    private JDBCUtil(){}

    // 静态代码块
    static {

        try {

            ClassLoader classLoader = JDBCUtil.class.getClassLoader();
            InputStream inputStream = classLoader.getResourceAsStream("db.properties");

            Properties properties = new Properties();
            properties.load(inputStream);

            driver = properties.getProperty("driver");
            url = properties.getProperty("url");
            user = properties.getProperty("username");
            password = properties.getProperty("password");

        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        try {

            Class.forName(driver);

        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

    }


    public static Connection getConnection(){

        try {

            return DriverManager.getConnection(url,user,password);

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    public static void close(Connection connection, Statement statement, ResultSet resultSet){

        if(connection != null){
            try {

                connection.close();

            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }

        if(statement != null){
            try {

                statement.close();

            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }

        if(resultSet != null){
            try {

                resultSet.close();

            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }


}

