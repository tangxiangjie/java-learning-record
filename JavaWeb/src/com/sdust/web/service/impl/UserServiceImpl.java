/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/4/7 下午4:04 ~ 2024/4/17 下午5:18
 * Modified date: 2024/4/17 下午5:18
 */

package com.sdust.web.service.impl;

import com.sdust.web.dao.IUserDao;
import com.sdust.web.dao.impl.UserDaoImpl;
import com.sdust.web.pojo.entity.User;
import com.sdust.web.service.IUserService;
import com.sdust.web.util.PageInfo;

import java.util.List;

public class UserServiceImpl implements IUserService {
    private IUserDao userDao = new UserDaoImpl();

    @Override
    public PageInfo selectByPage(Integer pageNo, Integer pageSize) {
        //Service业务层为了完成PageInfo的封装，需要调用两次Dao操作
        //1.查找当前页数据
        int offset = (pageNo - 1) * pageSize;
        List<User> list = userDao.selectByPage(offset, pageSize);
        //2.总的页数  查总的数量 Math.ceil(总数量/pageSize)
        int totalCount = userDao.selectTotalCount();
        int totalPage = (int)Math.ceil((double)totalCount / pageSize);

        PageInfo<User> pageInfo  = new PageInfo<User>(list, totalPage, pageNo, pageSize);
        return pageInfo;
    }

    @Override
    public User login(String name, String password) {
        return userDao.login(name, password);
    }

    @Override
    public void register(User user) {
        userDao.addUser(user);
    }

    @Override
    public boolean checkUser(String username) {
        return userDao.slecletByName(username);
    }
}
