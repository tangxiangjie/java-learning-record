/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/4/7 上午11:33 ~ 2024/4/9 下午12:00
 * Modified date: 2024/4/9 下午12:00
 */

package com.sdust.web.dao;

import com.sdust.web.pojo.entity.Teacher;

import java.util.List;

public interface ITeacherDao {
    public abstract List<Teacher> selectAll();
    void deleteById(Integer id);
    void add(Teacher teacher);//没有id
    Teacher selectById(Integer id);
    void update(Teacher teacher);//有id
    List<Teacher> selectByPage(int offset, Integer pageSize);
    int selectTotalCounnt();
}
