/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/6 上午12:39
 * Modified date: 2024/6/6 上午12:39
 */

package com.sdust.spring.service.impl;

import com.sdust.spring.dao.IBanjiDao;
import com.sdust.spring.dao.impl.BanjiDaoImpl;
import com.sdust.spring.service.IBanjiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
//默认名字是banjiServiceImpl,而不是banjiService
public class BanjiServiceImpl implements IBanjiService {
    @Autowired
    private IBanjiDao banjiDao;

    @Override
    public void selectAll() {
        System.out.println("BanjiServiceImpl.selectAll");
        banjiDao.selectAll();
    }
}
