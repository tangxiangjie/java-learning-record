/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/5 上午12:53
 * Modified date: 2024/6/5 上午12:53
 */

package com.sdust.spring.service.impl;

import com.sdust.spring.dao.IStudentDao;
import com.sdust.spring.dao.impl.StudentDaoImpl;
import com.sdust.spring.service.IStudentService;

public class StudentServiceImpl implements IStudentService {
    // private IStudentDao studentDao = new StudentDaoImpl();
    private IStudentDao studentDao;

    @Override
    public void selectAll() {
        System.out.println("StudentServiceImpl.selectAll");
        //studentDao.selectAll();//这个依赖由框架(IoC容器)来管理，没有new 动态代理不知道dao对象的存在及如何获取它 故注释掉
    }

    public void setStudentDao(IStudentDao studentDao) {
        this.studentDao = studentDao;
    }
}
