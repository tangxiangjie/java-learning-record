/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/3/21 上午10:40 ~ 2024/3/21 上午11:07
 * Modified date: 2024/3/21 上午11:07
 */

package com.sdust.day8.interfacedemo;

public abstract class AbstractBird {

    public abstract void egg();

}
