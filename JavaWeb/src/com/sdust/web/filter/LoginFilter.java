/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/4/9 下午4:21 ~ 2024/4/18 上午9:27
 * Modified date: 2024/4/18 上午9:27
 */

package com.sdust.web.filter;

import com.sdust.web.pojo.entity.User;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

// /*   拦截所有的请求
//@WebFilter(filterName = "login", urlPatterns = "/*")
public class LoginFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        Filter.super.init(filterConfig);
        System.out.println("LoginFilter.init");
    }

    // ServletRequest servletRequest = new HttpServletRequest();
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        System.out.println("LoginFilter.doFilter");
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        String servletPath = request.getServletPath();
        System.out.println("servletPath: " + servletPath);
        String method = request.getParameter("method");//在这里执行了getParameter 后边的处理编码的就不管用了
        //这些请求是要去完成登录的，不需要执行后边验证是否登录的流程
        if (servletPath.startsWith("/static")//登录界面的.icon文件等文件识别不出来
                || servletPath.equals("/login.jsp")
                || servletPath.equals("/login_layui.jsp")
                || servletPath.equals("/user_add.jsp")
                || servletPath.equals("/fail.jsp")
                || servletPath.equals("/verifyCode")
                || servletPath.equals("/auth")
                || servletPath.equals("/user") && "login".equals(method)
                || servletPath.equals("/user") && "register".equals(method)
                || servletPath.equals("/user") && "checkUser".equals(method)) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }

        HttpSession session = request.getSession();//每个都要这个操作 ----->  就有了过滤器
        User user = (User) session.getAttribute("user");
        if (user == null) {//为什么这样就可以判断是不是登录了-->因为登录验证里匹配的话，会new一个User出来
            //response.sendRedirect("/login.jsp");
            response.sendRedirect("/login_layui.jsp");
            return;
        }

        //加了这句话代表放行，继续往后执行，如果还有filter就访问后面的filter，没有filter就可以访问后台资源。
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
        Filter.super.destroy();
        System.out.println("LoginFilter.destroy");
    }
}
