<%@ page import="com.sdust.web.pojo.entity.Student" %>
<%@ page import="java.util.List" %>
<%@ page import="com.sdust.web.pojo.entity.Teacher" %><%--
  ~ 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
  ~ Copyright @TangXJ
  ~ Created by TangXJ
  ~ Created&Used date: 2024/4/2 上午11:18 ~ 2024/4/8 下午3:12
  ~ Modified date: 2024/4/8 下午3:12
  --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="static/bootstrap-3.4.1-dist/css/bootstrap.css">
</head>
<body>
        <%--${list}--%>
        <%
            //JSP脚本，这里面可以任意写java代码
            //request、response:JSP内置对象
            List<Teacher> teacherList = (List<Teacher>) request.getAttribute("teacherList");
            System.out.println(teacherList);
        %>
        <a class="btn btn-primary" href="teacher_add.jsp">添加</a>
        <table class="table table-striped table-bordered table-hover table-condensed">
            <tr>
                <td>ID</td>
                <td>姓名</td>
                <td>年龄</td>
                <td>性别</td>
                <td>薪资</td>
                <td>编辑</td>
                <td>删除</td>
            </tr>
            <%
                for (Teacher teacher : teacherList) {
            %>
                    <tr>
                        <td><%=teacher.getId()%></td>
                        <td><%=teacher.getName()%></td>
                        <td><%=teacher.getAge()%></td>
                        <td><%=teacher.getGender()%></td>
                        <td><%=teacher.getSalary()%></td>
                        <%--<td>编辑</td>--%>
                        <td><a href="/teacher?method=toUpdate&id=<%=teacher.getId()%>">编辑</a></td>
                        <%--<td><a href="/deleteTeacher?id=<%=teacher.getId()%>">删除</a></td>--%>
                        <td><a href="javascript:void(0)" onclick="deleteById(<%=teacher.getId()%>)">删除</a></td>
                    </tr>
            <%
                }
            %>
        </table>
        <script>
            function deleteById(id) {
                var isDelete = confirm('您确定要删除吗？');
                if (isDelete) {
                    location.href = '/deleteTeacher?id=' + id;
                }
            }
        </script>
</body>
</html>
