package com.sdust.day2;

import org.junit.Test;

import java.util.Scanner;

public class ConditionalStatement {

    @Test
    public void test1() {
        boolean bool = false;
        if (bool) {
            System.out.println("If...");
        } else {
            System.out.println("else...");
        }
    }

    /*
    >=90   <=100      优秀
    >=80    <90       良好
    >=70    <80       一般
    >=60    <70      及格
    <60              不及格
    */
    @Test
    public void test2() {
        //int score = 78;
        //通过Scanner可以实现从控制台输入信息
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入成绩：");
        int score = scanner.nextInt();
        //卫语句1
        if (score < 0 || score > 0) {//异常和正常 要分开
            System.out.println("Invalid input!");
            //后面的代码不再执行
            return;
        }
        //卫语句2...

        //合法输入
        if (score >= 90 && score <= 100) {
            System.out.println("优秀");
        } else if (score >= 80 && score < 90) {
            System.out.println("良好");
        } else if (score >= 70 && score < 80) {
            System.out.println("一般");
        } else if (score >= 60 && score < 70) {
            System.out.println("及格");
        } else {
            System.out.println("不及格");
        }
    }

    //三目运算符
    @Test
    public void test6() {
        //用英语按照一定的规则把想表达的意思表达出来
        System.out.println("ConditionalStatement.test6");//调试的一种
        int num1 = 5;
        int num2 = 6;
        int max1 = 0;
        if (num1 >= num2) {
            max1 = num1;
        } else {
            max1 = num2;
        }
        int max2 = num1 >= num2 ? num1 : num2;
        System.out.println("The max number is " + max1);
        System.out.println("The max number is " + max2);
    }

    @Test
    public void test7(){
        int age = 25;
        //逗逗加加
        System.out.println("I am 22 years old.");
        System.out.println("I am " + age + " years old.");
        String str1 = "我的年龄是22岁";
        String str2 = "";//空字符串
        System.out.println(10 + 20 + "" + 30);//"3030"
        System.out.println("" + 10 + 20 + 30);//"102030"
    }
}
