/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/4/8 下午3:45 ~ 2024/4/17 上午10:33
 * Modified date: 2024/4/17 上午10:33
 */

package com.sdust.web.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sdust.web.pojo.entity.Banji;
import com.sdust.web.pojo.query.BanjiQuery;
import com.sdust.web.pojo.vo.BanjiCountVO;
import com.sdust.web.service.IBanjiService;
import com.sdust.web.service.impl.BanjiServiceImpl;
import com.sdust.web.util.JSONUtil;
import com.sdust.web.util.LayUITableResult;
import com.sdust.web.util.PageInfo;
import com.sdust.web.util.Result;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/banji")
public class BanjiServlet extends HttpServlet {
    private IBanjiService banjiService = new BanjiServiceImpl();

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        //http://localhost:8080/banji?method=selectAll
        String method = req.getParameter("method");
        if (method == null || method.equals("")) {
            method = "selectAll";
        }
        switch (method) {
            case "selectAll":
                selectAll(req, resp);
                break;
            case "selectByPage":
                selectByPage(req, resp);
                break;
            case "selectByPage2"://layui
                selectByPage2(req, resp);
                break;
            case "deleteById":
                deleteById(req, resp);
                break;
            case "deleteSelected":
                deleteSelected(req, resp);
                break;
            /*case "toAddStudent":
                toAddStudent(req, resp);
                break;*/
            case "add":
                add(req, resp);
                break;
            case "selectById":
                selectById(req, resp);
                break;
            case "updateForm":
                updateForm(req, resp);
                break;
            case "toUpdate":
                toUpdate(req, resp);
                break;
            case "selectBanjiCount":
                selectBanjiCount(req, resp);
                break;
        }
    }

    private void selectBanjiCount(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        System.out.println("BanjiServlet.selectBanjiCount");
        List<BanjiCountVO> list= banjiService.selectBanjiCount();

        resp.setContentType("text/html;charset=UTF-8");
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(resp.getWriter(), list);
    }

    private void add(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        System.out.println("BanjiServlet.add");

        String name = req.getParameter("name");
        String address = req.getParameter("address");
        String date = req.getParameter("date");
        Banji banji = new Banji(null, name, address, Integer.parseInt(date));

        banjiService.add(banji);

        //resp.sendRedirect("/banji?method=selectByPage");//以前没用layui的写法
        JSONUtil.toJSON(resp, Result.ok("添加成功"));

    }

    private void selectById(HttpServletRequest req, HttpServletResponse resp) {
        System.out.println("BanjiServlet.selectById");
        String id = req.getParameter("id");
        Banji banji = banjiService.selectById(Integer.parseInt(id));
        System.out.println(banji);
        //toJSON是一个封装
        //JSONUtil.toJSON(resp, Result.ok("查询成功"));//code=0传回去
        //JSONUtil.toJSON(resp, banji);//查到的数据传回去 result里边，no no no
        JSONUtil.toJSON(resp, Result.ok(banji));//重载的ok()
    }

    private void updateForm(HttpServletRequest req, HttpServletResponse resp) {
        System.out.println("BanjiServlet.updateForm");
        String id = req.getParameter("id");
        String name = req.getParameter("name");
        String address = req.getParameter("address");
        String date = req.getParameter("date");
        Banji banji = new Banji(Integer.parseInt(id), name, address, Integer.parseInt(date));
        banjiService.updateForm(banji);

        JSONUtil.toJSON(resp, Result.ok("修改成功"));
    }

    private void selectByPage(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("BanjiServlet.selectByPage");
        String pageNo = req.getParameter("pageNo");
        String pageSize = req.getParameter("pageSize");
        if (pageNo == null || pageNo.equals("")) {
            pageNo = "1";
        }
        if (pageSize == null || pageSize.equals("")) {
            pageSize = "5";
        }

        PageInfo<Banji> banjiPageInfo = banjiService.selectByPage(Integer.parseInt(pageNo), Integer.parseInt(pageSize));
        req.setAttribute("banjiPageInfo", banjiPageInfo);
        req.getRequestDispatcher("banji_list2.jsp").forward(req, resp);
    }

    // /banji?method=selectByPage2&page=1&limit=10
    // /banji?method=selectByPage2&page=1&limit=10&name=Java&address=asad&date=2403
    private void selectByPage2(HttpServletRequest req, HttpServletResponse resp) {
        System.out.println("BanjiServlet.selectByPage2");
        String pageNo = req.getParameter("page");
        String pageSize = req.getParameter("limit");
        String name = req.getParameter("name");
        String address = req.getParameter("address");
        String date = req.getParameter("date");//这里不填的话，会Cannot parse null string
        Integer parsedDate = (date != null && !"".equals(date)) ? Integer.parseInt(date) : null;//还不够 剩下的在Dao里边判断就行了
        BanjiQuery banjiQuery = new BanjiQuery(Integer.parseInt(pageNo), Integer.parseInt(pageSize), name, address, parsedDate);

        //LayUITableResult layUITableResult = banjiService.selectByPage2(Integer.parseInt(pageNo), Integer.parseInt(pageSize));
        LayUITableResult layUITableResult = banjiService.selectByPage3(banjiQuery);

        JSONUtil.toJSON(resp, layUITableResult);
    }

    private void deleteById(HttpServletRequest req, HttpServletResponse resp) {
        System.out.println("BanjiServlet.deleteById");
        String id = req.getParameter("id");
        banjiService.deleteById(Integer.parseInt(id));
//        Result result = new Result();
//        result.setCode(0);//这是个假的
//        result.setMsg("删除成功");
//        JSONUtil.toJSON(resp, result);
        JSONUtil.toJSON(resp, Result.ok("删除成功"));//应该根据service是否成功添加状态码 msg
    }

    private void deleteSelected(HttpServletRequest req, HttpServletResponse resp) {
        System.out.println("BanjiServlet.deleteSelected");
        String[] ids = req.getParameterValues("ids[]");
//        String[] ids = req.getParameterValues("ids");
        banjiService.deleteSelected(ids);
        JSONUtil.toJSON(resp, Result.ok("删除成功"));//应该根据service是否成功添加状态码 msg
    }

    private void toUpdate(HttpServletRequest req, HttpServletResponse resp) {
        System.out.println("BanjiServlet.toUpdate");
    }

    /*private void toAddStudent(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("BanjiServlet.selectAll");
        List<Banji> banjiList = banjiService.selectAll();
        req.setAttribute("banjiList", banjiList);
        req.getRequestDispatcher("student_add.jsp").forward(req, resp);
    }*/

    private void selectAll(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("BanjiServlet.selectAll");
        List<Banji> banjiList = banjiService.selectAll();
        req.setAttribute("banjiList", banjiList);
        req.getRequestDispatcher("banji_list.jsp").forward(req, resp);
    }
}