/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/5 上午12:52
 * Modified date: 2024/6/5 上午12:52
 */

package com.sdust.spring.controller;

import com.sdust.spring.service.IStudentService;
import com.sdust.spring.service.impl.StudentServiceImpl;

public class StudentController {
    // private IStudentService studentService = new StudentServiceImpl();
    private IStudentService studentService;

    public void selectAll() {
        System.out.println("StudentController.selectAll");
        studentService.selectAll();
    }

    public void setStudentService(IStudentService studentService) {
        this.studentService = studentService;
    }
}
