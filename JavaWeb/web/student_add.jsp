<%@ page import="java.util.List" %>
<%@ page import="com.sdust.web.pojo.entity.Banji" %><%--
  ~ 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
  ~ Copyright @TangXJ
  ~ Created by TangXJ
  ~ Created&Used date: 2024/4/3 下午2:21 ~ 2024/4/8 下午4:00
  ~ Modified date: 2024/4/8 下午4:00
  --%>

<%--
  Created by IntelliJ IDEA.
  User: TangXJ
  Date: 2024/4/3
  Time: 14:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
    <%--<form action="/student?method=add" method="get">--%>
    <%--默认就是get ?method=就是get方法传参  会被覆盖  所以form表单一般不用--%>
    <%
        List<Banji> banjiList = (List<Banji>) request.getAttribute("banjiList");
    %>
    <form action="/student?method=add" method="post"><%--默认就是get ?method=就是get方法传参  会被覆盖  所以form表单一般不用--%>
        姓名:<input type="text" name="name"><br>
        年龄:<input type="text" name="age"><br>
        性别:<input type="text" name="gender"><br>
        班级:<select name="banjiId">
            <%
                for (Banji banji : banjiList) {
            %>
            <option value="<%=banji.getId()%>"><%=banji.getName()%></option>
            <%
                }
            %>
        </select><br>
        <input type="submit" value="添加">
    </form>
</body>
</html>
