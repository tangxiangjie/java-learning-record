/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/5 上午12:04
 * Modified date: 2024/6/5 上午12:04
 */

package com.sdust;

import com.sdust.spring.controller.BanjiController;
import com.sdust.spring.controller.CourseController;
import com.sdust.spring.controller.StudentController;
import com.sdust.spring.pojo.Banji;
import com.sdust.spring.pojo.Student;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringDemo {

    @Test
    public void testIOC() {
        //这行代码创建Spring容器，执行后在xml中配置的bean都new出对象(被实例化) 会放到spring容器中
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        //从Spring容器中根据名字取出指定实例化的bean对象
        Student student = (Student) context.getBean("student");
        System.out.println(student);
    }

    @Test
    public void testSingleton() {
        //这行代码创建Spring容器，执行后在xml中配置的bean都new出对象(被实例化) 会放到spring容器中
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        //从Spring容器中根据名字取出指定实例化的bean对象
        System.out.println("↑↑↑↑↑↑单例模式在加载时实例化↑↑↑↑↑↑");
        Student student1 = (Student) context.getBean("student");
        Student student2 = (Student) context.getBean("student");
        System.out.println(student1 == student2);
    }

    @Test
    public void testPrototype() {
        //这行代码创建Spring容器，执行后在xml中配置的bean都new出对象(被实例化) 会放到spring容器中
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        //从Spring容器中根据名字取出指定实例化的bean对象
        System.out.println("↓↓↓↓↓↓多例模式在get时实例化↓↓↓↓↓↓");
        Student student1 = (Student) context.getBean("student");
        Student student2 = (Student) context.getBean("student");
        System.out.println(student1 == student2);
    }

    @Test
    //测试依赖注入 1 set方法 2 构造方法  详见applicationContext.xml
    public void testDIBySetterAndConstructor() {
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        Banji banji = (Banji) context.getBean("banji");
        System.out.println(banji);
        Student student = (Student) context.getBean("student");
        System.out.println(student);
    }

    @Test
    public void testDITripleConstruct() {
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        StudentController studentController = (StudentController) context.getBean("studentController");
        System.out.println(studentController);
        studentController.selectAll();
    }

    @Test
    public void testDIAnnotation() {
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        CourseController courseController = (CourseController) context.getBean("courseController");
        System.out.println(courseController);
        courseController.selectAll();
    }

    @Test
    public void testDIAutowired() {
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        BanjiController banjiController = (BanjiController) context.getBean("banjiController");
        System.out.println(banjiController);
        banjiController.selectAll();
    }
}
