/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/3/20 下午2:50 ~ 2024/3/20 下午3:59
 * Modified date: 2024/3/20 下午3:59
 */

package com.sdust.day7.cal;

public class Div extends Cal{
    public Div() {
    }

    public Div(int num1, int num2) {
        super(num1, num2);
    }

    @Override//这里子类必须重写（实现）. 因为父类是抽象类
    public int getRes() {
        return num1 / num2;
    }
}
