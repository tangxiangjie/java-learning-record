/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/3/22 下午7:46 ~ 2024/3/22 下午8:04
 * Modified date: 2024/3/22 下午8:04
 */

package com.sdust.day9.duotaidemo;

import org.junit.Test;

public class Demo {
    @Test
    public void test1() {
        Shape shape1 = new Circle("Circle", "pi * r * r");
        Shape shape2 = new Triangle("Triangle", "base * height / 2");
        Shape shape3 = new Square("Square", "edge length * edge length");
        draw(shape1);
        draw(shape2);
        draw(shape3);
    }

    private void draw(Shape shape) {//一个函数, 父类参数, 实现子类展示
        shape.show();
        System.out.println(shape);
    }
}
