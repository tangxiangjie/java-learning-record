/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/6 上午12:51
 * Modified date: 2024/6/6 上午12:51
 */

package com.sdust.spring.service.impl;

import com.sdust.spring.dao.IBanjiDao;
import com.sdust.spring.service.IBanjiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//@Service
public class BanjiServiceImpl2 implements IBanjiService {
    //@Autowired
    private IBanjiDao banjiDao;
    @Override
    public void selectAll() {
        System.out.println("BanjiServiceImpl2.selectAll");
        banjiDao.selectAll();
    }
}
