/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/11 下午10:39 ~ 2024/6/12 下午10:20
 * Modified date: 2024/6/12 下午10:15
 */

package com.sdust.springboot.pojo.query;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class UserQuery {
    private Integer page;
    private Integer limit;
    private String name;
    //@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")//不管用 需要MyBeanUtil 转换从req中取到的字符串格式的日期
    private Date beginCreateTime;
    //@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")//不管用 需要MyBeanUtil 转换从req中取到的字符串格式的日期
    private Date endCreateTime;
}
