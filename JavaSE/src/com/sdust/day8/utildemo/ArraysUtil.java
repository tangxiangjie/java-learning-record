/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/3/21 下午1:57 ~ 2024/3/21 下午2:11
 * Modified date: 2024/3/21 下午2:11
 */

package com.sdust.day8.utildemo;

public class ArraysUtil {
    private ArraysUtil() {

    }

    public static int sum(int[] arr) {
        int sum = 0;
        for (int i : arr) {
            sum += i;
        }
        return sum;
    }

    public static int max(int[] arr) {
        int max = arr[0];
        for (int i : arr) {
            max = (max > i) ? max : i;
        }
        return max;
    }

    public static void bubbleSort(int[] arr) {
        int temp;
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = 0; j < arr.length - i - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
    }

}
