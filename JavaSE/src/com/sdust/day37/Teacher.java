/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/5/29 下午7:01 ~ 2024/5/29 下午11:39
 * Modified date: 2024/5/29 下午11:39
 */

package com.sdust.day37;

public class Teacher {
    private String name = "android";
    private int age = 18;
    private String address = "qingdao";

    @Override
    public String toString() {
        return "Teacher [name=" + name + ", age=" + age + ", address="
                + address + "]";
    }
}