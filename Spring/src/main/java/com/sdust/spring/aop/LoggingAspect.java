/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/7 上午10:30
 * Modified date: 2024/6/7 上午10:30
 */

package com.sdust.spring.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

//日志打印切面类(切面=通知+切入点)
@Aspect
public class LoggingAspect {

    //@Pointcut("execution(* com.sdust.spring.service.IBanjiService.selectAll(..))")
    //定义切入点
    @Pointcut("execution(* com.sdust.spring.service.*.*(..))")
    public void selectAllPointcut() {}//可能命名为serviceMethodPointcut()更合适

    // @Before("execution(* com.sdust.spring.service.IBanjiService.selectAll(..))")
    @Before("selectAllPointcut()")
    //前置通知
    public void logBefore(JoinPoint joinPoint) {
        //System.out.println("---------Logging before selectAll.----------");
        System.out.println("---------Logging before " + joinPoint.getSignature().getName() +".----------");
    }

    // @After("execution(* com.sdust.spring.service.IBanjiService.selectAll(..))")
    @After("selectAllPointcut()")
    //后置通知
    public void logAfter(JoinPoint joinPoint) {
        System.out.println("---------Logging after " + joinPoint.getSignature().getName() + ".---------");
    }
}