/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/4/2 上午11:34 ~ 2024/5/25 下午4:27
 * Modified date: 2024/5/25 下午4:27
 */

package com.sdust.web.controller;

import com.sdust.web.pojo.entity.Banji;
import com.sdust.web.pojo.entity.Student;
import com.sdust.web.pojo.vo.StudentBanjiVO;
import com.sdust.web.service.IBanjiService;
import com.sdust.web.service.IStudentService;
import com.sdust.web.service.impl.BanjiServiceImpl;
import com.sdust.web.service.impl.StudentServiceImpl;
import com.sdust.web.util.JSONUtil;
import com.sdust.web.util.PageInfo;
import com.sdust.web.util.Result;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 浏览器能访问jsp，也可以访问servlet
 * 和Web开发相关的jar包不在jdk里面，在Tomcat里面
 * http://localhost:8080/JavaWeb/hello.jsp
 * http://localhost:8080/JavaWeb/student
 */
//@WebServlet("/student")
public class StudentServlet extends HttpServlet {//一开始报红---和Web开发相关的jar包不在jdk里面，在Tomcat里面
    /*private IStudentDao studentDao = new StudentDaoImpl();*/
    private IStudentService studentService = new StudentServiceImpl();
    private IBanjiService banjiService = new BanjiServiceImpl();

    //访问Servlet时候默认访问service方法
    @Override//java重写父类方法
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //System.out.println("StudentServlet.service");
        req.setCharacterEncoding("UTF-8");//解决post请求乱码问题   method="post"
        /*HttpSession session = req.getSession();//每个都要这个操作 ----->  就有了过滤器
        User user = (User) session.getAttribute("user");
        if (user == null) {
            resp.sendRedirect("/login.jsp");
            return;
        }*/

        //http://localhost:8080/student?method=selectAll
        //http://localhost:8080/student?method=deleteById&id=1
        String method = req.getParameter("method");//获取浏览器来的
        if (method == null || method.equals("")) {
            method = "selectAll";
        }
        switch (method) {
            case "selectAll":
                selectAll(req, resp);
                break;
            case "deleteById":
                deleteById(req, resp);
                break;
            case "toAddStudent":
                toAddStudent(req, resp);
                break;
            case "add":
                add(req, resp);
                break;
            case "updateQuery":
                updateQuery(req, resp);
                break;
            case "update":
                update(req, resp);
                break;
            case "selectByPage":
                selectByPage(req, resp);
                break;
            case "selectCount":
                selectCount(req, resp);
                break;
        }

    }

    private void selectCount(HttpServletRequest req, HttpServletResponse resp) {
        System.out.println("StudentServlet.selectCount");
        int stuCounts = studentService.selectCount();
        System.out.println(stuCounts);
        JSONUtil.toJSON(resp, Result.ok(stuCounts));
    }

    private void toAddStudent(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("StudentServlet.toAddStudent");

        List<Banji> banjiList = banjiService.selectAll();
        req.setAttribute("banjiList", banjiList);
        req.getRequestDispatcher("student_add.jsp").forward(req, resp);
    }

    private void selectByPage(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String pageNo = req.getParameter("pageNo");
        String pageSize = req.getParameter("pageSize");
        if (pageNo == null || pageNo.equals("")) {
            pageNo = "1";
        }
        if (pageSize == null || pageSize.equals("")) {
            pageSize = "5";
        }

        PageInfo<StudentBanjiVO> pageInfo = studentService.selectByPage(Integer.parseInt(pageNo), Integer.parseInt(pageSize));

        req.setAttribute("pageInfo", pageInfo);
        req.getRequestDispatcher("student_list3.jsp").forward(req, resp);
    }

    private void update(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        System.out.println("StudentServlet.update");

        String id = req.getParameter("id");
        String name = req.getParameter("name");
        String age = req.getParameter("age");
        String gender = req.getParameter("gender");
        String banjiId = req.getParameter("banjiId");

        Student student = new Student(Integer.parseInt(id), name, Integer.parseInt(age), gender, Integer.parseInt(banjiId));

        studentService.update(student);
        //重定向（让servle执行完之后 自己发送请求）302 服务器告诉浏览器去请求这个地址
        resp.sendRedirect("/student?method=selectAll");
    }

    private void updateQuery(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("StudentServlet.updateQuery");

        String queryId = req.getParameter("id");
        Student student = studentService.selectById(Integer.parseInt(queryId));
        List<Banji> banjiList = banjiService.selectAll();

        //把数据放到req里面
        req.setAttribute("student", student);
        req.setAttribute("banjiList", banjiList);
        //转发到student_list.jsp页面进行展示
        req.getRequestDispatcher("student_update.jsp").forward(req, resp);
    }

    private void add(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        System.out.println("StudentServlet.add");

        String name = req.getParameter("name");
        String age = req.getParameter("age");
        String gender = req.getParameter("gender");
        String banjiId = req.getParameter("banjiId");
        Student student = new Student();
        student.setName(name);
        student.setAge(Integer.parseInt(age));
        student.setGender(gender);
        student.setBanjiId(Integer.parseInt(banjiId));

        studentService.add(student);

        //重定向（让servlet执行完之后 自己发送请求）302 服务器告诉浏览器去请求这个地址
        resp.sendRedirect("/student?method=selectAll");
    }

    private void deleteById(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        System.out.println("StudentServlet.deleteById");
        //http://localhost:8080/deleteStudent?id=2
        String id = req.getParameter("id");

        studentService.deleteById(Integer.parseInt(id));

        //resp啥也没有就是白色
        //重定向（让servle执行完之后 自己发送请求）302 服务器告诉浏览器去请求这个地址
        resp.sendRedirect("/student?method=selectAll");
        //浏览器看到resp是302  自己取到resp的地址  自己再发送一个请求
        //没重定向：1次请求  1次返回（200 白色，因为你也没跟JSP配合展示，所以啥也没有）
        //有重定向：2次请求  2次返回（第一次302 返回重定向命令及地址，第二次200 返回重定向请求的地方）
        //重定向(添加后重定向到selectAll)  VS  转发(servlet转发到jsp)
        //地址栏上的地址  是request决定的
    }

    private void selectAll(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("StudentServlet.selectAll");
        List<StudentBanjiVO> studentList = studentService.selectAll();

        /*//把list数据放到req里面，
        req.setAttribute("studentList", studentList);
        //转发到student_list.jsp页面进行展示
        req.getRequestDispatcher("student_list.jsp").forward(req, resp);*/
        System.out.println("sucesss");

        //把list数据放到req里面
        req.setAttribute("studentList", studentList);
        //转发到student_list.jsp页面进行展示
        req.getRequestDispatcher("student_list.jsp").forward(req, resp);
        //为什么jsp里边直接就可以request.getAttribute
    }
}
