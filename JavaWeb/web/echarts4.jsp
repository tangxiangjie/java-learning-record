<%--
  ~ 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
  ~ Copyright @TangXJ
  ~ Created by TangXJ
  ~ Created&Used date: 2024/5/25 下午3:48 ~ 2024/5/28 下午1:12
  ~ Modified date: 2024/5/28 下午1:12
  --%>

<%--
  Created by IntelliJ IDEA.
  User: TangXJ
  Date: 2024/5/25
  Time: 15:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <title>数字美化展示</title>
    <style>
        .number-container {
            display: flex;
            flex-direction: column;
            align-items: center;
            font-family: Arial, sans-serif;
        }
        .number-display {
            font-size: 48px;
            font-weight: bold;
            color: orange;
            margin: 10px 0;
        }
        .number-title {
            font-size: 24px;
            color: #333;
        }
    </style>
</head>
<body>
<div class="number-container">
    <div id="name">
        <div class="number-title">今日门诊人次</div>
        <div id="countUp" class="number-display" style="color: #c23531;">3,210</div>
    </div>
    <div id="gender">
        <div class="number-title">今日急诊人次</div>
        <div class="number-display" style="color: #2f4554;">1,230</div>
    </div>
    <div id="age">
        <div class="number-title">今日住院人次</div>
        <div class="number-display" style="color: #618da8;">123</div>
    </div>
</div>


<%--<span id="countUp">123</span>--%>

<script src="/static/jquery-2.1.4.js"></script>
<%--<script src="/static/countUp.umd.js"></script>--%>
<script src="/static/countUp.min1.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        // 设置轮询间隔（例如：5000 毫秒，即 5 秒）
        setInterval(fetchData, 5000);

        // 定义数据更新函数
        function fetchData() {
            $.ajax({
                url: '/student?method=selectCount', // 替换为你的后端 API URL
                method: 'GET',
                dataType: 'json', // 确保返回的数据被解析为 JSON 对象
                success: function(result) {
                    console.log(result);
                    console.log(result.data);
                    updateNumber('name', result.data); // 假设返回的数据包含 stuCounts
                },
                error: function(error) {
                    console.error('Error fetching data:', error);
                }
            });
        }

        // 定义数字更新函数
        function updateNumber(elementId, newValue) {
            var element = document.querySelector('#' + elementId + ' .number-display');
            var currentValue = parseInt(element.textContent.replace(/,/g, ''));
            if (isNaN(currentValue)) {
                currentValue = 1325; // 如果当前值不是数字，初始化为 0
            }
            var options = {
                startVal: currentValue,
                //duration: 3, // 动画持续时间
                // useEasing: true,
                // useGrouping: true,
                // separator: ',',
                // decimal: '.',
            };
            var countUp = new CountUp('countUp', currentValue, newValue, 0, 2.5, options);
            //var countUp = new CountUp('countUp', newValue, options);
            if (!countUp.error) {
                countUp.start();
            } else {
                console.error(countUp.error);
            }
        }

        // 初次加载数据
        fetchData();
    });
</script>
</body>
</html>
