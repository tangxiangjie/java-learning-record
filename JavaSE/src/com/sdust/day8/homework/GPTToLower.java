/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/3/23 上午10:43 ~ 2024/3/23 上午10:47
 * Modified date: 2024/3/23 上午10:47
 */

package com.sdust.day8.homework;

public class GPTToLower{
    public static void main(String[] args) {
        String input = "Hello World@@@@!!!!!";
        String result = convertToUpperToLower(input);
        System.out.println("Result: " + result);

        String str = "Hello World";
        String lowercase = str.toLowerCase(); // 将字符串转换为小写形式
        String uppercase = str.toUpperCase(); // 将字符串转换为大写形式

        System.out.println("Original: " + str); // 原始字符串不受影响
        System.out.println("Lowercase: " + lowercase); // 输出小写形式的字符串
        System.out.println("Uppercase: " + uppercase); // 输出大写形式的字符串

    }

    public static String convertToUpperToLower(String str) {
        StringBuilder result = new StringBuilder();
        for (char c : str.toCharArray()) {
            // 如果是大写字母，则转换为小写字母，否则保持原样
            result.append(Character.isUpperCase(c) ? Character.toLowerCase(c) : c);
        }
        return result.toString();
    }
}
