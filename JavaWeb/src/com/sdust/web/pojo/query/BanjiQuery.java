/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/4/16 下午5:10 ~ 2024/4/16 下午5:12
 * Modified date: 2024/4/16 下午5:12
 */

package com.sdust.web.pojo.query;

public class BanjiQuery {
    private Integer page;
    private Integer limit;
    private String name;
    private String address;
    private Integer date;

    public BanjiQuery() {
    }

    public BanjiQuery(Integer page, Integer limit, String name, String address, Integer date) {
        this.page = page;
        this.limit = limit;
        this.name = name;
        this.address = address;
        this.date = date;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getDate() {
        return date;
    }

    public void setDate(Integer date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "BanjiQuery{" +
                "page=" + page +
                ", limit=" + limit +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", date=" + date +
                '}';
    }
}
