/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/4/7 下午4:06 ~ 2024/4/17 下午5:18
 * Modified date: 2024/4/17 下午5:18
 */

package com.sdust.web.dao;

import com.sdust.web.pojo.entity.User;

import java.util.List;

public interface IUserDao {
    List<User> selectByPage(int offset, Integer pageSize);

    int selectTotalCount();

    User login(String name, String password);

    void addUser(User user);

    boolean slecletByName(String username);
}
