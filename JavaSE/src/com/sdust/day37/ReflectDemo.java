/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/5/29 下午5:28 ~ 2024/5/29 下午11:41
 * Modified date: 2024/5/29 下午11:41
 */

package com.sdust.day37;

import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ReflectDemo {

    @Test
    public void test1() {
        Student student2 = new Student();
        student2.setName("张三");
        Student student1 = student2;
        System.out.println(student2);
        System.out.println(student1);

        System.out.println(System.identityHashCode(student1));
        System.out.println(System.identityHashCode(student2));
        System.out.println("-------------------------------");

        String student3 = Student.studentInClass;
        String student4 = student3;
        System.out.println(System.identityHashCode(Student.studentInClass));
        System.out.println(System.identityHashCode(student3));
        System.out.println(System.identityHashCode(student4));
        System.out.println("-------------------------------");

        Class clazz = Student.class;//写Student类(类)的过程，同时也是写(生成)Student.class(实例对象)的过程
        Class clazz2 = clazz;
        System.out.println(System.identityHashCode(Student.class));
        System.out.println(System.identityHashCode(clazz));
        System.out.println(System.identityHashCode(clazz2));
    }

    @Test
    public void testClass() throws ClassNotFoundException {
        //获得字节码对象的三种方式，获得后就可以继续
        //1.Class.forName()
        Class clazz1 = Class.forName("com.sdust.day37.Student");
        //2.类型.class
        Class clazz2 = Student.class;
        //3.对象.getClass()
        Student student = new Student();
        Class clazz3 = student.getClass();

        System.out.println(clazz1);
        System.out.println(clazz2);
        System.out.println(clazz3);

        System.out.println(clazz1 == clazz2);//比较的是地址值 //true
        System.out.println(clazz1 == clazz3);              //true
        //变量放栈 对象放堆   栈里边的变量clazz1 2 3 指向同一个堆里边的对象Student.class
    }

    @Test
    public void testConstructor1() {
        Class clazz = Student.class;
        Constructor[] constructors = clazz.getDeclaredConstructors();
        //Constructor[] constructor2s = clazz.getConstructors();
        for (Constructor constructor : constructors) {
            System.out.println(constructor);//Constructor对象
            System.out.println(constructor.getName());//构造方法的名字
            System.out.println(constructor.getModifiers());//构造方法修饰符 1 public 2 private
            //单例模式  构造方法私有
        }
    }

    @Test
    public void testConstructor2() throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        //----------正常new------------
        Student student1 = new Student();
        student1.setId(1);
        student1.setName("张三");
        student1.setAge(23);
        student1.setGender("女");
        System.out.println(student1);
        //----------反射new--------------
        //private Student(Integer id, String name, Integer age, String gender) {
        Class clazz = Student.class;//拿到字节码对象(拿到Class对象)
        Constructor constructor = clazz.getDeclaredConstructor(Integer.class, String.class, Integer.class, String.class);
        // 将构造方法设置为可访问
        constructor.setAccessible(true);
        Student student2 = (Student) constructor.newInstance(2, "王五", 25, "男");
        System.out.println(student2);
    }

    @Test
    public void testMethod() throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, ClassNotFoundException {
        //Student student = new Student();
        //student.setName("赵六");
        //Class clazz = Student.class;
        Class clazz = Class.forName("com.sdust.day37.Student");
        Constructor constructor = clazz.getConstructor();//得到无参构造方法
        Student student = (Student) constructor.newInstance();//实例化
        //student.setName("赵六");
        Method setName = clazz.getMethod("setName", String.class);//方法名, 参数类型
        setName.invoke(student, "赵六");
        System.out.println(student);
    }

    @Test
    public void testPointClass() {
        System.out.println(String.class);
        System.out.println("------------------");
        Class clazz = Teacher.class;
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            System.out.println(field.getClass());
            System.out.println(field.getType());
        }
    }
}
