/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/3/25 上午9:05 ~ 2024/3/25 上午9:10
 * Modified date: 2024/3/25 上午9:10
 */

package com.sdust.day10;

//实体类 与 数据库表 一一对应
public class Teacher extends Object{
    private Integer id; //null    int id; //0
    private String name;
    private Integer age;
    private Double salary;//包装数据类型 初始值无意义，避免基本数据类型默认值跟真实值冲突

    public Teacher() {//构造方法特点：1.与类名相同 2.不需要返回值 3
        //super();//默认提供
    }

    public Teacher(Integer id, String name, Integer age, Double salary) {//构造方法重载 最典型的一个
        //super();//默认提供
        this.id = id;
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    @Override//重写 覆盖 覆写
    public String toString() {
        return "Teacher{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", salary=" + salary +
                '}';
    }
}


