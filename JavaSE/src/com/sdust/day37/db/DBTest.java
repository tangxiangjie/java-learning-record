/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/5/30 上午12:15 ~ 2024/5/30 上午12:16
 * Modified date: 2024/5/30 上午12:16
 */

package com.sdust.day37.db;

import com.sdust.day37.Student;
import com.sdust.day37.Teacher;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Properties;

public class DBTest {

    public static void main(String[] args) throws IOException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        //MySql mySql = new MySql();
        //IDB mySql = new MySql();//声明成父类类型 new子类
        //IDB db = new MySql();//声明成父类类型 new子类
        //db.getConnection();
        //db = new Oracle();
        //db.getConnection();
        //只出现接口 不出现具体的实现类 -> mysql换其他数据库 不用改代码
        //面向接口编程
        //反射+配置文件

        //FileInputStream fileInputStream = new FileInputStream("/src/com/sdust/day37/db/db1.properties");
        //1.通过当前的类获取类加载器
        ClassLoader classLoader = DBTest.class.getClassLoader();
        //2.通过类加载器的方法获取输入流
        InputStream inputStream = classLoader.getResourceAsStream("db1.properties");
        Properties properties = new Properties();
        properties.load(inputStream);
        String className = properties.getProperty("className");
        Class clazz = Class.forName(className);

        //Constructor constructor = clazz.getConstructor();
        //IDB db = (IDB) constructor.newInstance();
        //如果是new无参构造方法 可以直接这样写
        IDB db = (IDB) clazz.newInstance();
        db.getConnection();
    }

    public void db(IDB db) {//面向接口编程
        db.getConnection();
    }

    @Test
    public void testDb() {
        IDB db = new MySql();
        db(db);
        db = new SqlServer();
        db(db);
    }

    @Test
    public void testReflect2() throws IllegalAccessException {
        Teacher teacher = new Teacher();
        System.out.println(teacher);
        changeField(teacher);
        System.out.println(teacher);

        Student student = new Student("ZhangSan");
        System.out.println(student);
        changeField(student);
        System.out.println(student);
    }

    //动态性体现在传递任意一个对象使用反射都可以完成这个功能。
    private void changeField(Object object) throws IllegalAccessException {
        //1.获得对应的字节码
        Class clazz = object.getClass();//对象.getClass。  类.class
        //2.获取所有的fields
        Field[] fields = clazz.getDeclaredFields();//所有的  包括私有的
        //3.遍历所有的field，如果是String类型，将包含a改为b
        for (Field field : fields) {
            //3.1 是String类型才修改
            if (field.getType() == String.class) {
                //属性是私有的
                field.setAccessible(true);
                //person.getName()
                //3.2 获得field原来的值
                String oldValue = (String) field.get(object);
                if (oldValue == null || "".equals(oldValue)) {
                    continue;
                }
                String newValue = oldValue.replace("a", "b");
                //3.3 将修改后的值设置到field
                //person.setName("11");
                field.set(object, newValue);
            }
        }
    }
}
