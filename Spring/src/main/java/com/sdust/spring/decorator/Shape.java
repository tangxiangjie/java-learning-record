/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/11 上午10:58
 * Modified date: 2024/6/11 上午10:58
 */

package com.sdust.spring.decorator;

public interface Shape {
    void draw();
}
