/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/3/20 下午2:30 ~ 2024/4/12 下午7:57
 * Modified date: 2024/4/12 下午7:57
 */

package com.sdust.day7.cal;

public class Add extends Cal{

    public Add() {

    }

    public Add(int num1, int num2) {
        super(num1, num2);
    }

    @Override
    public int getRes() {
        return num1 + num2;
    }

    public void onlyAdd() {
        System.out.println("Add.onlyAdd");
    }
}
