package com.sdust.day3;

import org.junit.Test;
import com.sdust.day2.*;

import java.util.Scanner;

public class WhileForSwitchDemo {

    //Switch case
    @Test
    public void test1() {
        Scanner scanner = new Scanner(System.in);
        int num;
        System.out.println("input: ");
        num = scanner.nextInt();
        switch (num) {
            case 1:
                System.out.println("1");
                break;
            case 2:
                System.out.println("2");
                break;
            default:
                System.out.println("default");
                break;
        }
    }

/*    @Test
    //月份 天数
    public void test2() {
        Scanner scanner = new Scanner(System.in);
        Homework day2 = new Homework();
        System.out.println("please input month: ");
        int month = scanner.nextInt();
        if (month < 1 || month > 12) {
            System.out.println("invalid month");
            return;
        }
        switch (month) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                System.out.println("31days");
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                System.out.println("30days");
                break;
            case 2:
                System.out.println("please input year: ");
                int year = scanner.nextInt();
                if (day2.judgeLeapYear(year)) {
                    System.out.println("29days");
                } else {
                    System.out.println("28days");
                }
            default:
                System.out.println("default");
                break;
        }
    }*/
}
