/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/3/20 下午2:50 ~ 2024/3/20 下午2:54
 * Modified date: 2024/3/20 下午2:54
 */

package com.sdust.day7.cal;

public class Mul extends Cal{
    public Mul() {
    }

    public Mul(int num1, int num2) {
        super(num1, num2);
    }

    @Override
    public int getRes() {
        return num1 * num2;
    }
}
