/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/3/27 下午4:00 ~ 2024/3/27 下午4:59
 * Modified date: 2024/3/27 下午4:59
 */

package com.sdust.day12;

import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.Properties;

public class JDBCUtil {
    private static String driver;
    private static String url;
    private static String username;
    private static String password;

    private JDBCUtil(){}

    //静态代码块，在类加载的时候只会执行一次，避免重复加载驱动   还没调方法就执行了
    //读取配置文件
    static {
        try {
            //1.通过当前的类获取类加载器
            ClassLoader classLoader = JDBCUtil.class.getClassLoader();//反射 JVM会说  先当成固定的结论就行了
            //2.通过类加载器的方法获得一个输入流
            InputStream inputStream = classLoader.getResourceAsStream("db.properties");//不是因为叫了resource，是因为标识了Mark as
            //3.创建一个Properties对象   对配置文件的封装
            Properties properties = new Properties();
            properties.load(inputStream);
            //4.获取配置文件中的参数的值
            driver = properties.getProperty("driver");
            url = properties.getProperty("url");
            username = properties.getProperty("username");
            password = properties.getProperty("password");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        try {
            Class.forName(driver);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public static Connection getConnection() throws SQLException {
        Connection connection = DriverManager.getConnection(url, username, password);
        return connection;
        /*Connection connection = null;
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/study?useSSL=false&useUnicode=true&characterEncoding=utf8&serverTimezone=GMT%2b8", "root", "xiangjie");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return connection;*/
    }

    public static void close(Connection connection, Statement statement, ResultSet resultSet) {
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
