/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/3/22 下午2:14 ~ 2024/3/25 下午9:27
 * Modified date: 2024/3/25 下午9:27
 */

package com.sdust.day10;

import com.sdust.day8.staticmethoddemo.Student;
import org.junit.Test;

import java.security.Key;
import java.security.KeyStore;
import java.util.*;

public class CollectionDemo {
    @Test
    public void test1() {
        String[] array = new String[3];
        //List: 有序 可重复
        //有序: 放入顺序 与  拿出顺序一致，就是有序
//        List<String> list = new ArrayList<>();//多态 声明成父类, new子类类型
        ArrayList<String> list = new ArrayList<>();
        list.add("Java");
        list.add("UI");
        list.add("H5");
        list.add("H5");
        list.add("Python");
        list.add("aa");
        for (String str : list) {
            System.out.println(str);
        }
        System.out.println("------------------");
        //Set:无序 不重复
        //无序: 放入顺序与拿出顺序不一定一致,可能不一致
        HashSet<String> set = new HashSet<>();
//        Set<String> set = new HashSet<>();//多态, 声明成父类, new子类类型
        set.add("Java");
        set.add("UI");
        set.add("H5");
        set.add("H5");
        set.add("aa");
        for (String str : set) {
            System.out.println(str);
        }
    }

    //LinkedList

    //Map
    @Test
    public void test2() {
        //Map<String, String> map = new HashMap<>();//多态, 声明成父类, new子类类型
        HashMap<String, String> map = new HashMap<>();//HashMap不是接口
        map.put("CN", "中国");
        map.put("US", "美国");
        map.put("UK", "英国");
        Set<Map.Entry<String, String>> set = map.entrySet();//entry Set集合
        //① 遍历map 转为遍历set
        for (Map.Entry<String, String> entry : set) {
            System.out.println(entry.getKey() + ": " + entry.getValue());//放入顺序和拿出顺序不一定一样
        }
        System.out.println("-----------------");
        //②
        String country = map.get("CN");
        System.out.println(country);
        System.out.println("-----------------");
        //key不能重
        //② 根据key遍历
        Set<String> ketSet = map.keySet();
        for (String key : ketSet) {
            System.out.println(key + ": " + map.get(key));
        }
    }

    //迭代器
    @Test
    public void test3() {
        //Set遍历的时候, 可以使用for循环, 也可以使用迭代器遍历。
        HashMap<String, String> map = new HashMap<>();
        map.put("CN", "中国");
        map.put("US", "美国");
        map.put("UK", "英国");
        Set<String> set = map.keySet();
        Iterator<String> iterator1 = set.iterator();
        while (iterator1.hasNext()) {
            String key = iterator1.next();
            System.out.println(key + ": " + map.get(key));
        }
        System.out.println("-----------------");

        Set<Map.Entry<String, String>> entrySet = map.entrySet();
        Iterator<Map.Entry<String, String>> iterator2 = entrySet.iterator();
        while (iterator2.hasNext()) {
            Map.Entry<String, String> entry = iterator2.next();
            System.out.println(entry.getKey() + ": " + entry.getValue());//放入顺序和拿出顺序不一定一样
        }
    }

    @Test
    public void test4() {//第一周周测 勘误
        //byte i1 = 100, j1 = 200;
        int i2 = 100, j2 = 200;
        char c1 = 65;
        char c2 = 'A' + 32;
        char c3 = 'A';
        //char c4 = c3 + 32;
        char c5 = 32767;
        //char[] arr1 = new char[5]{1,2,3,4,5};
        char[] arr2 = {1,2,3,4,5};
        char[] arr3 = new char[]{1,2,3,4,5};
        System.out.println(c1);
        System.out.println(c2);
        System.out.println(c5);
        final float PI;
        PI = 3.14f;
        short s = 'a' + 1;
        System.out.println(s);
    }
}
