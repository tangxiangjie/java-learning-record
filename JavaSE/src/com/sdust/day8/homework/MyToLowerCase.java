/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/3/21 下午11:15 ~ 2024/3/22 下午1:52
 * Modified date: 2024/3/22 下午1:52
 */

package com.sdust.day8.homework;

import org.junit.Test;

import java.util.Scanner;

public class MyToLowerCase {
    public String myToLowerCase(String str) {
        char[] chars = str.toCharArray();
/*        for (char aChar : chars) {//aChar是临时变量，给它赋值并不会改变char数组中的值
            System.out.println(aChar);
            aChar = (aChar >= 'A' && aChar <= 'Z') ? (char)(aChar + 32) : aChar;
        }*/
        for (int i = 0; i < chars.length; i++) {
//            chars[i] = (chars[i] >= 'A' && chars[i] <= 'Z') ? (char)(chars[i] + 32) : chars[i];
            if (chars[i] >= 'A' && chars[i] <= 'Z') {
                chars[i] = (char)(chars[i] + 32);
            }
        }
        return new String(chars);
    }

    @Test
    public void test1() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入字符串");
        String str2 = scanner.nextLine();
        //String str1 = "I love Java!";
        System.out.println("你输入的字符串是: " + str2);
        System.out.println("转换结果: " + myToLowerCase(str2));
    }
}
