/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/4/8 下午3:53 ~ 2024/4/17 上午9:22
 * Modified date: 2024/4/17 上午9:22
 */

package com.sdust.web.dao;

import com.sdust.web.pojo.entity.Banji;
import com.sdust.web.pojo.query.BanjiQuery;
import com.sdust.web.pojo.vo.BanjiCountVO;

import java.util.List;

public interface IBanjiDao {
    List<Banji> selectAll();

    List<Banji> selectByPage(int offset, Integer pageSize);

    int selcetTotalCount();

    void add(Banji banji);

    List<BanjiCountVO> selectBanjiCount();

    void deleteById(int id);

    /**
     * dao应该是纯粹的数据库操作，在这里边写for循环也相当于写了逻辑，不太符合三层架构原则
     **/
    void deleteSelected(String[] ids);

    Banji selectById(int id);

    void updateForm(Banji banji);

    int selcetTotalCount(BanjiQuery banjiQuery);

    List<Banji> selectByPage(BanjiQuery banjiQuery);
}
