/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/3/21 下午2:49 ~ 2024/3/22 上午11:21
 * Modified date: 2024/3/22 上午11:21
 */

package com.sdust.day9;

import org.junit.Test;

import java.util.Arrays;

public class StringDemo {
    @Test
    public void test1() {
        //"abc"双引号括起来的字符串：字符串常量，它也是一个对象
        // 1.8之后常量池放到堆，在常量池里面找有没有这个"abc"对象，
        // 如果常量池里面没有这个字符串对象，java就帮你在常量池里面new一个"abc"字符串对象。
        // 如果常量池里面有这个字符串，就把这个字符串的地址赋给这个变量。
        String str1 = "abc";
        String str2 = new String("abc");
        String str3 = "abc";
        //------------   == 与 equals -----------------------//
        // ==: 比较的是变量指向的对象的地址值
        System.out.println(str1 == str2);//false
        System.out.println(str1 == str3);//true
        System.out.println(str2 == str3);//false

        // equals: 比较字符串的内容是不是相等
        // 比较字符串的内容是不是相等使用equals()
        // 表str1这个字符串和str2这个字符串的内容是不是相等
        System.out.println(str1.equals(str2));//true
        System.out.println(str1.equals(str3));//true
        System.out.println(str2.equals(str3));//true
    }

    @Test
    public void test2() {
        String str = "Java AndroidA";
        System.out.println(str.length());//13
        char[] chars = str.toCharArray();
        char ch = str.charAt(5);
        System.out.println(ch);//'A'
        System.out.println(str.indexOf('A'));//5 char -> int小的自动转大的
        System.out.println(str.indexOf('X'));
        System.out.println(str.indexOf('A', 6));//12
        System.out.println(str.indexOf("And"));
        System.out.println(str.lastIndexOf('A'));//12
    }

    @Test
    public void test3() {
        String str1 = "java";
        String str2 = "Java";
        System.out.println(str1.equals(str2));//false
        System.out.println(str1.equalsIgnoreCase(str2));//true

        String str = "java AndroidA";
        System.out.println(str.contains("And"));//true
        System.out.println(str.startsWith("java"));//true
        //offset:偏移
        System.out.println(str.startsWith("And", 5));//true
        System.out.println(str.endsWith("oidA"));//true
        System.out.println(str.isEmpty());//false
        // ""空字符串，也是一个对象
        System.out.println("".isEmpty());//true
    }

    @Test
    public void test4() {
        char[] chars1 = {'j', 'a', 'v', 'a', ' ', 'A', 'n', 'd'};
        String str1 = new String(chars1);
        System.out.println(str1);
        char[] chars2 = str1.toCharArray();
        System.out.println(chars2);
        //Arrays: 操作数组的工具类
        System.out.println(Arrays.toString(chars1));//[j, a, v, a,  , A, n, d]
    }

    @Test
    public void test5() {
        String str = "java AndroidA";
        String newStr = str.replace('A', 'B');
        System.out.println(newStr);
    }

    @Test
    public void test6() {
        String str = "java android python";
        String[] array = str.split(" ");
        System.out.println(array);
        System.out.println(Arrays.toString(array));
    }

    @Test
    public void test7() {
        // subtree: 子树
        // substring:子字符串
        String str = "java AndroidA";
        System.out.println(str.substring(5));//AndroidA

        //beginIndex – the beginning index, inclusive.
        //endIndex – the ending index, exclusive.
        System.out.println(str.substring(5, 8));//Andr
    }

    @Test
    public void test8() {
        String str1 = "Java";
        String str2 = "PHP";
        String str3 = "Python";
        String str4 = "OpenHarmony";
        String str = str1 + str2 + str3 + str4;
        System.out.println(str);
        //执行效率非常低 String长度不可变，相加过程会new新的str，中间的还要回收

        StringBuffer buffer = new StringBuffer("C");//线程安全 效率低
//        buffer.append(str1);
//        buffer.append(str2);
//        buffer.append(str3);
//        buffer.append(str4);
        //链式编程
        buffer.append(str1).append(str2).append(str3).append(str4);
        System.out.println(buffer);

        StringBuilder builder = new StringBuilder("C");//线程安全 效率低
        builder.append(str1);
        builder.append(str2);
        builder.append(str3);
        builder.append(str4);
        //也可以链式编程
        System.out.println(builder);

        String str5 = "Java" + "PHP" + "Python" + "C";
        //字符串常量的拼接无所谓 编译器会做优化
    }

    @Test
    public void test9() {
        //public static final int   MAX_VALUE = 0x7fffffff;//16进制 全1
        System.out.println(Integer.MAX_VALUE);//常量命名规则 全大写下划线连接

        //自动装箱和自动拆箱
        //'Integer(int)' is deprecated and marked for removal
        // JDK9之后deprecate 不赞成
        Integer integer1 = new Integer(3);//手动装箱
        System.out.println(integer1);
        Integer integer2 = 4;//基本数据类型--->引用数据类型   自动装箱(Java帮做new)
        System.out.println(integer2);
        //int i = integer2.intValue();//手动拆箱
        int i = integer2;//引用数据类型--->基本数据类型  自动拆箱
        Integer integer3 = Integer.valueOf(5);//手动装箱
        System.out.println(integer3);

        Integer integer4 = 3;
        int num2 = integer4 + 2;
        integer4 = integer4 + 4;
        System.out.println(integer4);
    }

    @Test
    public void test10() {
        //把字符串转换成基本数据类型（从网页的输入框中得到的都是字符串）
        String str1 = "123";
        int i = Integer.parseInt(str1);
        i = i + 1;
        System.out.println(i);
        System.out.println(Double.parseDouble("6.9999"));

        //下面的不常用
        String str = "123";
        //Integer integer1 = new Integer(str);//deprecated 不赞成
        //Integer integer2 = new Integer("4");//deprecated 不赞成
        Integer integer1 = Integer.valueOf(str);
        Integer integer2 = Integer.valueOf("1");
        int j = integer1.intValue();
        int k = integer2.intValue();
        System.out.println(j);
        System.out.println(k);
    }

    @Test
    public void test11() {
        //把基本数据类型转换成字符串
        String str1 = 12 + "";
        System.out.println(str1);

        String str2 = Integer.toString(3);
        String str3 = Double.toString(3.15);
        System.out.println(str2);
        System.out.println(str3);
    }
}
