/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/5 下午3:21
 * Modified date: 2024/6/5 下午3:21
 */

package com.sdust.spring.dao.impl;

import com.sdust.spring.dao.ICourseDao;
import org.springframework.stereotype.Repository;

// <bean name="studentDao" class="com.sdust.spring.dao.impl.StudentDaoImpl"/>
@Repository("courseDao")
public class CourseDaoImpl implements ICourseDao {
    @Override
    public void selectAll() {
        System.out.println("CourseDaoImpl.selectAll");
    }
}
