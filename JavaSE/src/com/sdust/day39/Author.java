/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/5/30 下午2:56
 * Modified date: 2024/5/30 下午2:56
 */

package com.sdust.day39;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

// 元注解，表示这个注解可以作用到方法、属性、类上
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Author {

    String name() default "";

    //带默认值的属性，使用时候不给这个属性赋值，也有一个默认值
    int age() default 20;

    //如果使用注解里面的value属性，使用的时候可以省略.
    //前提是：注解中只有这一个属性时候
    String[] value() default {};
}
