package com.sdust.day2;

import org.junit.Test;

import java.util.Random;
import java.util.Scanner;

public class Homework {

    //猜数字 10次机会
    @Test
    public void guessNumber() {
        Random random = new Random();
        // [0, 1000) + 1
        // [1, 1000]
        int num = random.nextInt(1000) + 1;
        Scanner scanner = new Scanner(System.in);
        int cnt = 10;
        int num_guess;
        while (cnt > 0) {
            System.out.println("猜吧!请输入你猜的数字: ");
            num_guess = scanner.nextInt();
            System.out.println("你猜的数字是: " + num_guess);
            if (num_guess > num) {
                System.out.println("太大了");
                cnt--;
            } else if(num_guess < num) {
                System.out.println("太小了");
                cnt--;
            } else {
                System.out.println("恭喜你，猜对了");
                break;
            }
        }
        if (cnt == 0) {
            System.out.println("次数用尽，猜数失败");
        }
    }

    //打印99乘法表
    @Test
    public void print99Table() {
        for (int i = 1; i <= 9; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print(j + "×" + i + "=" + (i * j));
                if(j != i) {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }

/*    //判断闰年
    @Test
    public boolean judgeLeapYear(int year_in) {
        //System.out.println("请输入你要查询的年份: ");
        //Scanner scanner = new Scanner(System.in);
        //int year_in = scanner.nextInt();
        if ((year_in % 4 == 0 && year_in % 100 != 0) || (year_in % 400 == 0)) {
            System.out.println(year_in + "年是闰年");
            return true;
        } else {
            System.out.println(year_in + "年不是闰年");
            return false;
        }
    }*/

    //交换数字
    @Test
    public void exchangeNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入第一个数: ");
        int num1 = scanner.nextInt();
        System.out.println("请输入第二个数: ");
        int num2 = scanner.nextInt();
        System.out.println("交换前: num1 = " + num1 + "\t" + "num2 = " + num2);
        int temp = num1;
        num1 = num2;
        num2 = temp;
        System.out.println("交换后: num1 = " + num1 + "\t" + "num2 = " + num2);
    }

    //收银台
    @Test
    public void recvMoney() {
        int cnt;
        double price, total, payment, change;
        Scanner scanner = new Scanner(System.in);

        System.out.print("请输入商品单价: ");
        price = scanner.nextDouble();
        System.out.println(price);

        System.out.print("请输入商品数量: ");
        cnt = scanner.nextInt();
        System.out.println(cnt);

        System.out.print("请付款: ");
        payment = scanner.nextDouble();
        System.out.println(payment);

        total = price * cnt;
        if (total >= 500) {
            System.out.println("商品总价超过500元, 给您打八折!");
            total *= 0.8;
        }
        if (payment < total) {
            System.out.println("对不起, 您的钱不够");
        } else {
            change = payment - total;
            System.out.println("找您" + String.format("%.2f", change));
        }
    }

    //输出直角三角形数
    @Test
    public void printRightTriangle() {
        for (int i = 1; i <= 5; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print(j);
            }
            System.out.println();
        }
    }

    public void printEquilateralTriangle(int n) {
        int m;
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= n - i; j++) {
                System.out.print(' ');
            }
            m = 2 * (i - 1) + 1;
            for (int j = 1; j <= m; j++) {
                System.out.print('*');
            }
            System.out.println();
        }
    }

    public void printInvertedTriangle(int n) {
        int m;
        for (int i = n; i >= 1; i--) {
            for (int j = 1; j <= n - i; j++) {
                System.out.print(' ');
            }
            m = 2 * (i - 1) + 1;
            for (int j = 1; j <= m; j++) {
                System.out.print('*');
            }
            System.out.println();
        }
    }

    //打印正三角/倒三角形
    @Test
    public void printTriangle() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("请输入三角形阶数: ");
        int num = scanner.nextInt();
        System.out.println(num);

        System.out.print("请输入你要打印的三角形类型(1 正三角形; 2 倒三角形): ");
        int flag = scanner.nextInt();
        System.out.println(flag);

        int n = num;
        int m;

        if (flag == 1) {
            printEquilateralTriangle(n);
        } else if (flag == 2) {
            printInvertedTriangle(n);
        } else {
            System.out.println("非法输入 ");
        }
    }

    //计算1-100以内所有奇数的和以及所有偶数的和，分别打印出来
    @Test
    public void printSum() {
        int sum_odd = 0;
        int sum_even = 0;
        for (int i = 1; i <= 100; i++) {
            if(i % 2 == 1) {
                sum_odd += i;
            }else {
                sum_even += i;
            }
        }
        System.out.println("sum_odd = " + sum_odd);
        System.out.println("sum_even = " + sum_even);
    }

    //用for循环输出1—1000之间能被5整除的数，且每行输出3个
    @Test
    public void printModFive() {
        int cnt = 0;
        for (int i = 1; i <= 1000; i++) {
            if(i % 5 == 0) {
                System.out.print(i);
                cnt++;
                if(cnt % 3 != 0) {
                    System.out.print(' ');
                }
                else {
                    cnt = 0;
                    System.out.println();
                }
            }
        }
    }

    //计算9的阶乘
    @Test
    public void printFactorial() {
        int ans = 1;
        for(int i = 1; i <= 9; i++) {
            ans *= i;
        }
        System.out.println(ans);
    }
}
