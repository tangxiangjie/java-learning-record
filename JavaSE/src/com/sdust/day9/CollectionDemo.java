/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/3/22 下午2:14 ~ 2024/3/22 下午7:20
 * Modified date: 2024/3/22 下午7:20
 */

package com.sdust.day9;

import com.sdust.day8.staticmethoddemo.Student;
import org.junit.Test;

import java.util.*;

public class CollectionDemo {
    @Test
    public void test1() {
        //数组最大的问题是长度固定, 而且需要操作下标。
        Student[] array = new Student[3];

        ArrayList<Student> list = new ArrayList<>();
        Student student1 = new Student();
        Student student2 = new Student();
        Student student3 = new Student();
        Student student4 = new Student();
        list.add(student1);
        list.add(student2);
        list.add(student3);
        list.add(student4);
        list.add(student1);

        //有序可重复
        //有序：你放进去的顺序和拿出来的顺序一致
        //ArrayList<String> list1 = new ArrayList<>();
        List<String> list1 = new ArrayList<>();
        list1.add("Java");
        list1.add("UI");
        list1.add("H5");
        list1.add("H5");
        list1.add("aa");
        for (String s : list1) {
            System.out.println(s);
        }
        System.out.println("-------------------");
        //无序不重复
        //无序：放进去顺序和拿出来的顺序可能是不一致的
        //HashSet<String> set = new HashSet<String>();
        Set<String> set = new HashSet<>();
        set.add("Java");
        set.add("UI");
        set.add("H5");
        set.add("H5");
        set.add("aa");
        for (String s : set) {
            System.out.println(s);
        }
    }
}
