/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/3/21 上午10:40 ~ 2024/3/25 上午9:13
 * Modified date: 2024/3/25 上午9:13
 */

package com.sdust.day8.interfacedemo;

//接口就是一个纯粹的抽象类，接口就是一个标准
public interface IFly {
    //public static final double PI = 3.14;
    public static final int num = 0;
    //public abstract void fly();
    void fly();

}
