/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/5 下午3:19
 * Modified date: 2024/6/5 下午3:19
 */

package com.sdust.spring.controller;

import com.sdust.spring.service.ICourseService;
import com.sdust.spring.service.impl.CourseServiceImpl;
import org.springframework.stereotype.Controller;

import javax.annotation.Resource;

// <bean name="studentController" class="com.sdust.spring.controller.StudentController">
/**
 * @Controller,@Service,@Repository
 *没什么区别，都是知道Spring容器实例化，实现IOC
 * 本质是@Component
 * */
//@Controller("courseController")//取名麻烦
@Controller//不起名 有默认的名字
public class CourseController {
    // private ICourseService courseService = new CourseServiceImpl();
    @Resource(name = "courseService")//注入的时候写名也有点麻烦   见BanjiController

    private ICourseService courseService;

    public void selectAll() {
        System.out.println("StudentController.selectAll");
        courseService.selectAll();
    }
}
