/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/5 下午3:20
 * Modified date: 2024/6/5 下午3:20
 */

package com.sdust.spring.service.impl;

import com.sdust.spring.dao.ICourseDao;
import com.sdust.spring.dao.impl.CourseDaoImpl;
import com.sdust.spring.service.ICourseService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

// <bean name="studentService" class="com.sdust.spring.service.impl.StudentServiceImpl">
@Service("courseService")
public class CourseServiceImpl implements ICourseService {
    //private ICourseDao courseDao = new CourseDaoImpl();
    @Resource(name = "courseDao")
    private ICourseDao courseDao;

    @Override
    public void selectAll() {
        System.out.println("CourseService.selectAll");
        courseDao.selectAll();
    }
}
