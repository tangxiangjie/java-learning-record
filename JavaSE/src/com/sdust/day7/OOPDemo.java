/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/3/20 下午2:04 ~ 2024/3/21 下午10:21
 * Modified date: 2024/3/21 下午10:21
 */

package com.sdust.day7;

import org.junit.Test;

public class OOPDemo {

    @Test
    public void test() {
        Student student = new Student(1, "Tom", 23, "java");
        System.out.println(student);//com.sdust.day7.Student@d7b1517
        System.out.println(student.toString());//com.sdust.day7.Student@d7b1517从父类的父类Object继承来的
    }

    @Test
    public void test2() {
        Person person = new Person();
        person.age = 12;
        System.out.println(person);
    }

    @Test
    public void test3() {
        Student student = new Student();
        student.age = 15;
        System.out.println(student);
        student.test2();
    }
}
