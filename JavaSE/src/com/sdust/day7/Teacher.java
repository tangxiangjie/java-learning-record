/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/3/20 上午11:31 ~ 2024/3/20 下午8:59
 * Modified date: 2024/3/20 下午8:59
 */

package com.sdust.day7;

import com.sdust.day7.Person;
import org.junit.Test;

public class Teacher extends Person {
    //    private int id;
//    private String name;
//    private int age;
    private double salary;

//    public void show() {
//        System.out.println("Teacher.show");
//    }

/*    public Teacher(int id, String name, int age, double salary) {
        super(id, name, age);
        this.salary = salary;
    }*/

    public void teach() {
        System.out.println("Teacher.teach");
    }

    @Test
    public void test2() {
        System.out.println("hello world");
    }

}
