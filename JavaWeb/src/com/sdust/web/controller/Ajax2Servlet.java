/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/4/12 上午11:38 ~ 2024/4/12 上午11:51
 * Modified date: 2024/4/12 上午11:51
 */

package com.sdust.web.controller;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet("/ajax2")
public class Ajax2Servlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("Ajax2Servlet.doPost");
        String name = req.getParameter("name");
        // {"exist":true, "msg":"此用户已经存在，请更换一个"}
        // {"exist":false, "msg":"此用户可用"}
        Map<String, Object> map = new HashMap<>();
        if ("tom".equals(name)) {
            map.put("exist", true);
            map.put("msg", "此用户已经存在，请更换一个");
        } else {
            map.put("exist", false);
            map.put("msg", "此用户可用");
        }

        resp.setContentType("text/html;charset=UTF-8");
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(resp.getWriter(), map);
    }
}
