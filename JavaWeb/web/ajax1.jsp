<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--
  ~ 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
  ~ Copyright @TangXJ
  ~ Created by TangXJ
  ~ Created&Used date: 2024/4/12 上午11:15 ~ 2024/4/12 上午11:32
  ~ Modified date: 2024/4/12 上午11:32
  --%>

<html>
<head>
    <title>Title</title>
</head>
<body>
<button onclick="ajaxGet()">ajax get</button>
<button onclick="ajaxPost()">ajax post</button>
<button onclick="ajax()">ajax</button>

<script src="/static/jquery-2.1.4.js" type="text/javascript" charset="utf-8"></script>
<script>
    function ajaxGet() {
        //$.get(url, [data], [callback], [type])
        $.get(
            '/ajax1',
            {'name': '张三1'},
            function(jsonObj) {
                console.log(jsonObj);
            },
            'json'
        );
    }

    function ajaxPost() {
        //$.post(url, [data], [callback], [type])
        $.post(
            '/ajax1',
            {'name': '张三2'},
            function(jsonObj) {
                console.log(jsonObj);
            },
            'json'
        );
    }

    function ajax() {
        //$.ajax( { option1:value1,option2:value2... } );
        $.ajax({
            async: true,//false 同步  sleep能看出来
            url: '/ajax1',
            type: 'GET',
            date: {'name': '王五'},
            success: function (jsonObj) {
                console.log(jsonObj);
            }
        });
    }
</script>
</body>
</html>
