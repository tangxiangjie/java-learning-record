/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/4/9 上午10:19 ~ 2024/4/9 上午11:49
 * Modified date: 2024/4/9 上午11:49
 */

package com.sdust.web.service.impl;

import com.sdust.web.dao.ICourseDao;
import com.sdust.web.dao.impl.CourseDaoImpl;
import com.sdust.web.pojo.entity.Course;
import com.sdust.web.service.ICourseService;
import com.sdust.web.util.PageInfo;

import java.util.List;

public class CourseServiceImpl implements ICourseService {
    private ICourseDao courseDao = new CourseDaoImpl();

    @Override
    public List<Course> selectAll() {
        //return courseDao.selectAll();
        List<Course> courseList = courseDao.selectAll();
        for (Course course : courseList) {
            course.setName(course.getName());
        }
        return courseList;
    }

    @Override
    public void deleteById(Integer id) {
        courseDao.deleteById(id);
    }

    @Override
    public void add(Course course) {
        courseDao.add(course);
    }

    @Override
    public Course selectById(Integer id) {
        return courseDao.selectById(id);
    }

    @Override
    public void update(Course course) {
        courseDao.update(course);
    }

    @Override
    public PageInfo<Course> selectByPage(Integer pageNo, Integer pageSize) {
        //Service业务层为了完成PageInfo的封装，需要调用两次Dao操作
        //1.查找当前页数据
        int offset = (pageNo - 1) * pageSize;
        List<Course> list = courseDao.selectByPage(offset, pageSize);
        //2.总的页数  查总的数量 Math.ceil(总数量/pageSize)
        int totalCount = courseDao.selectTotalCount();
        int totalPage = (int)Math.ceil((double)totalCount / pageSize);

        PageInfo<Course> pageInfo = new PageInfo<Course>(list, totalPage, pageNo, pageSize);
        return pageInfo;
    }
}
