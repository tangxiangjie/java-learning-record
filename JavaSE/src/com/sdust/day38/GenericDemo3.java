/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/5/30 下午1:26
 * Modified date: 2024/5/30 下午1:26
 */

package com.sdust.day38;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class GenericDemo3 {
    public void add(List<?> list) {//? 通配符
        // 既要限定集合类型，又不确定里边是什么类型
        // 主要用来遍历
        // list.add(1);//Required type: capture of ? | Provided:int
        for (Object object : list) {
            System.out.println(object);
        }
    }

    public void add1(List list) {//? 通配符
        // 既要限定集合类型，又不确定里边是什么类型
        // 主要用来遍历
        list.add(1);//Required type: capture of ? | Provided:int
        for (Object object : list) {
            System.out.println(object);
        }
    }

    public void add2(List<? extends Number> list) {//extends      元素的类型必须继承自指定的类

    }

    public void add3(List<? super String> list) {//super        元素的类型必须是指定的类的父类.自身也可以

    }

    @Test
    public void test1() throws Exception {
        List<Double> list1 = new ArrayList<Double>();
        List<Float> list2 = new ArrayList<Float>();
        List<Integer> list3 = new ArrayList<Integer>();
        List<String> list4 = new ArrayList<String>();
        add(list1);
        add(list2);
        add(list3);
        add(list4);

        add1(list1);
        add1(list2);
        add1(list3);
        add1(list4);

        add2(list1);
        add2(list2);
        add2(list3);
        // add2(list4);//Required type: List <? extends Number> Provided: List <String>

        // add3(list1);
        // add3(list2);
        // add3(list3);
        add3(list4);
        List<Object> object = new ArrayList<>();
        add3(object);
    }
}
