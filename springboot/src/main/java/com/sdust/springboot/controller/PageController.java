/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/11 下午9:00
 * Modified date: 2024/6/11 下午9:00
 */

package com.sdust.springboot.controller;

import jakarta.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/page")
public class PageController {

    /**
     * /page/user/add   /page/user/login
     *
     * @return
     */
    @RequestMapping("/**")
    public String path(HttpServletRequest request) {
        String requestURI = request.getRequestURI();
        System.out.println("requestURI: " + requestURI);
        String[] paths = requestURI.split("/");
        //["","page","user","list"]
        //["","page","login"]
        if (paths.length == 4) {
            return paths[2] + "_" + paths[3];
        } else if (paths.length == 3) {
            return paths[2];
        } else {
            return "failed";
        }
    }
}
