/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/4/7 上午11:17 ~ 2024/4/8 下午4:29
 * Modified date: 2024/4/8 下午4:29
 */

package com.sdust.web.dao;

import com.sdust.web.pojo.entity.Student;
import com.sdust.web.pojo.vo.StudentBanjiVO;

import java.util.List;

//接口：定义一个标准
public interface IStudentDao {
    public abstract List<StudentBanjiVO> selectAll();
    void deleteById(Integer id);
    void add(Student student);//没有id
    Student selectById(Integer id);
    void update(Student student);//有id

    List<StudentBanjiVO> selectByPage(int offset, Integer pageSize);

    int selectTotalCount();
}
