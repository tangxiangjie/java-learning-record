/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/3/20 下午2:33 ~ 2024/4/12 下午8:02
 * Modified date: 2024/4/12 下午8:02
 */

package com.sdust.day7.cal;

import org.junit.Test;

public class CalDemo {
    public static void main(String[] args) {
        Add add = new Add(3, 6);
        int result1 = add.getRes();
        System.out.println(result1);

        Sub sub = new Sub();
        sub.setNum1(4);
        sub.setNum2(9);
        int result2 = sub.getRes();
        System.out.println(result2);

        Div div = new Div(3, 5);
        int result3 = div.getRes();
        System.out.println(result3);
    }

    @Test
    public void test2() {
        //声明成子类型，new子类对象
        Add add = new Add(3, 2);
        //如果子类重写了getRes方法，那么调用的就是重写后的方法
        //如果子类没重写getRes方法，那么调用的就是从父类继承的方法
        int res1 = add.getRes();
        System.out.println(res1);

        //声明成父类类型，new子类对象
        Cal cal = new Add(3, 2);//①多态②继承③声明成父类类型，new子类类型
        //Person person = new Student();
        //person = new Teacher();
        //cal.onlyAdd();
        int res2 = cal.getRes();
        System.out.println(res2);

        cal = new Sub();
        cal.setNum1(3);
        cal.setNum2(2);
        int res3 = cal.getRes();
        System.out.println(res3);
    }

    public void jiSuan(Cal cal) {//多态
        cal.getRes();
    }
    //这一个就顶下边四个，写一个顶写四个，可以接受任意子类的传入
    //思想：面向接口编程    狭义：后边要讲的接口   广义：声明成父类。那么所有子类对象都可以传递过来
    //广义：声明成父类，指向子类对象
    //六、接口interface

    public void jiSuan(Add add) {
        add.getRes();
    }

    public void jiSuan(Sub sub) {
        sub.getRes();
    }

    public void jiSuan(Mul mul) {
        mul.getRes();
    }

    public void jiSuan(Div div) {
        div.getRes();
    }
}
