/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/6 上午12:39
 * Modified date: 2024/6/6 上午12:39
 */

package com.sdust.spring.controller;

import com.sdust.spring.service.IBanjiService;
import com.sdust.spring.service.impl.BanjiServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;

import javax.annotation.Resource;

@Controller
public class BanjiController {
    // @Resource(name = "banjiServiceImpl")
    //@Autowired
    //@Qualifier(value = "banjiServiceImpl2")
    //private IBanjiService banjiService;
    //private IBanjiService banjiServiceImpl;//此时变量名有含义了   和bean一致 会匹配注入
    @Autowired
    public IBanjiService banjiService;

    public void selectAll() {
        System.out.println("BanjiController.selectAll");

        banjiService.selectAll();
        //banjiServiceImpl.selectAll();
    }
}
