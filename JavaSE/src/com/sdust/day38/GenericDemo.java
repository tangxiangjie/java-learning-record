/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/5/30 上午9:29
 * Modified date: 2024/5/30 上午9:29
 */

package com.sdust.day38;

import com.sdust.day37.Student;
import com.sdust.day37.Teacher;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class GenericDemo {

    @Test
    public void test1() {
        List list = new ArrayList();
        list.add("ZhangSan");
        list.add(1);
        //集合使用 取出元素
        Object object = list.get(0);
        System.out.println(object);
        String str1 = (String) list.get(0);
        System.out.println(str1);
        //String str2 = (String) list.get(1);//ClassCastException
    }

    //使用泛型
    @Test
    public void test2() {
        //声明泛型集合的时候指定元素的类型
        List<String> list = new ArrayList<>();
        list.add("Java");
        //泛型解决的的是编译时期的报错，提前检查
        //list.add(1);//会报错
        String str = list.get(0);
        System.out.println(str);
    }

    @Test
    public void test3() {
        // 两端的数据类型必须要一致
        List<Object> list1 = new ArrayList<Object>();
        List<String> list2 = new ArrayList<String>();
        // 右侧的泛型可以不写
        List<String> list3 = new ArrayList<>();
        // 只在右侧写泛型不起作用
        List list4 = new ArrayList<String>();
        list4.add(1);

        // 两边不一致编译时候报错
        // List<Object> list5 = new ArrayList<String>();
        // 泛型类型必须为引用数据类型
        // List<int> list6 = new ArrayList<>();
    }

    @Test
    public void test5() {
        List<String> list1 = new ArrayList<>();
        List<Integer> list2 = new ArrayList<>();
        Class clazz1 = list1.getClass();
        Class clazz2 = list2.getClass();
        System.out.println(clazz1 == clazz2);
        System.out.println(clazz1);
        System.out.println(clazz2);
    }

    //泛型擦除:参数都是List list   认为是同一个方法
    /*public void add(List<Student> list) {

    }
    public void add(List<Teacher> list) {

    }*/
    //'add(List<Student>)' clashes with 'add(List<Teacher>)'; both methods have same erasure

}
