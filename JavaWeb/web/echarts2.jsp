<%--
  ~ 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
  ~ Copyright @TangXJ
  ~ Created by TangXJ
  ~ Created&Used date: 2024/5/25 下午3:39
  ~ Modified date: 2024/5/25 下午3:39
  --%>

<%--
  Created by IntelliJ IDEA.
  User: TangXJ
  Date: 2024/5/25
  Time: 15:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html style="height: 100%">
<head>
    <meta charset="utf-8">
    <script src="https://cdn.jsdelivr.net/npm/echarts/dist/echarts.min.js"></script>
    <style>
        .number-display {
            font-size: 48px;
            font-weight: bold;
            color: orange;
        }
    </style>
</head>
<body style="height: 100%; margin: 0">
<div id="main" style="height: 100%"></div>
<script type="text/javascript">
    // 初始化 ECharts 实例
    var myChart = echarts.init(document.getElementById('main'));

    // 配置选项
    var option = {
        title: {
            text: '门诊人次情况',
            left: 'center'
        },
        tooltip: {},
        xAxis: {
            type: 'category',
            data: ['门诊人次', '急诊人次', '住院人次', '出院人次']
        },
        yAxis: {
            type: 'value'
        },
        series: [{
            type: 'bar',
            data: [3210, 1230, 123, 123],
            itemStyle: {
                normal: {
                    color: function(params) {
                        var colorList = ['#c23531','#2f4554','#61a0a8','#d48265'];
                        return colorList[params.dataIndex];
                    }
                }
            }
        }]
    };

    // 使用配置项生成图表
    myChart.setOption(option);

    // 在图表上叠加自定义的 HTML/CSS
    myChart.on('finished', function () {
        var dom = myChart.getDom();
        var data = [3210, 1230, 123, 123];
        var categories = ['今日门诊人次:', '今日急诊人次:', '今日住院人次:', '今日出院人次:'];
        var colors = ['#c23531','#2f4554','#61a0a8','#d48265'];
        for (var i = 0; i < data.length; i++) {
            var div = document.createElement('div');
            div.className = 'number-display';
            div.style.color = colors[i];
            div.innerHTML = categories[i] + ' ' + data[i];
            div.style.position = 'absolute';
            div.style.left = (100 + i * 100) + 'px'; // 根据需求调整位置
            div.style.top = '50px'; // 根据需求调整位置
            dom.appendChild(div);
        }
    });
</script>
</body>
</html>
