package com.sdust.springboot;

import com.sdust.springboot.mapper.UserMapper;
import com.sdust.springboot.pojo.entity.User;
import com.sdust.springboot.service.IUserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class SpringbootApplicationTests {

    @Test
    void contextLoads() {
    }


    @Autowired
    private UserMapper userMapper;

    @Autowired
    private IUserService userService;

    //SpringBoot单元测试
    @Test
    public void test1() {
        List<User> userList = userMapper.selectAll();
        for (User user : userList) {
            System.out.println(user);
        }
    }

}
