/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/13 下午3:41
 * Modified date: 2024/6/13 下午3:41
 */

package com.sdust.springboot.service.impl;

import com.sdust.springboot.mapper.OperLogMapper;
import com.sdust.springboot.pojo.entity.OperLog;
import com.sdust.springboot.service.IOperLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OperLogServiceImpl implements IOperLogService {
    @Autowired
    private OperLogMapper operLogMapper;

    @Override
    public void add(OperLog operLog) {
        System.out.println(operLog);
        operLogMapper.add(operLog);
    }
}
