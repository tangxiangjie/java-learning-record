/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/3/26 下午3:31 ~ 2024/3/26 下午3:34
 * Modified date: 2024/3/26 下午3:34
 */

package com.sdust.day11;

public class MeiQianException extends Exception{

    //message 代表抛出异常之后打印的异常信息
    public MeiQianException(String message) {//要打印的信息
        super(message);//new Exception(message)
    }

}
