/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/4/2 下午8:22 ~ 2024/4/9 下午1:49
 * Modified date: 2024/4/9 下午1:49
 */

package com.sdust.web.controller;

import com.sdust.web.pojo.entity.Teacher;
import com.sdust.web.service.ITeacherService;
import com.sdust.web.service.impl.TeacherServiceImpl;
import com.sdust.web.util.PageInfo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/teacher")
public class TeacherServlet extends HttpServlet {
    //private IStudentDao studentDao = new StudentDaoImpl();//声明成接口 new 实现类
    //private ITeacherDao teacherDao = new TeacherDaoImpl();
    private ITeacherService teacherService = new TeacherServiceImpl();

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //System.out.println("TeacherServlet.service");
        req.setCharacterEncoding("UTF-8");//解决post请求乱码问题   method="post"

        String method = req.getParameter("method");
        if (method == null || method.equals("")) {
            method = "selectByPage";
        }
        switch (method) {
            case "selectAll":
                selectAll(req, resp);
                break;
            case "selectByPage":
                selectByPage(req, resp);
                break;
            case "deleteById":
                deleteById(req, resp);
                break;
            case "add":
                add(req, resp);
                break;
            case "toUpdate":
                toUpdate(req, resp);
                break;
            case "update":
                update(req, resp);
                break;
        }

    }

    private void selectByPage(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("TeacherServlet.selectByPage");

        String pageNo = req.getParameter("pageNo");
        String pageSize = req.getParameter("pageSize");
        if (pageNo == null || pageNo.equals("")) {
            pageNo = "1";
        }
        if (pageSize == null || pageSize.equals("")) {
            pageSize = "5";
        }

        PageInfo<Teacher> pageInfo = teacherService.selectByPage(Integer.parseInt(pageNo), Integer.parseInt(pageSize));
        req.setAttribute("pageInfo", pageInfo);
        req.getRequestDispatcher("teacher_list2.jsp").forward(req, resp);
    }

    private void update(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        System.out.println("TeacherServlet.update");

        String id = req.getParameter("id");
        String name = req.getParameter("name");
        String age = req.getParameter("age");
        String gender = req.getParameter("gender");

        Teacher teacher = new Teacher(Integer.parseInt(id), name, Integer.parseInt(age), gender, 0.0);

        teacherService.update(teacher);
        //重定向（让servle执行完之后 自己发送请求）302 服务器告诉浏览器去请求这个地址
        resp.sendRedirect("/teacher?method=selectByPage");
    }

    private void toUpdate(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("TeacherServlet.toUpdate");

        String queryId = req.getParameter("id");
        Teacher teacher = teacherService.selectById(Integer.parseInt(queryId));

        //把数据放到req里面
        req.setAttribute("teacher", teacher);
        //转发到teacher_list.jsp页面进行展示
        req.getRequestDispatcher("teacher_update.jsp").forward(req, resp);
    }

    private void add(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        System.out.println("TeacherServlet.add");

        String name = req.getParameter("name");
        String age = req.getParameter("age");
        String gender = req.getParameter("gender");
        Teacher teacher = new Teacher();
        teacher.setName(name);
        teacher.setAge(Integer.parseInt(age));
        teacher.setGender(gender);

        teacherService.add(teacher);

        resp.sendRedirect("/teacher?method=selectByPage");
    }

    private void deleteById(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        System.out.println("TeacherServlet.deleteById");
        String id = req.getParameter("id");

        teacherService.deleteById(Integer.parseInt(id));

        resp.sendRedirect("/teacher");
    }

    private void selectAll(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("TeacherServlet.selectAll");

        List<Teacher> teacherList = teacherService.selectAll();

        //把list数据放到req里面
        req.setAttribute("teacherList", teacherList);
        //转发到teacher_list.jsp页面进行展示
        req.getRequestDispatcher("teacher_list.jsp").forward(req, resp);
    }
}
