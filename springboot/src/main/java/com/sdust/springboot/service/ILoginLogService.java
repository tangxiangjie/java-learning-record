/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/13 下午9:55
 * Modified date: 2024/6/13 下午9:55
 */

package com.sdust.springboot.service;

import com.sdust.springboot.pojo.entity.LoginLog;
import org.springframework.stereotype.Service;

public interface ILoginLogService {
    void add(LoginLog loginLog);
}
