/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/13 下午9:51
 * Modified date: 2024/6/13 下午9:51
 */

package com.sdust.springboot.pojo.entity;

import lombok.Data;

import java.util.Date;

@Data
public class LoginLog {
    private Integer id;
    private Integer userId;
    private String userName;
    private String ip;
    private String msg;
    private Date accessTime;
    private Integer status;
    private Integer deleted;
    private Date createTime;
    private Date updateTime;
}
