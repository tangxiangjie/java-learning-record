/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/3/22 下午7:45 ~ 2024/3/22 下午8:03
 * Modified date: 2024/3/22 下午8:03
 */

package com.sdust.day9.duotaidemo;

//子类Square
public class Square extends Shape{
    private String areaFormula;

    public Square() {
    }

    public Square(String name, String areaFormula) {
        super(name);
        this.areaFormula = areaFormula;
    }

    @Override
    public void show() {
        System.out.println("Square.show");
        System.out.println("I am " + super.name);
        System.out.println("My area formula is" + areaFormula);
    }

    @Override
    public String toString() {
        return "Square{" +
                "areaFormula='" + areaFormula + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
