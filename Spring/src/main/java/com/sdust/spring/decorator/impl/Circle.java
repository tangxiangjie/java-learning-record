/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/11 上午10:59 ~ 2024/6/11 上午11:18
 * Modified date: 2024/6/11 上午11:09
 */

package com.sdust.spring.decorator.impl;

import com.sdust.spring.decorator.Shape;

//Shape的具体实现类
public class Circle implements Shape {
    @Override
    public void draw() {
        System.out.println("Shape: Circle");
    }
}
