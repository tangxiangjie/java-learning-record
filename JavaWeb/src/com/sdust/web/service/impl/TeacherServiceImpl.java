/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/4/7 下午2:15 ~ 2024/4/9 下午1:44
 * Modified date: 2024/4/9 下午1:44
 */

package com.sdust.web.service.impl;

import com.sdust.web.dao.ITeacherDao;
import com.sdust.web.dao.impl.TeacherDaoImpl;
import com.sdust.web.pojo.entity.Teacher;
import com.sdust.web.service.ITeacherService;
import com.sdust.web.util.PageInfo;

import java.util.List;

public class TeacherServiceImpl implements ITeacherService {
    private ITeacherDao teacherDao = new TeacherDaoImpl();

    @Override
    public List<Teacher> selectAll() {
        return teacherDao.selectAll();
    }

    @Override
    public void deleteById(Integer id) {
        teacherDao.deleteById(id);
    }

    @Override
    public void add(Teacher teacher) {
        teacherDao.add(teacher);
    }

    @Override
    public Teacher selectById(Integer id) {
        return teacherDao.selectById(id);
    }

    @Override
    public void update(Teacher teacher) {
        teacherDao.update(teacher);
    }

    @Override
    public PageInfo<Teacher> selectByPage(Integer pageNo, Integer pageSize) {
        //1.
        int offset = (pageNo - 1) * pageSize;
        List<Teacher> teacherList = teacherDao.selectByPage(offset, pageSize);
        //2.
        int totalCount = teacherDao.selectTotalCounnt();
        int totalPage = (int) Math.ceil((double)totalCount / pageSize);

        PageInfo<Teacher> pageInfo = new PageInfo<>(teacherList, totalPage, pageNo, pageSize);
        return pageInfo;
    }
}
