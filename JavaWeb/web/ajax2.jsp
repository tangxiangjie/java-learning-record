<%--
  ~ 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
  ~ Copyright @TangXJ
  ~ Created by TangXJ
  ~ Created&Used date: 2024/4/12 上午11:15 ~ 2024/4/13 下午4:48
  ~ Modified date: 2024/4/13 下午4:48
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>

用户名：<input type="text" name="name" id="name"/><span id="nameSpan"></span><br/>
密码：<input type="text" name="password" id="password"/><br/>
<input type="button" value="登录">

<script src="/static/jquery-2.1.4.js" type="text/javascript" charset="utf-8"></script>
<script>
    $(function() {
        $('#name').blur(function (){
            $.post(
                '/ajax2',
                {'name':$(this).val()},
                function (jsonObj) {//callback
                    console.log(jsonObj);
                    // {"exist":true, "msg":"已存在"}
                    // {"exist":false, "msg":"OK"}
                    if (jsonObj.exist) {
                        $('#nameSpan').html(jsonObj.msg);
                        $('#nameSpan').css('color','red');
                    } else {
                        $('#nameSpan').html(jsonObj.msg);
                        $('#nameSpan').css('color','green');
                    }
                },
                'json'
            )
        });
    });
</script>

</body>
</html>
