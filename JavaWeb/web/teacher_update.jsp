<%@ page import="com.sdust.web.pojo.entity.Student" %>
<%@ page import="com.sdust.web.pojo.entity.Teacher" %><%--
  ~ 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
  ~ Copyright @TangXJ
  ~ Created by TangXJ
  ~ Created&Used date: 2024/4/7 下午2:36 ~ 2024/4/8 下午3:12
  ~ Modified date: 2024/4/8 下午3:12
  --%>

<%--
  Created by IntelliJ IDEA.
  User: TangXJ
  Date: 2024/4/7
  Time: 14:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
    <%
        //JSP脚本，这里面可以任意写java代码
        //request、response:JSP内置对象
        Teacher teacher = (Teacher) request.getAttribute("teacher");
    %>

    <%--<form action="/student?method=update&id=<%=student.getId()%>" method="post">--%><%--默认就是get ?method=就是get方法传参  会被覆盖  所以form表单一般不用--%>
    <form action="/teacher?method=update" method="post" onsubmit="return update_confirm()"><%--默认就是get ?method=就是get方法传参  会被覆盖  所以form表单一般不用--%>
        <input type="hidden" name="id" value="<%=teacher.getId()%>"/>
        <%--<input type="hidden" name="id" value="<%=student.getId()%>"/>--%><%--readonly 可以删掉F12！！ 以后学后端验证--%>
        姓名:<input type="text" name="name" value="<%=teacher.getName()%>"><br>
        年龄:<input type="text" name="age" value="<%=teacher.getAge()%>"><br>
        性别:<input type="text" name="gender" value="<%=teacher.getGender()%>"><br>
        <input type="submit" value="更新">
    </form>

    <script>
        function update_confirm(){
            return confirm("您确认要更新吗？");
        }
    </script>
</body>
</html>
