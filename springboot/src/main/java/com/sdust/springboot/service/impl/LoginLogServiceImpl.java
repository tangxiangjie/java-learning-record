/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/13 下午9:56
 * Modified date: 2024/6/13 下午9:56
 */

package com.sdust.springboot.service.impl;

import com.sdust.springboot.pojo.entity.LoginLog;
import com.sdust.springboot.service.ILoginLogService;
import org.springframework.stereotype.Service;

@Service
public class LoginLogServiceImpl implements ILoginLogService {
    @Override
    public void add(LoginLog loginLog) {
        System.out.println(loginLog);
    }
}
