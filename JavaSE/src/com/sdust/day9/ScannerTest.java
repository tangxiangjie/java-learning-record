/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/3/23 下午9:39 ~ 2024/3/25 上午9:01
 * Modified date: 2024/3/25 上午9:01
 */

package com.sdust.day9;

import org.junit.Test;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ScannerTest {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        try {
            System.out.println("请输入一个整数：");
            int number = scanner.nextInt();
            //scanner.next();
            System.out.println("您输入的整数是：" + number);
        } catch (InputMismatchException e) {
            System.out.println("输入不符合预期！");
        }

        // 不清空输入缓冲区
        try {
            System.out.println("请再次输入一个整数：");
            int number = scanner.nextInt(); // 尝试读取输入，但由于输入缓冲区中仍然包含非数字字符，会继续抛出异常
            System.out.println("您输入的整数是：" + number);
        } catch (InputMismatchException e) {
            System.out.println("再次输入不符合预期！");
        }

        scanner.close();
    }

    @Test
    public void test1() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("请输入两个整数：");
        int num1 = scanner.nextInt();
        int num2 = scanner.nextInt();

        scanner.next();
        System.out.println("请输入一个字符串：");
        String str = scanner.nextLine();

        System.out.println("您输入的两个整数是：" + num1 + " 和 " + num2);
        System.out.println("您输入的字符串是：" + str);

        scanner.close();
    }

}
