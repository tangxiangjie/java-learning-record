/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/12 下午3:26
 * Modified date: 2024/6/12 下午3:26
 */

package com.sdust.springboot.controller;

import com.sdust.springboot.utils.Result;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

@RestController
@RequestMapping("/upload")
public class UploadController {

    // MultipartFile封装了所有图片上传信息
    @RequestMapping("/uploadImage")
    public Result upload(MultipartFile file) {
        String uuid = UUID.randomUUID().toString().replace("-", "");
        //a.png
        String filename = file.getOriginalFilename();
        //.png
        String extension = filename.substring(filename.lastIndexOf("."));
        //f7a9f3e6805a4e81b5d27245c6c30070.png
        String newFilename = uuid + extension;
        String filePath = "D:\\mypic\\" + newFilename;
        try {
            file.transferTo(new File(filePath));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return Result.ok("上传成功", newFilename);
    }
}
