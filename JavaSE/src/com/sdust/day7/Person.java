/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/3/20 上午11:30 ~ 2024/3/21 下午10:15
 * Modified date: 2024/3/21 下午10:15
 */

package com.sdust.day7;

import org.junit.Test;

//一个类不继承任何一个类，默认继承Object
public class Person /*extends Object*/{
    //private只在类的内部访问，子类也不行
    protected int id;
    protected String name = "Ren";
    public int age;
    //protected String className;

    public Person() {
        //Java里面new子类对象时候首先new父类对象
        super();//new Object();
    }

    public Person(int id, String name, int age) {
        super();//new Object();
//        id = id; //就近原则
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public void show() {
        System.out.println("Person.show");
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    @Test
    public void test() {
        System.out.println("1111");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
