/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/12 下午8:35
 * Modified date: 2024/6/12 下午8:35
 */

package com.sdust.springboot;

import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

// @SpringBootTest
public class DateTest {

    @Test
    public void testDate2String() {
        Date date = new Date();
        System.out.println(date);//Wed Jun 12 20:42:11 CST 2024
        //SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd F E HH:mm:ss:SSS");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy年MM月dd日 第F周 E HH:mm:ss:SSS");
        //SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyy/MM/dd F E HH:mm:ss:SSS");
        String newDate = simpleDateFormat.format(date);
        System.out.println(newDate);//2024-06-12 20:42:11:206
        //2024-06-12 2 周三 20:43:45:435
    }

    @Test
    public void testString2Date() throws ParseException {
        String date = "2024-06-12 20:42:11:206";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
        Date newDate = simpleDateFormat.parse(date);
        System.out.println(newDate);
    }

}
