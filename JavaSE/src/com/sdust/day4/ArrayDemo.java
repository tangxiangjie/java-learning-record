package com.sdust.day4;

import org.junit.Test;

public class ArrayDemo {
    @Test
    public void test1() {
        int []arr = new int[6];
        //数组下标: 数组名[下标]
        arr[0] = 23;
        arr[1] = 23;
        //new出来的数组 有属性 array.length
        System.out.println(arr.length);//6
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
        for (int i = arr.length - 1; i >= 0; i--) {
            System.out.println(arr[i]);
        }
    }

    //定义的时候直接赋值
    @Test
    public void test2() {
        int arr1[] = {1, 2, 3, 4};
        System.out.println(arr1.length);
        int arr2[] = new int[]{1, 2, 3, 4};
        System.out.println(arr2.length);
        //System.out.println(arr1[50]);
    }

    @Test
    public void test3() {
        int arr1[] = {1, 2, 3, 4};
        System.out.println(arr1.length);
        //数组下标越界异常
        System.out.println(arr1[50]);
    }

    @Test
    public void test4() {
        int arr1[] = {1, 9, 3, 4};
        //array.fori
        //int max = arr1[0];
        int min = arr1[0];
        /*for (int i = 0; i < arr1.length; i++) {
            max = (arr1[i] > max) ? arr1[i] : max;
        }*/
        //方法调用: 方法名(参数)
        //debug: 可以在函数里边加断点 省的点step into了, 前提是知道函数在哪个位置
        int max = getMax(arr1);

        for (int i = 0; i < arr1.length; i++) {
            min = (arr1[i] < min) ? arr1[i] : min;
        }
        System.out.println("max: " + max);
        System.out.println("min: " + min);
        for (int i = 0; i < arr1.length; i++) {
            
        }
    }

    /** /**+enter
     *求最大值
     * @param arr  要求最大值的数组
     * @return 返回数组最大值
     */
    public int getMax(int[] arr) {
        int max = arr[0];
        for (int i = 0; i < arr.length; i++) {
            max = (arr[i] > max) ? arr[i] : max;
        }
        return max;
    }

    //bubble sort
    @Test
    public void testBubble() {
        int[] arr = new int[] {45, 70, 35, 99, 6, 3};
        //这里老师说建议i从1开始, 他说的理由是符合正常逻辑, 但也间接解决了可能存在的数组越界问题, 没讲到这个知识点
        bubbleSort(arr);
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
    }

    //冒泡排序
    private void bubbleSort(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length - 1 - i; j++) {
                if (arr[j] > arr[j + 1]) {
                    int t = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = t;
                }
            }
        }
    }

    public void toCharArrayy() {
        String str = "absda";
        //
        char []arr = str.toCharArray();
        //next///nextLine
    }
}
