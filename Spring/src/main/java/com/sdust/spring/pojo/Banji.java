/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/5 上午12:18
 * Modified date: 2024/6/5 上午12:18
 */

package com.sdust.spring.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
/*@AllArgsConstructor
@NoArgsConstructor*/
public class Banji {
    private Integer id;
    private String name;

    public Banji() {
        System.out.println("Banji.Banji NoArgsConstructor");
    }

    public Banji(Integer id, String name) {
        System.out.println("Banji.Banji AllArgsConstructor");
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        System.out.println("Banji.getId");
        return id;
    }

    public void setId(Integer id) {
        System.out.println("Banji.setId");
        this.id = id;
    }

    public String getName() {
        System.out.println("Banji.getName");
        return name;
    }

    public void setName(String name) {
        System.out.println("Banji.setName");
        this.name = name;
    }
}
