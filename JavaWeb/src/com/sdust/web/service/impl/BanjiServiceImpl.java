/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/4/8 下午3:46 ~ 2024/4/17 上午9:22
 * Modified date: 2024/4/17 上午9:22
 */

package com.sdust.web.service.impl;

import com.sdust.web.dao.IBanjiDao;
import com.sdust.web.dao.impl.BanjiDaoImpl;
import com.sdust.web.pojo.entity.Banji;
import com.sdust.web.pojo.query.BanjiQuery;
import com.sdust.web.pojo.vo.BanjiCountVO;
import com.sdust.web.service.IBanjiService;
import com.sdust.web.util.LayUITableResult;
import com.sdust.web.util.PageInfo;

import java.util.List;

public class BanjiServiceImpl implements IBanjiService {
    IBanjiDao banjiDao = new BanjiDaoImpl();
    @Override
    public List<Banji> selectAll() {
        return banjiDao.selectAll();
    }

    @Override
    public PageInfo<Banji> selectByPage(Integer pageNo, Integer pageSize) {
        //1.
        int offset = (pageNo - 1) * pageSize;
        List<Banji> banjiList = banjiDao.selectByPage(offset, pageSize);
        //2.
        int totalCount = banjiDao.selcetTotalCount();
        int totaoPage = (int) Math.ceil((double)totalCount / pageSize);

        PageInfo<Banji> pageInfo = new PageInfo<Banji>(banjiList, totaoPage, pageNo, pageSize);
        return pageInfo;
    }

    @Override
    public LayUITableResult selectByPage2(Integer pageNo, Integer pageSize) {
        //1.
        int offset = (pageNo - 1) * pageSize;
        List<Banji> banjiList = banjiDao.selectByPage(offset, pageSize);
        //2.
        int totalCount = banjiDao.selcetTotalCount();
        //int totaoPage = (int) Math.ceil((double)totalCount / pageSize);//不需要啦 自己会算的

//        LayUITableResult layUITableResult = new LayUITableResult();
//        layUITableResult.setCode(0);
//        layUITableResult.setCount(totalCount);
//        layUITableResult.setData(banjiList);

        return LayUITableResult.ok(totalCount, banjiList);
    }

   /*@Override
    public LayUITableResult selectByPage3(BanjiQuery banjiQuery) {
        int offset = (banjiQuery.getPage() - 1) * banjiQuery.getLimit();
        List<Banji> banjiList = banjiDao.selectByPage(offset, banjiQuery.getLimit());
        int totalCount = banjiDao.selcetTotalCount();

        return LayUITableResult.ok(totalCount, banjiList);
    }*/

    @Override
    public LayUITableResult selectByPage3(BanjiQuery banjiQuery) {
        List<Banji> banjiList = banjiDao.selectByPage(banjiQuery);

        int totalCount = banjiDao.selcetTotalCount(banjiQuery);
        return LayUITableResult.ok(totalCount, banjiList);
    }

    @Override
    public void add(Banji banji) {
        banjiDao.add(banji);
    }

    @Override
    public List<BanjiCountVO> selectBanjiCount() {
        return banjiDao.selectBanjiCount();
    }

    @Override
    public void deleteById(int id) {
        banjiDao.deleteById(id);
    }

    @Override
    public void deleteSelected(String[] ids) {
        //delete from banji where id in(2, 4, 5);
        /*for (String id : ids) {
            banjiDao.deleteById(Integer.parseInt(id));//会频繁地调用数据库连接、断开
        }*/
        banjiDao.deleteSelected(ids);
    }

    @Override
    public Banji selectById(int id) {
        return banjiDao.selectById(id);
    }

    @Override
    public void updateForm(Banji banji) {
        banjiDao.updateForm(banji);
    }

}
