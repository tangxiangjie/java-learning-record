/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/3/20 下午10:05 ~ 2024/3/20 下午10:50
 * Modified date: 2024/3/20 下午10:50
 */

package com.sdust.day6;

public class Teacher {
    //属性、字段
    //属性私有private,只有类的内部可以访问
    private int id;
    private String name;
    private int age;
    private double salary;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    //set方法: setAge() setName()
    public void setAge(int age) {
        //Variable 'age' is assigned to itself
        //就近原则
        //age = age;
        //this代表当前对象
        if (age >= 0 && age <= 120) {
            this.age = age;
        }
    }

    //get方法: getAge()  getName()
    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", salary=" + salary +
                '}';
    }
}
