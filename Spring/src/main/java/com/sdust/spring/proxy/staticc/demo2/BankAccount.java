/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/7 上午1:11
 * Modified date: 2024/6/7 上午1:11
 */

package com.sdust.spring.proxy.staticc.demo2;

public interface BankAccount {
    void withdraw(double amount);
}
