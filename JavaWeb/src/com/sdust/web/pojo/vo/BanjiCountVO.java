/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/4/12 下午2:14 ~ 2024/4/12 下午2:15
 * Modified date: 2024/4/12 下午2:15
 */

package com.sdust.web.pojo.vo;

public class BanjiCountVO {
    //班级名字
    private String name;
    //班级人数
    private Integer value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BanjiCountVO{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
