/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/4/1 下午10:50
 * Modified date: 2024/4/1 下午10:50
 */

package com.sdust.jdbcuni;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;

public class JDBCUtil2 {
    public static int updateSQL(String sql){

        Connection connection = null;
        PreparedStatement statement = null;
        int resultSet ;
        try {
            connection = JDBCUtil.getConnection();
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }finally {
            {
                JDBCUtil.close(connection,statement,null);
            }
        }

        return resultSet;
    }
    public static ArrayList<Object> querySQL(String sql) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = JDBCUtil.getConnection();
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();

            Properties properties = new Properties();
            FileInputStream fileInputStream = new FileInputStream("resources\\db.properties");
            properties.load(fileInputStream);
            fileInputStream.close();

            String className = (String) properties.get("objectName");
            Class<?> clazz = Class.forName(className);
            Constructor con = clazz.getDeclaredConstructor();
            Object o = con.newInstance();
            System.out.println(o);

            ArrayList<Object> object = new ArrayList<>();


            Field[] fields = clazz.getDeclaredFields();
            while (resultSet.next()) {
                Object newObj = clazz.getDeclaredConstructor().newInstance(); // 创建新对象实例
                for (Field field : fields) {
                    field.setAccessible(true);
                    String name = field.getName();
                    Object value = resultSet.getObject(name); // 获取字段值
                    field.set(newObj, value); // 设置字段值到新对象中
                }
                object.add(newObj); // 将新对象添加到ArrayList中
            }
            return object;
        } catch (SQLException | InstantiationException | IllegalAccessException |
                 NoSuchMethodException | InvocationTargetException e) {
            throw new RuntimeException(e);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } finally {
            JDBCUtil.close(connection, statement, resultSet);
        }
    }
}
