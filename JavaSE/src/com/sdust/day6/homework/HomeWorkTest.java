/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/3/20 上午9:27 ~ 2024/3/21 下午5:18
 * Modified date: 2024/3/21 下午5:18
 */

package com.sdust.day6.homework;

import org.junit.Test;

public class HomeWorkTest {

    @Test
    public void testNumber() {
        Number number1 = new Number(1, 3);
        System.out.println(number1.add());
        System.out.println(number1.sub());
        System.out.println(number1.multiple());
        System.out.println(number1.divide());
    }

    @Test
    public void testRectangle() {
        Rectangle rectangle1 = new Rectangle(4,6);
        System.out.println(rectangle1.zhouChang());
        System.out.println(rectangle1.mianJi());

        Rectangle rectangle2 = new Rectangle(4);
        System.out.println(rectangle2.zhouChang());
        System.out.println(rectangle2.mianJi());
    }


}
