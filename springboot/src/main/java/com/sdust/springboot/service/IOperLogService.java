/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/13 下午3:40
 * Modified date: 2024/6/13 下午3:40
 */

package com.sdust.springboot.service;

import com.sdust.springboot.pojo.entity.OperLog;

public interface IOperLogService {
    void add(OperLog operLog);
}
