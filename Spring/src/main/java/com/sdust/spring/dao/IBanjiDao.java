/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/6 上午12:39 ~ 2024/6/6 上午12:40
 * Modified date: 2024/6/6 上午12:40
 */

package com.sdust.spring.dao;

public interface IBanjiDao {
    void selectAll();
}
