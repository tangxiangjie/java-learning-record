/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/3/22 下午7:42 ~ 2024/3/22 下午7:59
 * Modified date: 2024/3/22 下午7:59
 */

package com.sdust.day9.duotaidemo;

//父类Shape
public abstract class Shape {
    protected String name;

    public Shape() {
    }

    public Shape(String name) {
        this.name = name;
    }

    public abstract void show();
}
