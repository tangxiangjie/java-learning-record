/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/3/20 下午2:29 ~ 2024/3/25 上午9:12
 * Modified date: 2024/3/25 上午9:12
 */

package com.sdust.day7.cal;

public abstract class Cal {//抽象类就不能new了
    protected int num1;
    protected int num2;

    public Cal() {
    }

    public Cal(int num1, int num2) {
        this.num1 = num1;
        this.num2 = num2;
    }

    //抽象方法是没有方法体，如果这个类里面有抽象方法，这个类必须是抽象类。
    public abstract int getRes();

    //方法的签名
/*    public int getRes() {//大括号: 方法体
        //为了不让它报错
        return 0;
    }*/

    //抽象方法: 没有方法体
    //一个类 有抽象方法, 这个类必须变成抽象类

    public int getNum1() {
        return num1;
    }

    public void setNum1(int num1) {
        this.num1 = num1;
    }

    public int getNum2() {
        return num2;
    }

    public void setNum2(int num2) {
        this.num2 = num2;
    }
}
