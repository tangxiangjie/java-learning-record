/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/5/30 下午3:05
 * Modified date: 2024/5/30 下午3:05
 */

package com.sdust.day39;

import org.junit.Test;

import java.lang.reflect.Method;
import java.util.Arrays;

//@Author("男")
//@Author(value = "男 ")
//@Author(value = {"男","女"})
@Author(value = "男", name = "gao", age = 23)
public class AuthorUseDemo {

    @Author
    private String str;

    @Author(name = "zhangSan", age = 23, value = {"男", "女"})
    //@Author(name = "zhangSan")
    public void add() {

    }

    @Test
    public void test1() throws NoSuchMethodException {
        Class<AuthorUseDemo> clazz = AuthorUseDemo.class;
        Author typeAuthor = clazz.getAnnotation(Author.class);
        System.out.println(typeAuthor.value()[0]);

        //1.先获得代表这个方法的method
        Method add = clazz.getDeclaredMethod("add");
        //2.在method上拿到指定注解
        Author methodAnnotation = add.getAnnotation(Author.class);
        //3.
        System.out.println(methodAnnotation.name());//zhangSan
        System.out.println(methodAnnotation.age());//23
        System.out.println(Arrays.toString(methodAnnotation.value()));//[男, 女]

    }
}
