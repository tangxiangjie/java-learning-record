/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/3/21 上午9:43 ~ 2024/3/21 下午7:20
 * Modified date: 2024/3/21 下午7:20
 */

package com.sdust.day8.day7diffpackinherittest;

//接口就是一个纯粹的抽象类，接口就是一个标准
public interface IFly {
    //public static final double PI = 3.14;
    double PI = 3.14;

    //public void fly();
    void fly();
}
