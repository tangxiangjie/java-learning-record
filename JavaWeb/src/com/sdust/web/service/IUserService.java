/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/4/7 下午4:02 ~ 2024/4/17 下午5:18
 * Modified date: 2024/4/17 下午5:18
 */

package com.sdust.web.service;

import com.sdust.web.pojo.entity.User;
import com.sdust.web.util.PageInfo;

public interface IUserService {
    PageInfo<User> selectByPage(Integer pageNo, Integer pageSize);

    User login(String name, String password);

    void register(User user);

    boolean checkUser(String username);
}
