/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/2 下午11:59
 * Modified date: 2024/6/2 下午11:59
 */

package com.sdust.mybatis.test;

import com.sdust.mybatis.pojo.Banji;
import com.sdust.mybatis.pojo.Student;
import com.sdust.mybatis.utils.MyBatisUtils;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyBatisTest {

    @Test
    public void testSelectById() throws IOException {
        String resource = "mybatis.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        //创建SQLSessionFactory
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        //得到SQLSession
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //执行sql语句
        Student student = sqlSession.selectOne("student.selectById", 14);
        System.out.println(student);
    }

    @Test
    public void testUtilsSelectById() {
        //得到SQLSession
        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        //执行sql语句
        Student student = sqlSession.selectOne("student.selectById", 14);
        System.out.println(student);
    }

    @Test
    public void testSelectAll() {
        //得到SQLSession
        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        //执行sql语句
        List<Student> list = sqlSession.selectList("student.selectAll");
        for (Student student : list) {
            System.out.println(student);
        }
    }

    @Test
    public void testDeleteById() {
        // Setting autocommit to false on JDBC Connection
        // JDBC默认autocommit值是true，MyBatis把JDBC的autocommit设置为false，
        // 当执行delete的时候并没有真正提交到数据库，对于更新类的操作需要手动提交。
        // 在JDBC里面默认不需要用户手动提交因为autocommit 默认就是true，执行executeUpdate的时候直接修改了数据库
        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        int count = sqlSession.delete("student.deleteById", 29);
        System.out.println(count);
        //对于更新类的操作 需要手动提交
        sqlSession.commit();
        sqlSession.close();
    }

    @Test
    public void testAdd() {
        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        Student student = new Student(null, "田所333", 23, "男", 4, null);
        int count = sqlSession.insert("student.add", student);
        System.out.println(count);
        System.out.println(student);//添加到数据后，获得自增的id值并设置给Java对象
        //对于更新类的操作 需要手动提交
        sqlSession.commit();
        sqlSession.close();
    }

    @Test
    public void testUpdate() {
        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        Student student = new Student(22, "苗伟改了", 20, "男", 4, null);
        int count = sqlSession.update("student.update", student);//老师用的insert也可以实现
        System.out.println(count);
        //对于更新类的操作 需要手动提交
        sqlSession.commit();
        sqlSession.close();
    }

    @Test
    public void testSelectByPage() {
        int pageNo = 1;
        int pageSize = 10;
        int offset = (pageNo - 1) * pageSize;

        Map<String, Integer> pageInfo = new HashMap<>();//再次体会Map-->可以模拟一个类
        pageInfo.put("offset", offset);
        pageInfo.put("pageSize", pageSize);

        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        List<Student> list = sqlSession.selectList("student.selectByPage", pageInfo);
        for (Student student : list) {
            System.out.println(student);
        }
    }

    @Test
    public void testSelectCount() {
        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        Integer count = sqlSession.selectOne("student.selectCount");
        System.out.println(count);
        sqlSession.close();
    }

    @Test
    public void testSelectAllMap() {
        //得到SQLSession
        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        //执行sql语句
        List<Student> list = sqlSession.selectList("student.selectAllMap");
        for (Student student : list) {
            System.out.println(student);
        }
    }

    @Test
    public void testOne2One() {
        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        List<Student> list = sqlSession.selectList("student.selectStudentBanjiInfo");
        for (Student student : list) {
            System.out.println(student);
        }
    }

    @Test
    public void testOne2Many() {
        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        List<Banji> list = sqlSession.selectList("banji.selectBanjiStudentInfo");
        for (Banji banji : list) {
            System.out.println(banji);
            List<Student> students = banji.getStudentList();
            for (Student student : students) {
                System.out.println(student);
            }
        }
    }

    @Test
    public void testSelectByCondition() {
        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        Student selectCondition = new Student(null, null, 23, "男", null, null);
        List<Student> students = sqlSession.selectList("student.selectByCondition", selectCondition);
        for (Student student : students) {
            System.out.println(student);
        }
    }

    @Test
    public void testUpdateByCondition() {
        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        Student updateCondition1 = new Student(19, null, 114514, "男", null, null);
        Student updateCondition2 = new Student(20, "长十郎", null, "男", null, null);
        int count = sqlSession.update("student.updateByCondition", updateCondition2);
        //对于更新类的操作 需要手动提交
        sqlSession.commit();
        sqlSession.close();
        System.out.println(count);
    }

    @Test
    public void testDeleteInRange() {
        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        /*Integer[] deleteArray = {31,33,34};*/
        Integer[] array = {27, 28, 30};
        List<Integer> list = new ArrayList<>();
        list.add(27);
        list.add(28);
        //int count = sqlSession.update("student.deleteByArray", array);
        int count = sqlSession.update("student.deleteByList", list);
        //对于更新类的操作 需要手动提交
        /*sqlSession.commit();
        sqlSession.close();*/
        System.out.println(count);
    }

    @Test
    public void testSelectByCondition2() {
        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        Student selectCondition1 = new Student(null, null, null, null, null, null);
        Student selectCondition2 = new Student(null, null, 23, "男", null, null);
        List<Student> students = sqlSession.selectList("student.selectByCondition2", selectCondition2);
        for (Student student : students) {
            System.out.println(student);
        }
    }

    @Test
    public void testAddSelective() {
        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        Student addStudent1 = new Student(null, "add1", null, null, null, null);
        Student addStudent2 = new Student(null, "add2", 23, "男", null, null);
        Integer count = sqlSession.update("student.addSelective", addStudent1);
        // Integer count = sqlSession.update("student.addSelective", addStudent2);
        //对于更新类的操作 需要手动提交
        sqlSession.commit();
        sqlSession.close();
        System.out.println(count);
    }

    @Test
    public void testUpdateByTrim() {
        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        Student updateCondition1 = new Student(16, "Tom", 114514, "男", null, null);
        Student updateCondition2 = new Student(18, "长十郎", null, "男", null, null);
        int count = sqlSession.update("student.updateByTrim", updateCondition2);
        //对于更新类的操作 需要手动提交
        /*sqlSession.commit();
        sqlSession.close();*/
        System.out.println(count);
    }

    @Test
    public void testSelectByTrim() {
        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        Student selectCondition = new Student(null, "张", null, "男", null, null);
        List<Student> students = sqlSession.selectList("student.selectByTrim", selectCondition);
        for (Student student : students) {
            System.out.println(student);
        }
    }
}
