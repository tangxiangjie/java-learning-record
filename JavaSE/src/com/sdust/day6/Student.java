/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/3/20 下午10:05 ~ 2024/3/20 下午10:39
 * Modified date: 2024/3/20 下午10:39
 */

package com.sdust.day6;

//抽取张三、李四、王五这些所有学生的共同特征
//现实世界都是由很多对象组成的，基于对象的共同特征抽象出类。
//4、类是对象的模板，对象是类的具体实例。
//5、一个类可以创建多个对象：同一个类的不同对象，结构相同，数据不同。
public class Student {
    //属性、字段
    //属性私有private,只有类的内部可以访问
    int id;
    String name;
    int age;
    String gender;

    //方法
    public String getInfo() {
        //String str = "[Student: id="+id+",name="+name+",age="+age+",gender="+gender+"]";
        //return str;
        return "[Student: id="+id+",name="+name+",age="+age+",gender="+gender+"]";
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", gender='" + gender + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }


}
