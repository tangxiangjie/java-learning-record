/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/11 下午5:21
 * Modified date: 2024/6/11 下午5:21
 */

package com.sdust.springboot.controller;

import com.sdust.springboot.pojo.entity.Student;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.*;

@Controller
// @RequestMapping("/page")
public class IndexController {
    @RequestMapping("/tttxj")
    public String Index() {
        System.out.println("IndexController.Index");
        return "index";//index很特殊  写不写都可以访问到
        //默认就加好视图解析器了。"/templates" + ".html"
        //org.springframework.boot.autoconfigure.thymeleaf.ThymeleafProperties
    }

    @RequestMapping("/index")
    public String studentIndex(Model model) {
        model.addAttribute("message", "HelloWorld");

        Student student = new Student(1, "李四", 23, new Date());
        model.addAttribute("student", student);

        Student student1 = new Student(1, "张三1", 23, new Date());
        Student student2 = new Student(2, "张三2", 23, new Date());
        Student student3 = new Student(3, "张三3", 23, new Date());
        List<Student> list = new ArrayList<>();
        list.add(student1);
        list.add(student2);
        list.add(student3);
        model.addAttribute("list", list);
        return "index1";
    }

    @RequestMapping("/data")
    public String data(HttpServletRequest request, HttpSession session) {
        //补全了User类   把这部分先注释掉了
/*        Student student1 = new Student(1, "张三1", 23 , new Date());
        request.setAttribute("student1", student1);

        Student student2 = new Student(2, "张三2", 23, new Date());
        session.setAttribute("student2", student2);

        User user = new User(33, "zhangsan", "12345");
        session.setAttribute("user", user);
        User user2 = new User(23, "lisi", "12345");
        // session.setAttribute("user2", user2);

        Student student3 = new Student(3, "张三3", 23, new Date());
        ServletContext application = request.getServletContext();
        application.setAttribute("student3", student3);*/

        return "data";
    }

    @RequestMapping(value = "/util")
    public String set(Model model) {
        Set<String> names = new HashSet<String>() ;
        List<Integer> ids = new ArrayList<Integer>() ;
        for (int i = 0 ; i < 5 ; i ++) {
            names.add("boot-" + i) ;
            ids.add(i) ;
        }
        model.addAttribute("names", names) ;
        model.addAttribute("ids", ids) ;
        model.addAttribute("mydate", new Date()) ;
        return "util" ;
    }
}
