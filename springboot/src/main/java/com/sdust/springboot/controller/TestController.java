/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/12 上午11:26
 * Modified date: 2024/6/12 上午11:26
 */

package com.sdust.springboot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class TestController {

    // 同名controller优先
    @RequestMapping("/test")
    @ResponseBody
    public String test() {
        return "hello test";
    }
}
