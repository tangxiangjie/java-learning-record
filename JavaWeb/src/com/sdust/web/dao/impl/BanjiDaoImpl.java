/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/4/8 下午3:53 ~ 2024/4/17 上午10:37
 * Modified date: 2024/4/17 上午10:37
 */

package com.sdust.web.dao.impl;

import com.sdust.web.dao.IBanjiDao;
import com.sdust.web.pojo.entity.Banji;
import com.sdust.web.pojo.query.BanjiQuery;
import com.sdust.web.pojo.vo.BanjiCountVO;
import com.sdust.web.util.JDBCUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BanjiDaoImpl implements IBanjiDao {
    @Override
    public List<Banji> selectAll() {
        Connection connection = null;
        //Statement statement = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Banji> banjiList = new ArrayList<>();
        try {
            connection = JDBCUtil.getConnection();
            String sql = "select id,name from banji";
            //LEFT JOIN INNER JOIN
            //预编译的statement
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                Banji banji = new Banji(id, name);
                System.out.println(banji);
                banjiList.add(banji);
            }
            for (Banji banji : banjiList) {
                System.out.println(banji);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            JDBCUtil.close(connection, statement, resultSet);
        }
        return banjiList;
    }

    @Override
    public List<Banji> selectByPage(int offset, Integer pageSize) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Banji> list = new ArrayList<>();
        try {
            connection = JDBCUtil.getConnection();
            String sql = "select id,name,address,date from banji LIMIT ?,?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1, offset);
            statement.setInt(2, pageSize);
            System.out.println(statement);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                String address = resultSet.getString("address");
                int date = resultSet.getInt("date");
                Banji banji = new Banji(id, name, address, date);
                list.add(banji);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return list;
    }

    @Override
    public int selcetTotalCount() {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        int totalCount = 0;
        try {
            connection = JDBCUtil.getConnection();
            String sql = "select count(*) from banji";
            statement = connection.prepareStatement(sql);
            System.out.println(statement);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                totalCount = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return totalCount;
    }

    @Override
    public void add(Banji banji) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = JDBCUtil.getConnection();
            String sql = "insert into banji(name,address,date) values(?,?,?)";
            statement = connection.prepareStatement(sql);
            statement.setString(1,banji.getName());
            statement.setString(2,banji.getAddress());
            statement.setInt(3,banji.getDate());
            System.out.println(statement);
            int count = statement.executeUpdate();
            System.out.println("count:" + count);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            JDBCUtil.close(connection, statement, null);
        }
    }

    @Override
    public List<BanjiCountVO> selectBanjiCount() {
        Connection connection = null;
        //Statement statement = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<BanjiCountVO> banjiList = new ArrayList<>();
        try {
            connection = JDBCUtil.getConnection();
            String sql = "SELECT b.`name` AS name,COUNT(s.id) AS value\n" +
                    "FROM student AS s INNER JOIN banji AS b\n" +
                    "ON s.banji_id=b.id\n" +
                    "GROUP BY banji_id;";
            //LEFT JOIN INNER JOIN
            //预编译的statement
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                String name = resultSet.getString("name");
                int value = resultSet.getInt("value");
                BanjiCountVO banjiCountVO = new BanjiCountVO();
                banjiCountVO.setName(name);
                banjiCountVO.setValue(value);
                System.out.println(banjiCountVO);
                banjiList.add(banjiCountVO);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            JDBCUtil.close(connection, statement, resultSet);
        }
        return banjiList;
    }

    @Override
    public void deleteById(int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = JDBCUtil.getConnection();
            String sql = "delete from banji where id=?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1,id);
            System.out.println(statement);
            int count = statement.executeUpdate();
            System.out.println("count:" + count);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            JDBCUtil.close(connection, statement, null);
        }
    }

    /**
     * dao应该是纯粹的数据库操作，在这里边写for循环也相当于写了逻辑，不太符合三层架构原则
     **/
    @Override
    public void deleteSelected(String[] ids) {

        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = JDBCUtil.getConnection();
            //String sqll = "delete from banji where id in (";
            StringBuilder sql = new StringBuilder("delete from banji where id in (");
            //StringBuilder线程不安全 效率高 StringBuffer 线程安全 效率低
            for (int i = 0; i < ids.length; i++) {
                //sqll += (i == ids.length - 1) ? "?)" : "?,";
                sql.append((i == ids.length - 1) ? "?)" : "?,");
            }
            System.out.println(sql);
            statement = connection.prepareStatement(sql.toString());//String, which is used in SQL, can be unsafe
            for (int i = 0; i < ids.length; i++) {
                statement.setInt(i+1,Integer.parseInt(ids[i]));
            }
            System.out.println(statement);
            int count = statement.executeUpdate();
            System.out.println("count:" + count);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            JDBCUtil.close(connection, statement, null);
        }
    }

    @Override
    public Banji selectById(int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Banji banji = new Banji();
        try {
            connection = JDBCUtil.getConnection();
            String sql = "select id,name,address,date from banji WHERE id=?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1, id);
            System.out.println(statement);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                String name = resultSet.getString("name");
                String address = resultSet.getString("address");
                int date = resultSet.getInt("date");
                banji.setId(id);
                banji.setName(name);
                banji.setAddress(address);
                banji.setDate(date);
            }
            System.out.println(banji);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return banji;
    }

    @Override
    public void updateForm(Banji banji) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = JDBCUtil.getConnection();
            String sql = "update banji set name=?,address=?,date=? where id=?";
            statement = connection.prepareStatement(sql);
            statement.setString(1,banji.getName());
            statement.setString(2,banji.getAddress());
            statement.setInt(3,banji.getDate());
            statement.setInt(4,banji.getId());
            System.out.println(statement);
            int count = statement.executeUpdate();
            System.out.println("count:" + count);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            JDBCUtil.close(connection, statement, null);
        }
    }

    @Override
    public List<Banji> selectByPage(BanjiQuery banjiQuery) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Banji> list = new ArrayList<>();
        try {
            connection = JDBCUtil.getConnection();
            //String sql = "select id,name,address,date from banji where name like ? and address like ? and date like ? LIMIT ?,?";
            //String sql = "select id,name,address,date from banji where date like ? LIMIT ?,?";
            //String sql = "select id,name,address,date from banji where name like ? and address like ? LIMIT ?,?";
            //String sql = "select id,name,address,date from banji where name like ? LIMIT ?,?";
            //这个条件不是空，我就拼接上这个条件 and xxx like ? 不是字符串就不要写模糊查找了
            //String sql = "select id,name,address,date from banji";
            //String sql = "select id,name,address,date from banji where 1=1";sql注入，利用sql特性来解决问题
            //①where成为第一个条件②控制where出现（多个if效率就低了）
            //if(name is not null) {
            //  sql += " and name like ?"
            //}
            //if(address is not null) {
            //  sql += " and address like ?"
            //}
            //if(date is not null) {
            //  sql += " and date like ?"
            //}
            //sql += " LIMIT ?,?"
            String sql = "select id,name,address,date from banji where 1=1";
            //构造一个集合存放所有的条件
            List<Object> queryList = new ArrayList<>();
            String queryName = banjiQuery.getName();
            if (queryName != null && !"".equals(queryName)) {//短路无所谓
                sql += " and name like ?";
                //queryList.add(queryName);
                queryList.add("%" + queryName +"%");
            }
            String queryAddress = banjiQuery.getAddress();
            if (queryAddress != null && !"".equals(queryAddress)) {//短路无所谓
                sql += " and address like ?";
                //queryList.add(queryAddress);
                queryList.add("%" + queryAddress +"%");
            }
            Integer queryDate = banjiQuery.getDate();
            if (queryDate != null) {//短路无所谓
                sql += " and date like ?";
                //queryList.add(queryName);
                queryList.add("%" + queryDate +"%");
            }
            sql += " limit ?,?";
            statement = connection.prepareStatement(sql);//预编译
            for (int i = 0; i < queryList.size(); i++) {
                statement.setObject(i + 1, queryList.get(i));
            }
            int offset = (banjiQuery.getPage() - 1) * banjiQuery.getLimit();
            statement.setInt(queryList.size() + 1, offset);

            statement.setInt(queryList.size() + 2, banjiQuery.getLimit());
            System.out.println(statement);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                String address = resultSet.getString("address");
                int date = resultSet.getInt("date");
                Banji banji = new Banji(id, name, address, date);
                list.add(banji);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return list;
    }

    @Override
    public int selcetTotalCount(BanjiQuery banjiQuery) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        int totalCount = 0;
        try {
            connection = JDBCUtil.getConnection();
            String sql = "select count(*) from banji where 1=1";
            List<Object> queryList = new ArrayList<>();
            String queryName = banjiQuery.getName();
            if (queryName != null && !"".equals(queryName)) {//短路无所谓
                sql += " and name like ?";
                //queryList.add(queryName);
                queryList.add("%" + queryName +"%");
            }
            String queryAddress = banjiQuery.getAddress();
            if (queryAddress != null && !"".equals(queryAddress)) {//短路无所谓
                sql += " and address like ?";
                //queryList.add(qaueryAddress);
                queryList.add("%" + queryAddress +"%");
            }
            Integer queryDate = banjiQuery.getDate();
            if (queryDate != null) {//短路无所谓
                sql += " and date like ?";
                //queryList.add(queryName);
                queryList.add("%" + queryDate +"%");
            }
            statement = connection.prepareStatement(sql);
            for (int i = 0; i < queryList.size(); i++) {
                statement.setObject(i+1, queryList.get(i));
            }
            System.out.println(statement);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                totalCount = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return totalCount;
    }
}
