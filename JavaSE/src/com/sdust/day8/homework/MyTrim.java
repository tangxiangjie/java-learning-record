/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/3/21 下午10:49 ~ 2024/3/22 上午11:59
 * Modified date: 2024/3/22 上午11:59
 */

package com.sdust.day8.homework;

import org.junit.Test;

import java.util.Scanner;

public class MyTrim {
    public String myTrim(String str) {
        if (str == null || str.isEmpty()) {//"".equals(str)
             return null;
        }
        char[] chars = str.toCharArray();
        int start = 0;
        int end = str.length() - 1;
        while (chars[start] == ' ' && start < end){//头标往右直到不为空格或者到倒数第二个，防止数组越界访问
            start++;
        }
        while (chars[end] == ' ' && end > start) {//尾标向右直到不为空格或者正数第二个，防止数组越界
            end--;
        }
        //System.out.println(start);
        //System.out.println(end);
        if (chars[start] == ' ') {//最后一次start++不一定是因为不是空格跳出while, 检验一下
            return null;
        }
        return str.substring(start, end + 1);//[start, end + 1)
    }

    public String teacherTrim(String str) {
        if(str == null || str.equals("")) {
            return "";
        }

        int startIndex = 0;
        int endIndex = str.length() - 1;

        while (startIndex <= endIndex && (str.charAt(startIndex) == '-')) {
            startIndex++;
        }
        while (startIndex <= endIndex && (str.charAt(endIndex) == '-')) {
            endIndex--;
        }

        return  str.substring(startIndex, endIndex + 1);
    }

    @Test
    public void test1() {
        String[] strs = {null, "", "    ", "     a     a     ", "     java Android  ", "   A", "a   ", "A"};
        for (String str : strs) {
            System.out.println("你输入的字符串是: " + str);
            System.out.println("结果: " + myTrim(str));
        }
    }

    @Test
    public void test2() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入:");
        String str = scanner.nextLine();
        System.out.println("你输入的字符串是: " + str);
        System.out.println("结果: " + myTrim(str));
    }

    @Test
    public void test3() {
        String str = "----------A";
        String newStr = teacherTrim(str);
        System.out.println(newStr);
    }
}
