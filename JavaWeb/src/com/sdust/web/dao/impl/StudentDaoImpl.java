/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/4/7 上午11:20 ~ 2024/4/10 上午9:34
 * Modified date: 2024/4/10 上午9:34
 */

package com.sdust.web.dao.impl;

import com.sdust.web.dao.IStudentDao;
import com.sdust.web.pojo.entity.Student;
import com.sdust.web.pojo.vo.StudentBanjiVO;
import com.sdust.web.util.JDBCUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class StudentDaoImpl implements IStudentDao {
    @Override
    public List<StudentBanjiVO> selectAll() {
        Connection connection = null;
        //Statement statement = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<StudentBanjiVO> studentBanjiVOList = new ArrayList<>();
        try {
            connection = JDBCUtil.getConnection();
            String sql = "SELECT s.id,s.`name`,s.age,s.gender,b.id as banji_id,b.`name` AS banji_name\n" +
                    "FROM student AS s LEFT JOIN banji AS b-- 与内连接效果一样\n" +
                    "ON s.banji_id=b.id";
            //LEFT JOIN INNER JOIN
            //预编译的statement
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                int age = resultSet.getInt("age");
                String gender = resultSet.getString("gender");
                int banjiId = resultSet.getInt("banji_id");
                String banjiName = resultSet.getString("banji_name");
                StudentBanjiVO studentBanjiVO = new StudentBanjiVO(id, name, age, gender, banjiId, banjiName);
                System.out.println(studentBanjiVO);
                studentBanjiVOList.add(studentBanjiVO);
            }
            for (StudentBanjiVO studentBanjiVO : studentBanjiVOList) {
                System.out.println(studentBanjiVO);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            JDBCUtil.close(connection, statement, resultSet);
        }
        return studentBanjiVOList;
    }

    @Override
    public void deleteById(Integer id) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = JDBCUtil.getConnection();
            String sql = "delete from student where id=?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1,id);
            System.out.println(statement);
            int count = statement.executeUpdate();
            System.out.println("count:" + count);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            JDBCUtil.close(connection, statement, null);
        }
    }

    @Override
    public void add(Student student) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = JDBCUtil.getConnection();
            String sql = "insert into student(name,age,gender,banji_id) values(?,?,?,?)";
            statement = connection.prepareStatement(sql);
            statement.setString(1,student.getName());
            statement.setInt(2,student.getAge());
            statement.setString(3,student.getGender());
            statement.setInt(4,student.getBanjiId());
            System.out.println(statement);
            int count = statement.executeUpdate();
            System.out.println("count:" + count);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            JDBCUtil.close(connection, statement, null);
        }
    }

    @Override
    public Student selectById(Integer queryId) {

        Connection connection = null;
        //Statement statement = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Student student = null;
        try {
            connection = JDBCUtil.getConnection();
            String sql = "select id,name,age,gender,banji_id from student where id=?";
            //预编译的statement
            statement = connection.prepareStatement(sql);
            statement.setInt(1, queryId);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                int age = resultSet.getInt("age");
                String gender = resultSet.getString("gender");
                int banjiId = resultSet.getInt("banji_id");
                student = new Student(id, name, age, gender, banjiId);
                System.out.println(student);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            JDBCUtil.close(connection, statement, resultSet);
        }

        return student;
    }

    @Override
    public void update(Student student) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = JDBCUtil.getConnection();
            String sql = "update student set name=?,age=?,gender=?,banji_id=? where id=?";
            statement = connection.prepareStatement(sql);
            statement.setString(1,student.getName());
            statement.setInt(2,student.getAge());
            statement.setString(3,student.getGender());
            statement.setInt(4,student.getBanjiId());
            statement.setInt(5,student.getId());
            System.out.println(statement);
            int count = statement.executeUpdate();
            System.out.println("count:" + count);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            JDBCUtil.close(connection, statement, null);
        }
    }

    @Override
    public List<StudentBanjiVO> selectByPage(int offset, Integer pageSize) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<StudentBanjiVO> list = new ArrayList<>();
        try {
            connection = JDBCUtil.getConnection();
            String sql = "SELECT s.id,s.`name`,s.age,s.gender,b.id as banji_id,b.`name` AS banji_name\n" +
                    "FROM student AS s LEFT JOIN banji AS b\n" +/*INNER JOIN*/
                    "ON s.banji_id=b.id\n" +
                    "ORDER BY s.id\n" +
                    "LIMIT ?,? ";
            statement = connection.prepareStatement(sql);
            statement.setInt(1, offset);
            statement.setInt(2, pageSize);
            System.out.println(statement);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                int age = resultSet.getInt("age");
                String gender = resultSet.getString("gender");
                int banjiId = resultSet.getInt("banji_id");
                String banjiName = resultSet.getString("banji_name");
                StudentBanjiVO student = new StudentBanjiVO(id, name, age, gender, banjiId, banjiName);
                list.add(student);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return list;
    }

    @Override
    public int selectTotalCount() {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        int totalCount = 0;
        try {
            connection = JDBCUtil.getConnection();
            String sql = "select count(*) from student";
            statement = connection.prepareStatement(sql);
            System.out.println(statement);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                totalCount = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return totalCount;
    }
}
