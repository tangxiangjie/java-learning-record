<%@ page import="com.sdust.web.util.PageInfo" %>
<%@ page import="com.sdust.web.pojo.entity.Course" %>
<%@ page import="java.util.List" %>
<%--
  ~ 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
  ~ Copyright @TangXJ
  ~ Created by TangXJ
  ~ Created&Used date: 2024/4/7 下午5:22 ~ 2024/4/9 上午11:48
  ~ Modified date: 2024/4/9 上午11:48
  --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="static/bootstrap-3.4.1-dist/css/bootstrap.css">
</head>
<body>
    <%
        List<Course> courseList = (List<Course>) request.getAttribute("courseList");
    %>
    <a class="btn btn-primary" href="course_add.jsp">添加</a>
    <table class="table table-striped table-bordered table-hover table-condensed">
        <tr>
            <td>ID</td>
            <td>课程名称</td>
            <td>学分</td>
            <td>编辑</td>
            <td>删除</td>
        </tr>
        <%
            for (Course course : courseList) {
        %>
        <tr>
            <td><%=course.getId()%></td>
            <td><%=course.getName()%></td>
            <td><%=course.getCredit()%></td>
            <td><a href="/course?method=updateQuery&id=<%=course.getId()%>">编辑</a></td>
            <td><a href="javascript:void(0)" onclick="deleteById(<%=course.getId()%>)">删除</a></td>
        </tr>
        <%
            }
        %>
    </table>

    <script>
        function deleteById(id) {
            var isDelete = confirm('您确认要删除吗？')
            if (isDelete) {
                location.href = '/course?method=deleteById&id=' + id;
            }
        }
    </script>
</body>
</html>
