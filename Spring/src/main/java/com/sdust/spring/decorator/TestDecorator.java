/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/11 上午11:21
 * Modified date: 2024/6/11 上午11:21
 */

package com.sdust.spring.decorator;

import com.sdust.spring.decorator.impl.Circle;
import com.sdust.spring.decorator.impl.RedShapeDecorator;

public class TestDecorator {
    public static void main(String[] args) {
        Shape circle = new Circle();
        Shape decoratedCircle = new RedShapeDecorator(circle);//对Shape来说 无需知道装饰器类(ShapeDecorator)的存在 有点多态的味道
        decoratedCircle.draw();
        System.out.println("-------------------");
        circle.draw();
    }
}
