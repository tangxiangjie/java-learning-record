/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/5/30 上午9:29
 * Modified date: 2024/5/30 上午9:29
 */

package com.sdust.day38;

import com.sdust.day37.Student;
import com.sdust.day37.Teacher;
import org.junit.Test;

public class GenericDemo2 {

    public Student add(Student student, Teacher teacher) {
        return student;
    }

    //<K,T>标明是泛型
    public <K,T> K add(K k, T t){
        return k;
    }

    @Test
    public void test1() {
        Float result1 = add(1.0f, 1);
        System.out.println(result1);
        String result2 = add("adc", 1);
        System.out.println(result2);
    }

    @Test
    public void test2() {
        Student student = new Student("李四");
        BaseDao<Student> studentBaseDao = new BaseDao<>();
        studentBaseDao.add(student);
        studentBaseDao.update(student);

        Teacher teacher = new Teacher();
        BaseDao<Teacher> teacherBaseDao = new BaseDao<>();
        teacherBaseDao.add(teacher);
        teacherBaseDao.update(teacher);
    }

}
