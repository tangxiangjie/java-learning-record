/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/13 下午10:13
 * Modified date: 2024/6/13 下午10:13
 */

package com.sdust.springboot.annotation;

import java.lang.annotation.*;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MyLog {
    /**
     * 对哪个模块操作
     * @return
     */
    public String module() default "";

    public String desc() default "";
}
