/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/6 下午11:42 ~ 2024/6/7 上午1:10
 * Modified date: 2024/6/6 下午11:44
 */

package com.sdust.spring.proxy.staticc;

public class Client {

    public static void main(String[] args) {
        DongShiZhang dongShiZhang = new DongShiZhang();
        IQianzi mishu = new MiShu(dongShiZhang);
        //前两行在实际开发中 其实早就应该有了
        // 为了测试没办法，上边的代码实际上不会出现在客户端当中

        mishu.qianzi();
    }
}
