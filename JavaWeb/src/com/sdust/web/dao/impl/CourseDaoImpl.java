/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/4/9 上午10:23 ~ 2024/4/9 上午10:28
 * Modified date: 2024/4/9 上午10:28
 */

package com.sdust.web.dao.impl;

import com.sdust.web.dao.ICourseDao;
import com.sdust.web.pojo.entity.Course;
import com.sdust.web.util.JDBCUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CourseDaoImpl implements ICourseDao {
    @Override
    public List<Course> selectAll() {
        Connection connection = null;
        //Statement statement = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Course> courseList = new ArrayList<>();
        try {
            connection = JDBCUtil.getConnection();
            String sql = "select id,name,credit from course";
            //LEFT JOIN INNER JOIN
            //预编译的statement
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                int credit = resultSet.getInt("credit");
                Course course = new Course(id, name, credit);
                System.out.println(course);
                courseList.add(course);
            }
            for (Course course : courseList) {
                System.out.println(course);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            JDBCUtil.close(connection, statement, resultSet);
        }
        return courseList;
    }

    @Override
    public void deleteById(Integer id) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = JDBCUtil.getConnection();
            String sql = "delete from course where id=?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1,id);
            System.out.println(statement);
            int count = statement.executeUpdate();
            System.out.println("count:" + count);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            JDBCUtil.close(connection, statement, null);
        }
    }

    @Override
    public void add(Course course) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = JDBCUtil.getConnection();
            String sql = "insert into course(name,credit) values(?,?)";
            statement = connection.prepareStatement(sql);
            statement.setString(1,course.getName());
            statement.setInt(2,course.getCredit());
            System.out.println(statement);
            int count = statement.executeUpdate();
            System.out.println("count:" + count);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            JDBCUtil.close(connection, statement, null);
        }
    }

    @Override
    public Course selectById(Integer queryId) {

        Connection connection = null;
        //Statement statement = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Course course = null;
        try {
            connection = JDBCUtil.getConnection();
            String sql = "select id,name,credit from course where id=?";
            //预编译的statement
            statement = connection.prepareStatement(sql);
            statement.setInt(1, queryId);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                int credit = resultSet.getInt("credit");
                course = new Course(id, name, credit);
                System.out.println(course);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            JDBCUtil.close(connection, statement, resultSet);
        }

        return course;
    }

    @Override
    public void update(Course course) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = JDBCUtil.getConnection();
            String sql = "update course set name=?,credit=?where id=?";
            statement = connection.prepareStatement(sql);
            statement.setString(1,course.getName());
            statement.setInt(2,course.getCredit());
            statement.setInt(3,course.getId());
            System.out.println(statement);
            int count = statement.executeUpdate();
            System.out.println("count:" + count);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            JDBCUtil.close(connection, statement, null);
        }
    }

    @Override
    public List<Course> selectByPage(int offset, Integer pageSize) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Course> list = new ArrayList<>();
        try {
            connection = JDBCUtil.getConnection();
            String sql = "SELECT id,name,credit from course LIMIT ?,?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1, offset);
            statement.setInt(2, pageSize);
            System.out.println(statement);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                int credit = resultSet.getInt("credit");
                Course course = new Course(id, name, credit);
                list.add(course);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return list;
    }

    @Override
    public int selectTotalCount() {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        int totalCount = 0;
        try {
            connection = JDBCUtil.getConnection();
            String sql = "select count(*) from course";
            statement = connection.prepareStatement(sql);
            System.out.println(statement);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                totalCount = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return totalCount;
    }
}
