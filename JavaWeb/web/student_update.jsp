<%@ page import="com.sdust.web.pojo.entity.Student" %>
<%@ page import="java.util.List" %>
<%@ page import="com.sdust.web.pojo.entity.Banji" %><%--
  ~ 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
  ~ Copyright @TangXJ
  ~ Created by TangXJ
  ~ Created&Used date: 2024/4/3 下午2:54 ~ 2024/4/8 下午4:19
  ~ Modified date: 2024/4/8 下午4:19
  --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
    <%
        //JSP脚本，这里面可以任意写java代码
        //request、response:JSP内置对象
        Student student = (Student) request.getAttribute("student");
        List<Banji> banjiList = (List<Banji>) request.getAttribute("banjiList");
        System.out.println(student);
        System.out.println(banjiList);
    %>

    <%--<form action="/student?method=update&id=<%=student.getId()%>" method="post">--%><%--默认就是get ?method=就是get方法传参  会被覆盖  所以form表单一般不用--%>
    <form action="/student?method=update" method="post" onsubmit="return update_confirm()"><%--默认就是get ?method=就是get方法传参  会被覆盖  所以form表单一般不用--%>
        <input type="hidden" name="id" value="<%=student.getId()%>"/>
        <%--<input type="hidden" name="id" value="<%=student.getId()%>"/>--%><%--readonly 可以删掉F12！！ 以后学后端验证--%>
        姓名:<input type="text" name="name" value="<%=student.getName()%>"><br>
        年龄:<input type="text" name="age" value="<%=student.getAge()%>"><br>
        性别:<input type="text" name="gender" value="<%=student.getGender()%>"><br>
        班级:<select name="banjiId">
                <%
                    for (Banji banji : banjiList) {
                        String selected = (student.getBanjiId() == banji.getId()) ? "selected" : "";
                %>
                <option value="<%=banji.getId()%>" <%=selected%>><%=banji.getName()%></option>
                    <%
                    }
                %>
            </select><br>
        <input type="submit" value="更新">
    </form>

    <script>
        function update_confirm(){
            return confirm("您确认要更新吗？");
        }
    </script>
</body>
</html>
