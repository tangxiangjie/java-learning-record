<%--
  ~ 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
  ~ Copyright @TangXJ
  ~ Created by TangXJ
  ~ Created&Used date: 2024/4/12 上午11:16 ~ 2024/4/20 下午3:33
  ~ Modified date: 2024/4/18 下午7:45
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
    省份：
    <select id="provinceId" >
        <option>---请选择省份---</option>
    </select>
    城市：
    <select id="cityId">
        <option>---请选择城市---</option>
    </select>
    区县：
    <select id="areaId">
        <option>---请选择区县---</option>
    </select>

    <script src="/static/jquery-2.1.4.js" type="text/javascript" charset="utf-8"></script>
    <script>
        $(function() {
            $.post(
                '/ajax3?method=selectProvince',
                function (jsonObj) {
                    //[{id: 110000, name: '北京市'},{id: 130000, name: '河北省'}]
                    console.log(jsonObj);
                    $(jsonObj).each(function (){
                        //console.log(this.id);
                        //console.log(this.name);
                        //this : {id: 110000, name: '北京市'}
                        //$('#provinceId').append('<option value="1">---请选择省份---</option>');
                        $('#provinceId').append('<option value="'+this.id+'">'+this.name+'</option>')
                    })
                },
                'json'
            );

            //给省份下拉框绑定change事件
            $('#provinceId').change(function () {
                //this
                $.post(
                    '/ajax3?method=selectCity',
                    {'provinceId': $(this).val()},
                    function (jsonObj) {
                        console.log(jsonObj);
                        //清除原来的数据
                        //$('#cityId option:not(:first)').remove();
                        $('#cityId option:gt(0)').remove();
                        $('#areaId option:gt(0)').remove();
                        $(jsonObj).each(function() {
                            $('#cityId').append('<option value="'+this.id+'">'+this.name+'</option>');
                        })
                    },
                    'json'
                );
            });

            //给城市下拉框绑定change事件
            $('#cityId').change(function () {
                //this
                $.post(
                    '/ajax3?method=selectArea',
                    {'cityId': $(this).val()},
                    function (jsonObj) {
                        console.log(jsonObj);
                        //清除原来的数据
                        //$('#areaId option:not(:first)').remove();
                        $('#areaId option:gt(0)').remove();
                        $(jsonObj).each(function() {
                            $('#areaId').append('<option value="'+this.id+'">'+this.name+'</option>');
                        })
                    },
                    'json'
                );
            });
        });
    </script>
</body>
</html>