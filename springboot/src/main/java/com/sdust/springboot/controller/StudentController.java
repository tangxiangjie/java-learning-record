/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/11 下午9:34
 * Modified date: 2024/6/11 下午9:34
 */

package com.sdust.springboot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/student")
public class StudentController {

    @RequestMapping("/demo")
    public void demo(Integer id, String name) {
        System.out.println("========StudentController.demo========");
        System.out.println(id);
        System.out.println(name);
    }
}
