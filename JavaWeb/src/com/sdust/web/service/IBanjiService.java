/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/4/8 下午3:46 ~ 2024/4/16 下午9:02
 * Modified date: 2024/4/16 下午9:02
 */

package com.sdust.web.service;

import com.sdust.web.pojo.entity.Banji;
import com.sdust.web.pojo.query.BanjiQuery;
import com.sdust.web.pojo.vo.BanjiCountVO;
import com.sdust.web.util.LayUITableResult;
import com.sdust.web.util.PageInfo;

import java.util.List;

public interface IBanjiService {

    List<Banji> selectAll();

    PageInfo<Banji> selectByPage(Integer pageNo, Integer pageSize);

    LayUITableResult selectByPage2(Integer pageNo, Integer pageSize);

    LayUITableResult selectByPage3(BanjiQuery banjiQuery);

    void add(Banji banji);

    List<BanjiCountVO> selectBanjiCount();

    void deleteById(int id);

    void deleteSelected(String[] ids);

    Banji selectById(int id);

    void updateForm(Banji banji);

}
