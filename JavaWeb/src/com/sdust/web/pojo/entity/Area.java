/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/4/12 下午3:23 ~ 2024/4/12 下午3:27
 * Modified date: 2024/4/12 下午3:27
 */

package com.sdust.web.pojo.entity;

public class Area {
    private Integer id;
    private String name;
    private Integer CityId;

    public Area() {
    }

    public Area(Integer id, String name, Integer cityId) {
        this.id = id;
        this.name = name;
        CityId = cityId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCityId() {
        return CityId;
    }

    public void setCityId(Integer cityId) {
        CityId = cityId;
    }
}
