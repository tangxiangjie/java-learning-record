/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/4/12 上午11:03 ~ 2024/4/12 上午11:31
 * Modified date: 2024/4/12 上午11:31
 */

package com.sdust.web.controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/ajax1")
public class Ajax1Servlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("Ajax1Servlet.doGet");
        String name = req.getParameter("name");
        System.out.println(name);

        //sleep 5秒
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        //{"name":"李四1", "age":23}
        resp.setContentType("text/html;charset=UTF-8");
        resp.getWriter().write("{\"name\":\"李四1\", \"age\":23}");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("Ajax1Servlet.doPost");
        String name = req.getParameter("name");
        System.out.println(name);

        //{"name":"李四2", "age":23}
        resp.setContentType("text/html;charset=UTF-8");
        resp.getWriter().write("{\"name\":\"李四2\", \"age\":23}");
    }
}
