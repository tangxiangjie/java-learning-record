/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/4/8 下午3:44 ~ 2024/4/16 上午9:16
 * Modified date: 2024/4/16 上午9:16
 */

package com.sdust.web.pojo.entity;

public class Banji {
    private Integer id;
    private String name;
    private String address;
    private Integer date;

    public Banji() {
    }

    public Banji(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Banji(Integer id, String name, String address, Integer date) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.date = date;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getDate() {
        return date;
    }

    public void setDate(Integer date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Banji{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", date=" + date +
                '}';
    }
}
