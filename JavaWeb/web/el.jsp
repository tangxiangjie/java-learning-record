<%@ page import="com.sdust.web.pojo.entity.Student" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%--
  ~ 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
  ~ Copyright @TangXJ
  ~ Created by TangXJ
  ~ Created&Used date: 2024/4/18 上午9:05 ~ 2024/4/18 上午9:48
  ~ Modified date: 2024/4/18 上午9:48
  --%>

<%--
  Created by IntelliJ IDEA.
  User: TangXJ
  Date: 2024/4/18
  Time: 9:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Title</title>
</head>
<body>
    <%
        //JSP内置对象: request(Request)、response(Response)、session(Session)、application(Servletcontext)、pageContext(PageContext)
        //page: 当前整个JSP页面这个对象
        //1.普通字符串
        pageContext.setAttribute("name", "zhangsan");
        request.setAttribute("name", "lisi");
        //2.Student对象
        Student student = new Student(1, "zhansga", 23, "男", 1);
        session.setAttribute("student", student);
        //3.List<Student>
        List<Student> list = new ArrayList<>();
        Student student1 = new Student(1, "zhangsan1", 23, "男", 2);
        Student student2 = new Student(2, "zhangsan2", 23, "男", 3);
        Student student3 = new Student(3, "zhangsan3", 23, "男", 3);
        list.add(student1);
        list.add(student2);
        list.add(student3);
        application.setAttribute("list", list);
    %>

    <%--JSP表达式--%>
    <%=pageContext.getAttribute("name")%><br>
    <%=request.getAttribute("name")%><br>
    <%
        Student stu = (Student) session.getAttribute("student");
    %>
    <%--不声明成变量直接写也行--%>
    <%=stu.getName()%>
    <%=stu.getAge()%>

    <%
        List<Student> stuList = (List<Student>) application.getAttribute("list");
    %>
    <%=stuList.get(0).getName()%>
    <%=stuList.get(0).getAge()%>

    <hr>
    <%--EL表达式就是用来拿域对象里边的值的--%>
    <%--EL表达式--%>
    <%--不能用pageContext了   这是java对象 ×  改名了而已--%>
    ${pageScope.name}<br>
    ${requestScope.name}<br>
    ${name}<br><%--pageContext 从小到大的范围里边找page(PageContext) request(Request) session(Session) application(ServletContext) --%>
    ${sessionScope.student.name}<br><%--前提是生成标准的getset--%>
    ${list[0].name}<br>
    ${student.name}<br><%--会在session中找到     index.jsp里边显示用户名那个--%>
    ${list}<br>

    <%--<%=%>解决了  循环遍历的java代码还没解决--%>
    <%--JspStandardTagLibrary--%>

    <%--1、向域对象放数据--%>
    <%
        pageContext.setAttribute("age", 23);
        session.setAttribute("gender", "男");
    %>
    <c:set var="age" value="24" scope="request"></c:set>
    ${age} <br/>
    <hr/>
    <%--2、条件判断--%>
</body>
</html>
