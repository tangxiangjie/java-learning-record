/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/7 下午1:58
 * Modified date: 2024/6/7 下午1:58
 */

package com.sdust.ssm.service.impl;

import com.sdust.ssm.mapper.StudentMapper;
import com.sdust.ssm.pojo.Student;
import com.sdust.ssm.service.IStudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentServiceImpl implements IStudentService {
    @Autowired
    private StudentMapper studentMapper;

    @Override
    public List<Student> selectAll() {
        return studentMapper.selectAll();
    }

    @Override
    public void deleteById(Integer id) {

    }

    @Override
    public Student selectById(Integer id) {
        return null;
    }
}
