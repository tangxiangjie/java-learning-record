/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/6/7 下午2:17
 * Modified date: 2024/6/7 下午2:17
 */

package com.sdust.ssm.controller;

import com.sdust.ssm.pojo.Student;
import com.sdust.ssm.service.IStudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/student")//不加路径不带/student 直接方法也能访问到
public class StudentController {
    @Autowired
    private IStudentService studentService;

    @RequestMapping("/selectAll")
    @ResponseBody
    public List<Student> selectAll() {
        List<Student> students = studentService.selectAll();
        return students;
    }
}
