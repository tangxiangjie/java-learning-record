/*
 * 适度编码益脑，沉迷编码伤身，合理安排时间，享受快乐生活。
 * Copyright @TangXJ
 * Created by TangXJ
 * Created&Used date: 2024/4/2 上午11:34 ~ 2024/4/9 上午11:49
 * Modified date: 2024/4/9 上午11:49
 */

package com.sdust.web.controller;

import com.sdust.web.pojo.entity.Course;
import com.sdust.web.service.ICourseService;
import com.sdust.web.service.impl.CourseServiceImpl;
import com.sdust.web.util.PageInfo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/course")
public class CourseServlet extends HttpServlet {//一开始报红---和Web开发相关的jar包不在jdk里面，在Tomcat里面
    /*private ICourseDao courseDao = new CourseDaoImpl();*/
    private ICourseService courseService = new CourseServiceImpl();

    //访问Servlet时候默认访问service方法
    @Override//java重写父类方法
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //System.out.println("CourseServlet.service");
        req.setCharacterEncoding("UTF-8");//解决post请求乱码问题   method="post"

        //http://localhost:8080/course?method=selectAll
        //http://localhost:8080/course?method=deleteById&id=1
        String method = req.getParameter("method");//获取浏览器来的
        if (method == null || method.equals("")) {
            method = "selectByPage";
        }
        switch (method) {
            case "selectAll":
                selectAll(req, resp);
                break;
            case "deleteById":
                deleteById(req, resp);
                break;
            case "add":
                add(req, resp);
                break;
            case "updateQuery":
                updateQuery(req, resp);
                break;
            case "update":
                update(req, resp);
                break;
            case "selectByPage":
                selectByPage(req, resp);
                break;
        }

    }

    private void selectByPage(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String pageNo = req.getParameter("pageNo");
        String pageSize = req.getParameter("pageSize");
        if (pageNo == null || pageNo.equals("")) {
            pageNo = "1";
        }
        if (pageSize == null || pageSize.equals("")) {
            pageSize = "5";
        }

        PageInfo<Course> pageInfo = courseService.selectByPage(Integer.parseInt(pageNo), Integer.parseInt(pageSize));

        req.setAttribute("pageInfo", pageInfo);
        req.getRequestDispatcher("course_list2.jsp").forward(req, resp);
    }

    private void update(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        System.out.println("CourseServlet.update");

        String id = req.getParameter("id");
        String name = req.getParameter("name");
        String credit = req.getParameter("credit");

        Course course = new Course(Integer.parseInt(id), name, Integer.parseInt(credit));

        courseService.update(course);
        //重定向（让servle执行完之后 自己发送请求）302 服务器告诉浏览器去请求这个地址
        resp.sendRedirect("/course");
    }

    private void updateQuery(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("CourseServlet.updateQuery");

        String queryId = req.getParameter("id");
        Course course = courseService.selectById(Integer.parseInt(queryId));

        //把数据放到req里面
        req.setAttribute("course", course);
        req.getRequestDispatcher("course_update.jsp").forward(req, resp);
    }

    private void add(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        System.out.println("CourseServlet.add");

        String name = req.getParameter("name");
        String credit = req.getParameter("credit");
        Course course = new Course();
        course.setName(name);
        course.setCredit(Integer.parseInt(credit));

        courseService.add(course);

        //重定向（让servlet执行完之后 自己发送请求）302 服务器告诉浏览器去请求这个地址
        resp.sendRedirect("/course");
    }

    private void deleteById(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        System.out.println("CourseServlet.deleteById");
        //http://localhost:8080/deleteCourse?id=2
        String id = req.getParameter("id");

        courseService.deleteById(Integer.parseInt(id));

        resp.sendRedirect("/course");

    }

    private void selectAll(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("CourseServlet.selectAll");

        List<Course> courseList = courseService.selectAll();

        req.setAttribute("courseList", courseList);
        req.getRequestDispatcher("course_list.jsp").forward(req, resp);
    }
}
